<?php
$ios='https://itunes.apple.com/nz/app/marine-mate/id716514873?mt=8';

$android='https://play.google.com/store/apps/details?id=com.mogeo.marinemate';

$ua = strtolower($_SERVER['HTTP_USER_AGENT']);

if(strpos($ua,'android') !== false) {
  header('Location: '.$android);   exit();
}

if((strpos($ua,'iphone') !== false) || (strpos($ua,'ipod') !== false) || (strpos($ua,'ipad') !== false)) {
  header('Location: '.$ios);   exit();
}

echo '
<html>
<body style="width:320px; font-size:20px">
Sorry, there is no downloadable app for your device yet.
<br /><br />
<a href="'.$ios.'">iPhone app</a><br /><br />
<a href="'.$android.'">Android app</a><br /><br /><br />
</body>
</html>';
?>