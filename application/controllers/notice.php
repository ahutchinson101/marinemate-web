<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notice extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('notice_model');
		$this->lang->load('admin','english');
		
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->notice_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->notice_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$regions=$this->notice_model->GetAllRegions();
		$data=array();
		if(is_admin()==FALSE){
			if($this->session->userdata('region_id')!=0){
				$places=$this->notice_model->GetRegionPlaces($this->session->userdata('region_id'));
				$data['places']=$places;
			}
		}
		$data['regions']=$regions;
		$this->load->view('notice',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function GetPlaces($id=0){
		$places=$this->notice_model->GetRegionPlaces($id);
		if($places){
			$response=array();
			$response['result']=$this->lang->line('success');
			$response['data']=$places;
		}else{
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('place_not');
		}
		echo json_encode($response);exit;
	}
	public function addNew(){
		$action=$this->input->post('action');
		switch ($action){
			case 'add':
				
				//$place=$this->input->post('places');
				$title=$this->input->post('title');
				$notice_live=$this->input->post('notice_live');
				$expiry_date=$this->input->post('expiry_date');
				$notice_description=$this->input->post('description');
				$addedbyorganisation=$this->input->post('addedbyorganisation',true);
				$regions=$this->input->post('noticeregions');
				if(count($regions)<=0){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('region_error');
					echo  json_encode($response);exit;
				}
				/*else if($place==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('place_error');
					echo  json_encode($response);exit;
				}*/
				else if($title==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('title_error');
					echo  json_encode($response);exit;
				}else if($notice_live==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('notice_live_error');
					echo  json_encode($response);exit;
				}else if($notice_description==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('description_error');
					echo  json_encode($response);exit;
				}else{
					$response=array();
					$data=array();
					$data['user_id']=$this->session->userdata('user_id');
					//$data['place_id']=($place=='all')?0:$place;
					$data['notice_title']=$title;
					$data['notice_description']=$notice_description;
					$data['notice_live_date']=$notice_live;
					$data['expiry_date']=$expiry_date;
					$data['posted_date']=date('Y-m-d H:i:s');
					$data['addedbyorganization']=$addedbyorganisation;
					$this->db->insert("notice_and_region_mapping",array("regions_id"=>implode(',',$regions),'created_date'=>date('Y-m-d H:i:s')));
					$noticregionmapid=$this->db->insert_id();
					$data['notice_and_regions_id']=$noticregionmapid;
					for($i=0;$i<count($regions);$i++){
						$data['region_id']=($regions[$i]=='all')?0:$regions[$i];	
						$this->notice_model->AddNotice($data);
					}
						$response['result']=$this->lang->line('success');
					echo json_encode($response);exit;
				}
			break;
			case 'edit':
					$edit_id=$this->input->post('edit_id');
					if(intval($edit_id)){
						$regions=$this->input->post('noticeregions');
						//$place=$this->input->post('places');
						$title=$this->input->post('title');
						$notice_live=$this->input->post('notice_live');
						$expiry_date=$this->input->post('expiry_date');
						$notice_description=$this->input->post('description');
						$addedbyorganisation=$this->input->post('addedbyorganisation',true);
						$region_mapping_id=$this->input->post('region_mapping_id');
						if(count($regions)<=0){
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('region_error');
							echo  json_encode($response);exit;
						}
						/*else if($place==""){
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('place_error');
							echo  json_encode($response);exit;
						}*/
						else if($title==""){
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('title_error');
							echo  json_encode($response);exit;
						}else if($notice_live==""){
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('notice_live_error');
							echo  json_encode($response);exit;
						}else if($notice_description==""){
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('description_error');
							echo  json_encode($response);exit;
						}else{
							$response=array();
							$data=array();
							//$data['user_id']=$this->session->userdata('user_id');
							//$data['region_id']=($regions=='all')?0:$regions;
							//$data['place_id']=($place=='all')?0:$place;;
							$data['notice_title']=$title;
							$data['notice_description']=$notice_description;
							$data['notice_live_date']=$notice_live;
							$data['expiry_date']=$expiry_date;
							$data['updated_date']=date('Y-m-d H:i:s');
							$data['addedbyorganization']=$addedbyorganisation;
							$data['notice_and_regions_id']=$region_mapping_id;
							$this->db->update("notice_and_region_mapping",array("regions_id"=>implode(',',$regions),'updated_date'=>date('Y-m-d H:i:s')),array('id'=>$region_mapping_id));
							$notices=$this->db->get_where('notice',array('notice_and_regions_id'=>$region_mapping_id))->result();
							$existids=array();
							foreach($notices as $nt){
								if(in_array($nt->region_id,$regions)){
									$this->notice_model->UpdateNotice($data,$nt->id);
									$existids[]=$nt->region_id;
								}else{
									$this->db->query("delete from notice where id='".$nt->id."' AND region_id='".$nt->region_id."' AND notice_and_regions_id=".$nt->notice_and_regions_id);
									$this->db->insert('deleted_notice',array('notice_id'=>$nt->id,'created_date'=>date('Y-m-d H:i:s')));	
								}
							}
							for($i=0;$i<count($regions);$i++){
								if(!in_array($regions[$i],$existids)){
									$data['region_id']=($regions[$i]=='all')?0:$regions[$i];	
									$this->notice_model->AddNotice($data);
								}
							}
							$response['result']=$this->lang->line('success');
							/*if($this->notice_model->UpdateNotice($data,$edit_id)==TRUE){
								$response['result']=$this->lang->line('success');
							}else{
								$response['result']=$this->lang->line('error');
								$response['message']=$this->lang->line('notice_not_update');
							}*/
							echo json_encode($response);exit;
						}
					}else{
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('notice_not_update');
						echo json_encode($response);exit;
					}
				break;
		}
	}
	public function editForm($id=0){
		$regions=$this->notice_model->GetAllRegions();
		$notice=$this->notice_model->GetOneNotice($id);
		$places=$this->notice_model->GetRegionPlaces($notice->region_id);
		$notice_and_regions_mapping=$this->db->get_where('notice_and_region_mapping',array('id'=>$notice->notice_and_regions_id))->row();
		$data=array();
		$data['regions']=$regions;
		$data['places']=$places;
		$data['notice']=$notice;
		$data['edit_id']=$id;
		$data['notice_and_regions_mapping']=$notice_and_regions_mapping;
		$this->load->view('edit_notice',$data);
	}
	public function all(){
		$aColumns = array('notice_live_date','region_id','place_id','notice_title','addedbyorganization','expiry_date','id','user_id','notice_and_regions_id');
		$sIndexColumn = "id";
		$sTable = "notice";
		$iDisplayStart = $this->input->post('iDisplayStart', true);
		$iDisplayLength = $this->input->post('iDisplayLength', true);
		$iSortCol_0 = $this->input->post('iSortCol_0', true);
		$iSortingCols = $this->input->post('iSortingCols', true);
		$sEcho = $this->input->post('sEcho', true);
		$posted_form_date=$this->input->post('posted_form_date',true);
		$posted_to_date=$this->input->post('posted_to_date',true);
		$expiry_form_date=$this->input->post('expiry_form_date',true);
		$expiry_to_date=$this->input->post('expiry_to_date',true);
		$filter_place_regions=$this->input->post('filter_place_regions',true);
		$filter_place=$this->input->post('filter_place',true);
		$filter_title=$this->input->post('filter_title',true);
		$filter_search=$this->input->post('filter_search',true);
		$filter_addedbyorganisation=$this->input->post('filter_addedbyorganisation',true);
		
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}

		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		
		if(is_admin()==FALSE){
			if($this->session->userdata('region_id')!=0){
				$this->db->having('region_id',$this->session->userdata('region_id'));
				$this->db->or_having('region_id',0);
			}
		}
		if($filter_search!=""){
			$this->db->like('notice_title',$this->db->escape_like_str($filter_search));
			$this->db->or_like('DATE_FORMAT(notice_live_date,"%D %M %Y")', $this->db->escape_like_str($filter_search));
			$this->db->or_like('addedbyorganization',$this->db->escape_like_str($filter_search));
		}
		if($posted_form_date!=""){
			$this->db->where('DATE_FORMAT(notice_live_date,"%Y-%m-%d")>=',date('Y-m-d',strtotime($posted_form_date)));
		}
		if($posted_to_date!=""){
			$this->db->where('DATE_FORMAT(notice_live_date,"%Y-%m-%d")<=',date('Y-m-d',strtotime($posted_to_date)));
		}
		if($expiry_form_date!=""){
			$this->db->where('DATE_FORMAT(expiry_date,"%Y-%m-%d")>=',date('Y-m-d',strtotime($expiry_form_date)));
		}
		if($expiry_to_date!=""){
			$this->db->where('DATE_FORMAT(expiry_date,"%Y-%m-%d")<=',date('Y-m-d',strtotime($expiry_to_date)));
		}
		if($filter_place_regions!=""){
			$this->db->where('region_id',($filter_place_regions=='all'?0:$filter_place_regions));
		}
		if($filter_place!=""){
			$this->db->where('place_id',$filter_place);
		}
		if($filter_title!=""){
			$this->db->like('notice_title',$this->db->escape_like_str($filter_title));
		}
		if($filter_addedbyorganisation!=""){
			$this->db->like('addedbyorganization',$this->db->escape_like_str($filter_addedbyorganisation));
		}
		$this->db->group_by('notice_and_regions_id');
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		//echo  $this->db->last_query();exit;
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		if(is_admin()==FALSE){
			if($this->session->userdata('region_id')!=0){
			$iTotal = $this->db->query("SELECT count(id) as cnt FROM ".$sTable." WHERE `region_id` IN(".$this->session->userdata('region_id').",0) GROUP BY notice_and_regions_id")->row()->cnt;
			}else{
				$iTotal = $this->db->count_all($sTable);
			}
		}else{
			$iTotal = $this->db->count_all($sTable);
		}
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'notice_live_date' )	{
					$row[] = date('dS F Y',strtotime($aRow[ $aColumns[$i] ]));
				}
				else if($aColumns[$i]=="region_id"){
					$row[]=$this->notice_model->getMappingReginsName($aRow['notice_and_regions_id']);
					/*if($aRow[ $aColumns[$i] ]!=0){
						$row[] = $this->notice_model->GetRegionName($aRow[ $aColumns[$i] ]);
					}else{
						$row[] = 'All';
					}*/
				}
				else if( $aColumns[$i] == 'place_id' ){
					if($aRow[ $aColumns[$i] ]!=0){
						$row[] = $this->notice_model->GetPlaceName($aRow[ $aColumns[$i] ]);
					}else{
						$row[] = 'All';
					}
				}
				else if( $aColumns[$i] == 'notice_title' ){
					$row[] = $aRow[ $aColumns[$i] ];
				}else if( $aColumns[$i] == 'addedbyorganization'){
					$row[] = $aRow[ $aColumns[$i] ];
				}
				else if( $aColumns[$i] == 'expiry_date' ){
					$row[] = date('dS F Y',strtotime($aRow[ $aColumns[$i] ]));
				}
				
			}
			
			if(is_admin()==TRUE || $this->session->userdata('region_id')==0){
				$row[] = '<a href="javascript:void(0);" id="editnotice" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deletenotice" data-ID="'.$aRow['notice_and_regions_id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}else{
				$row[] = '<a href="javascript:void(0);" id="editnotice" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deletenotice" data-ID="'.$aRow['notice_and_regions_id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
				/*if($aRow['region_id']!=0){
					$data=$this->db->get_where('notice_and_region_mapping',array('id'=>$aRow['notice_and_regions_id']))->row();
					$ids=explode(',',$data->regions_id);
					if(count($ids)>1){
						$row[]="";
					}else{
						$row[] = '<a href="javascript:void(0);" id="editnotice" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>';
					}
				}else{
					$row[]="";
				}*/
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function deleteRecord(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$notices=$this->db->get_where('notice',array('notice_and_regions_id'=>$edit_id))->result();
			foreach($notices as $notice){
				$this->db->where('id',$notice->id)->delete('notice');
				$this->db->insert('deleted_notice',array('notice_id'=>$notice->id,'created_date'=>date('Y-m-d H:i:s')));
			}
			$this->db->where('id',$edit_id)->delete('notice_and_region_mapping');
		}
	}
}
?>