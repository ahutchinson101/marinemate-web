<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('api_model');
		set_time_limit(0);
		ini_set('memory_limit','-1');
	}
	public function index(){
		$response=array();
		$api_access=false;
		$last_requested_date=($this->input->post('last_requested_date')!="")?$this->input->post('last_requested_date'):'';
		$api_users=($this->input->post('api_users')!="")?$this->input->post('api_users'):'';
		$api_password=($this->input->post('api_password')!="")?$this->input->post('api_password'):'';
		$dv=isset($_POST['dv'])?$_POST['dv']:'';
		if($this->api_model->GetApi($api_users,$api_password)==TRUE){
			/* add update array*/
			$region_master = $this->api_model->GetRegionsMaster($last_requested_date);
			//$region_kml_files=$this->api_model->GetRegionsKmlFiles($last_requested_date);
			
			if(strlen($dv)>0 && $dv=='v1'){
				$places = $this->api_model->GetPlacesdv1($last_requested_date);
			}else{
				$places = $this->api_model->GetPlaces($last_requested_date);
			}
			if(strlen($dv)>0 && $dv=='v1'){
				$place_images = $this->api_model->GetPlaceImages($last_requested_date); //21 11 2014 palce images
			}
			
			$notice = $this->api_model->GetNotice($last_requested_date);
			$rules=$this->api_model->GetRules($last_requested_date);
			$marker=$this->api_model->GetMarker($last_requested_date);
			$sections=$this->api_model->GetSections($last_requested_date);
			$response['Result']='Ok';
			$response['last_response_date']=date('Y-m-d H:i:s');
			
			if(count($region_master)>0){
				$response['region_master']=$region_master;
			}
			/*if(count($region_kml_files)>0){
				$response['region_kml_files']=$region_kml_files;
			}*/
			if(count($places)>0){
				$response['places']=$places;
			}
			if(count($notice)>0){
				$response['notice']=$notice;
			}
			if(count($rules)>0){
				$response['rules']=$rules;
			}
			if(count($marker)>0){
				$response['marker']=$marker;
			}
			if(count($sections)>0){
				$response['sections']=$sections;
			}
			if(count($region_master)>0){
				$region_coordinates=array();
				$regions_id=array();
				foreach ($region_master as $regions){
					$regions_id[]=$regions->id;
				}
				$region_coordinates=$this->api_model->GetRegionCoordinates($regions_id);
				if(count($region_coordinates)>0){
					$response['regions_coordinates']=$region_coordinates;
				}
			}
			/* add update array*/
			/* deleted array*/
			$deleted_region_master=array();
			//$deleted_region_kml_files=array();
			$deleted_places=array();
			$deleted_notice=array();
			$deleted_rules=array();
			$deleted_marker=array();
			$deleted_sections=array();
			$deleted_region_master=$this->api_model->GetDeletedRegionsMaster($last_requested_date);
			//$deleted_region_kml_files=$this->api_model->GetDeletedRegionKmlFiles($last_requested_date);
			$deleted_places=$this->api_model->GetDeletedPlaces($last_requested_date);
			if(strlen($dv)>0 && $dv=='v1'){
				$deleted_place_images=$this->api_model->GetDeletedPlaceImages($last_requested_date);   //21 11 2014 deleted place images
			}
			$deleted_notice=$this->api_model->GetDeletedNotice($last_requested_date);
			$deleted_rules=$this->api_model->GetDeletedRules($last_requested_date);
			$deleted_marker=$this->api_model->GetDeletedMarker($last_requested_date);
			$deleted_sections=$this->api_model->GetDeletedSections($last_requested_date);
			
			if(count($deleted_region_master)>0){
				$deleted_region_master1=array();
				foreach ($deleted_region_master as $deleted_region_master){
					$deleted_region_master1[] = $deleted_region_master->region_id;
				}
				$response['deleted_region_master']=$deleted_region_master1;
			}
			/*if(count($deleted_region_kml_files)>0){
				$deleted_region_kml_files1=array();
				foreach ($deleted_region_kml_files as $deleted_region_kml_files){
					$deleted_region_kml_files1[]=$deleted_region_kml_files->kml_id;
				}
				$response['deleted_region_kml_files']=$deleted_region_kml_files1;
			}*/
			if(count($deleted_places)>0){
				$deleted_places1=array();
				foreach ($deleted_places as $deleted_places){
					$deleted_places1[]=$deleted_places->place_id;
				}
				$response['deleted_places']=$deleted_places1;
			}
			if(strlen($dv)>0 && $dv=='v1'){
				if(count($deleted_place_images)>0){
					$deleted_place_images1=array();
					foreach ($deleted_place_images as $deleted_place_images){
						$deleted_place_images1[]=$deleted_place_images->image;
					}
					$response['deleted_place_images']=$deleted_place_images1;
				}
			}
			if(count($deleted_notice)>0){
				$deleted_notice1=array();
				foreach ($deleted_notice as $deleted_notice){
					$deleted_notice1[]=$deleted_notice->notice_id;
				}
				$response['deleted_notice']=$deleted_notice1;
			}
			if(count($deleted_rules)>0){
				$deleted_rules1=array();
				foreach ($deleted_rules as $deleted_rules){
					$deleted_rules1[]=$deleted_rules->rules_id;
				}
				$response['deleted_rules']=$deleted_rules1;
			}
			if(count($deleted_marker)>0){
				$deleted_marker1=array();
				foreach ($deleted_marker as $deleted_marker){
					$deleted_marker1[] = $deleted_marker->marker_id;
				}
				$response['deleted_marker']=$deleted_marker1;
			}
			if(count($deleted_sections)>0){
				$deleted_sections1=array();
				foreach ($deleted_sections as $deleted_sections){
					$deleted_sections1[] = $deleted_sections->sections_id;
				}
				$response['deleted_sections']=$deleted_sections1;
			}
			/* deleted array*/
			
			$files_to_exclude_from_cloude_sync = array();
			
			if(strlen($dv)>0 && $dv=='v1'){
				if(count($place_images)>0){
					foreach ($place_images as $placeimg){
						if($placeimg->image!="" && is_file(FCPATH.'assets/place_images/'.$placeimg->image)){
							$files_to_exclude_from_cloude_sync[] = $placeimg->image;
						}
					}
				}
			}
				
			if(count($sections)>0){
				foreach ($sections as $kml){
					if($kml->kmlfiles!="" && is_file(FCPATH.'assets/regions/kmls/'.$kml->kmlfiles)){
						$files_to_exclude_from_cloude_sync[] = $kml->kmlfiles;
					}
				}
			}
				
			if(count($marker)>0){
				foreach ($marker as $marker_files){
					if($marker_files->marker_icon1!="" && is_file(FCPATH.'assets/markers/'.$marker_files->marker_icon1)){
						$files_to_exclude_from_cloude_sync[] = $marker_files->marker_icon1;
					}
					if($marker_files->marker_icon2!="" && is_file(FCPATH.'assets/markers/'.$marker_files->marker_icon2)){
						$files_to_exclude_from_cloude_sync[] = $marker_files->marker_icon2;
						$twox=explode('.', $marker_files->marker_icon1);
						$files_to_exclude_from_cloude_sync[] = $twox[0].'@2x.'.$twox[count($twox)-1];
					}
				}
			}
			
			if(count($files_to_exclude_from_cloude_sync)>0){
				$response['files_to_exclude_from_cloud']=$files_to_exclude_from_cloude_sync;
			}
			
			$filename = time().".txt";
			$string = json_encode($response);
			$fp = fopen(FCPATH.$filename, "a");
			fwrite($fp, $string);
			fclose($fp);
			$this->load->helper('createzipfile');
			$createzipfile = new createzipfile_helper();
			$createzipfile->addFile(file_get_contents(FCPATH.$filename), "response.txt");
			
			/* file array*/
			/*if(count($region_master)>0){
				foreach ($region_master as $region_files){
					if($region_files->border_coordinate_file!=""){
						$path=FCPATH.'assets/regions/csv/'.$region_files->border_coordinate_file;
						$createzipfile->addFile(file_get_contents($path), $region_files->border_coordinate_file);
					}
				}
			}*/
			
			/*if(count($region_kml_files)>0){
				foreach ($region_kml_files as $kml){
					if($kml->kml_files!=""){
						$kml_path=FCPATH.'assets/regions/kmls/'.$kml->kml_files;
						$createzipfile->addFile(file_get_contents($kml_path), $kml->kml_files);
					}
				}
			}*/
			if(strlen($dv)>0 && $dv=='v1'){
				if(count($place_images)>0){
					foreach ($place_images as $placeimg){
						if($placeimg->image!="" && is_file(FCPATH.'assets/place_images/'.$placeimg->image)){
							$image_path=FCPATH.'assets/place_images/'.$placeimg->image;
							$createzipfile->addFile(file_get_contents($image_path), $placeimg->image);
						}
					}
				}
			}
			
			if(count($sections)>0){
				foreach ($sections as $kml){
					if($kml->kmlfiles!="" && is_file(FCPATH.'assets/regions/kmls/'.$kml->kmlfiles)){
						$kml_path=FCPATH.'assets/regions/kmls/'.$kml->kmlfiles;
						$createzipfile->addFile(file_get_contents($kml_path), $kml->kmlfiles);
					}
				}
			}
			
			if(count($marker)>0){
				foreach ($marker as $marker_files){
					if($marker_files->marker_icon1!="" && is_file(FCPATH.'assets/markers/'.$marker_files->marker_icon1)){
						$marker_files_path=FCPATH.'assets/markers/'.$marker_files->marker_icon1;
						$createzipfile->addFile(file_get_contents($marker_files_path), $marker_files->marker_icon1);
					}
					if($marker_files->marker_icon2!="" && is_file(FCPATH.'assets/markers/'.$marker_files->marker_icon2)){
						$marker_files_path2=FCPATH.'assets/markers/'.$marker_files->marker_icon2;
						$createzipfile->addFile(file_get_contents($marker_files_path2), $marker_files->marker_icon2);
						$twox=explode('.', $marker_files->marker_icon1);
						$createzipfile->addFile(file_get_contents($marker_files_path2),$twox[0].'@2x.'.$twox[count($twox)-1]);
					}
				}
			}
			/* file array*/
			
			$rand = rand(0, 9999);
			$zipName = md5(time().$rand).".zip";
			$fd=fopen($zipName, "wb");
			$out=fwrite($fd,$createzipfile->getZippedfile());
			fclose($fd);
			$createzipfile->forceDownload($zipName);
			@unlink($zipName);
			@unlink(FCPATH.$filename);
			exit;
		}else{
			echo "Error: API Username and password is invalide";
		}
	}
	public function exportskml(){
		exit;
		$kmlsData=$this->db->query("SELECT * FROM sections WHERE kmlfiles!='' ORDER BY row asc,col asc")->result();
		$filename="sectionskml.csv";
		$handler=fopen(FCPATH.$filename, "a+");
		fputcsv($handler, array('Regions','Row','Col','kml'));
		$this->load->helper('createzipfile');
		$createzipfile = new createzipfile_helper();
		foreach($kmlsData as $kml){
			$kml_path=FCPATH.'assets/regions/kmls/'.$kml->kmlfiles;
			$kmlfilename='R'.$kml->row.'-C'.$kml->col.'.kml';
			$createzipfile->addFile(file_get_contents($kml_path), $kmlfilename);	
			$regionName=$this->getRegionName($kml->region_id);
			fputcsv($handler, array($regionName,$kml->row,$kml->col,$kmlfilename));
		}
		fclose($handler);
		$createzipfile->addFile(file_get_contents(FCPATH.$filename), "sectionskml.csv");
		$rand = rand(0, 9999);
		$zipName = md5(time().$rand).".zip";
		$fd=fopen($zipName, "wb");
		$out=fwrite($fd,$createzipfile->getZippedfile());
		fclose($fd);
		$createzipfile->forceDownload($zipName);
		@unlink($zipName);
		@unlink(FCPATH.$filename);
		exit;
	}
	public function ImportKml(){
		exit;
		$dir=opendir(FCPATH.'assets/regions/New-KML/');
		$notUpdated=array();
		if($dir){
			
			while(($f=readdir($dir))!==false){
				if ($f=='.' || $f == '..' || $f == '.svn' || $f == '._.DS_Store' || $f == '.DS_Store' ) {
					continue;	
				}else{
					$rowcol=explode('.kml',$f);
					$namep = $rowcol[0];
					$row = substr($namep,0,3);
					if(substr($row,strlen($row)-1,strlen($row))=='-')
					{
						$row = substr($namep,0,2);
						$col = substr($namep,4,strlen($namep));
						
					}
					else if(substr($row,strlen($row)-1,strlen($row))=='C'){
						$row = substr($namep,0,2);
						$col = substr($namep,3,strlen($namep));						
					}
					else
					{
						$col = substr($namep,4,strlen($namep));
						if(substr($col,0,1)=='-')
						{
							$col = substr($namep,5,strlen($namep));
						}
					}
					
					$row=trim(str_replace('R','',$row));
					$col=trim(str_replace('C','',$col));
					
					/*$newrowcol=explode('-',$rowcol[0]);
					$row=trim(str_replace('R','',$newrowcol[0]));
					$col=trim(str_replace('C','',$newrowcol[1]));*/
					$query=$this->db->query("select * from `sections` WHERE `row`='".$row."' AND `col`='".$col."' AND kmlfiles!=''");
					if($query->num_rows()>0){
						$data=$query->row();
						copy(FCPATH.'assets/regions/New-KML/'.$f,FCPATH.'assets/regions/kmls/'.$data->kmlfiles);
					}else{
						$notUpdated[]=$f;	
					}
				}
			}	
			closedir( $dir );	
		}
		print_r($notUpdated);
	}
	function getRegionName($id){
		$ids=explode(',',$id);
		$this->db->where_in('id',$ids);
		$query=$this->db->get('region_master')->result();
		$region="";
		foreach($query as $d){
			$region.=$d->region_name.',';
		}
		return substr($region,0,-1);
	}
	public function mmsurvey(){
		$requestData=json_decode($this->input->post('requestData',true));
		$response=array();
		if($this->api_model->GetApi($requestData->api_users,$requestData->api_password)==TRUE){
			$data=array();
			$data['main_region']=$requestData->region;
			$data['age_group']=$requestData->age_group;
			$data['main_type_vessel']=$requestData->type_of_vessel;
			$data['main_reason_vessel']=$requestData->reason_of_vessel;
			$data['safety_equipment']=$requestData->safety_equipment;
			$data['created_date']=date('Y-m-d H:i:s');
			if($this->api_model->AddMMSurvey($data)==TRUE){
				$response['Result']="Ok";
				$response['message']="Success";
			}else{
				$response['Result']="Fail";
				$response['message']="Error";
			}
		}else{
			$response['Result']="Fail";
			$response['message']="Api Username/password is wrong";
		}
		echo json_encode($response);exit;
	}
	public function exportsPlace(){
		exit;
		$PlacesData=$this->db->query("SELECT p.* ,r.region_name as region_name,m.marker_type as marker_type FROM places p LEFT JOIN region_master r ON r.id=p.region_id LEFT JOIN marker m ON m.id=p.marker_id")->result();
		$filename="Pleaces.csv";
		$handler=fopen(FCPATH.$filename, "a+");
		fputcsv($handler, array('place_name','latitude','longitude','place_description','region_name','marker_type'));
		$this->load->helper('createzipfile');
		$createzipfile = new createzipfile_helper();
		
		foreach($PlacesData as $place){
			fputcsv($handler, array($place->place_name,$place->latitude,$place->longitude,$place->place_description,$place->region_name,$place->marker_type));
		}
		
		fclose($handler);
		$createzipfile->addFile(file_get_contents(FCPATH.$filename), "Pleaces.csv");
		$rand = rand(0, 9999);
		$zipName = md5(time().$rand).".zip";
		$fd=fopen($zipName, "wb");
		$out=fwrite($fd,$createzipfile->getZippedfile());
		fclose($fd);
		$createzipfile->forceDownload($zipName);
		@unlink($zipName);
		@unlink(FCPATH.$filename);
		exit;
	}
	
	public function importPlace(){
		exit;
		$this->load->model('places_model');
		$this->load->helper('csvreader');
		$csvreader=new CSVReader();
		$csvData=$csvreader->parse_file(FCPATH.'assets/place/places.csv');
		foreach($csvData as $cd){
			$data=array();
			$data['place_name']=$cd['Name'];
			$data['region_id']=trim($cd['region']);
			$data['marker_id']=trim($cd['marker']);
			$data['latitude']=$cd['Lat'];
			$data['longitude']=$cd['Long'];
			$data['place_description']=utf8_decode($cd['Description']);
			$data['created_date']=date('Y-m-d H:i:s');
			$this->places_model->AddNewPlace($data);
			echo $this->db->insert_id()."<br/>";
		}
	}
	public function deletePlace(){
		exit;
		$regionID = 4;
		if(intval($regionID)){
			$Places=$this->db->get_where('places',array('region_id'=>4))->result();
			foreach($Places as $pl){
				
				$notices=$this->db->get_where('notice',array('place_id'=>$pl->id));
				if($notices->num_rows()>0){
					$notice=$notices->result();
					foreach ($notice as $no){
						$this->db->insert('deleted_notice',array('notice_id'=>$no->id,'created_date'=>date('Y-m-d H:i:s')));
					}
				}
				$this->db->where('place_id',$pl->id);
				$this->db->delete('notice');
				
				$rules=$this->db->get_where('rules',array('place_id'=>$pl->id));
				
				if($rules->num_rows()>0){
					$rule=$rules->result();
					foreach ($rule as $ru){
						$this->db->insert('deleted_rules',array('rules_id'=>$ru->id,'created_date'=>date('Y-m-d H:i:s')));
					}
				}
				$this->db->where('place_id',$pl->id);
				$this->db->delete('rules');
				$this->db->insert("deleted_places",array("place_id"=>$pl->id,'created_date'=>date('Y-m-d H:i:s')));
			
			$this->db->where('id',$pl->id);
			$this->db->delete('places');	
				}
				
			
			
		}
	}
}
