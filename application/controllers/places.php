<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Places extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		/*if(is_admin()==FALSE){
			redirect('notice');
		}*/
		$this->load->model('places_model');
		$this->lang->load('admin','english');
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->places_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->places_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$regions=$this->places_model->GetRegions();
		$markers=$this->places_model->GetMarkers();
		$data['regions']=$regions;
		$data['markers']=$markers;
		$this->load->view('places',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function all(){
		$aColumns = array('place_name','region_id','marker_id','latitude','longitude','place_description','image','id');
		$sIndexColumn = "id";
		$sTable = "places";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		$filter_place_regions=$this->input->post('filter_place_regions',true);
		$filter_place=$this->input->post('filter_place',true);
		$filter_search=$this->input->post('filter_search',true);
		
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		if($filter_search!=""){
			$this->db->like('place_name', $this->db->escape_like_str($filter_search));
			$this->db->or_like('place_description',$this->db->escape_like_str($filter_search));
			$this->db->or_like("latitude",$this->db->escape_like_str($filter_search));
			$this->db->or_like("longitude",$this->db->escape_like_str($filter_search));
		}
		if($filter_place_regions!=""){
			$this->db->where('region_id',$filter_place_regions);
		}
		if($filter_place!=""){
			$this->db->like('place_name',$this->db->escape_like_str($filter_place));
		}
		if(is_admin()==FALSE){
			if($this->session->userdata('region_id')!=0){
				$this->db->having('region_id',$this->session->userdata('region_id'));
				$this->db->or_having('region_id',0);
			}
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		//echo  $this->db->last_query();exit;
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		//$iTotal = $this->db->count_all($sTable);
		if(is_admin()==FALSE){
			if($this->session->userdata('region_id')!=0){
			$iTotal = $this->db->query("SELECT count(id) as cnt FROM ".$sTable." WHERE `region_id` IN(".$this->session->userdata('region_id').")")->row()->cnt;
			}else{
				$iTotal = $this->db->count_all($sTable);
			}
		}else{
			$iTotal = $this->db->count_all($sTable);
		}
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'place_name' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if( $aColumns[$i] == 'region_id' ){
					$row[] = $this->places_model->GetRegionName($aRow[ $aColumns[$i] ]);
				}
				if($aColumns[$i]=='marker_id'){
					$row[]=$this->places_model->GetMarkersName($aRow[ $aColumns[$i] ]);
				}
				if($aColumns[$i] == 'latitude'){
					$row[] = $aRow['latitude'].','.$aRow['longitude'];
				}
				if($aColumns[$i] == 'place_description'){
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if($aColumns[$i] == 'image'){
					if($aRow[ $aColumns[$i] ]!=''){
					$row[] = '<img class="place_image" width="60px" src="'.INCLUDE_URL.'assets/place_images/'.$aRow[ $aColumns[$i] ].'" alt="'.$aRow[ $aColumns[$i] ].'" title="'.$aRow[ $aColumns[$i] ].'">';;
					}else{
						$row[]='';
					}
				}
			}
			
			if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="editplaces" data-ID="'.$aRow['id'].'" marker="'.$aRow['marker_id'].'" regions="'.$aRow['region_id'].'" latitude="'.$aRow['latitude'].'" longitude="'.$aRow['longitude'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deleteplaces" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}else{
				$row[] = '<a href="javascript:void(0);" id="editplaces" data-ID="'.$aRow['id'].'" marker="'.$aRow['marker_id'].'" regions="'.$aRow['region_id'].'" latitude="'.$aRow['latitude'].'" longitude="'.$aRow['longitude'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deleteplaces" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function addNew(){
		$action=strtolower($this->input->post('action'));
		switch ($action){
			case 'add':
				$config['upload_path'] = 'assets/place_images/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name']=true;
				$this->load->library('upload', $config);
				
				$place_name=$this->input->post('place_name',true);
				$region=$this->input->post('regions',true);
				$latitude=$this->input->post('latitude',true);
				$longitude=$this->input->post('longitude',true);
				$description=$this->input->post('description',true);
				$marker=$this->input->post('marker',true);
				if($place_name==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('place_name_error');
					echo  json_encode($response);exit;
				}else if($region==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('region_error');
					echo  json_encode($response);exit;
				}else if($latitude==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('latitude_error');
					echo  json_encode($response);exit;
				}else if($longitude==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('longitude_error');
					echo  json_encode($response);exit;
				}else if($description==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('description_error');
					echo  json_encode($response);exit;
				}else if($marker==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('marker_error');
					echo  json_encode($response);exit;
				}else if(isset($_FILES["place_image"]["name"]) && $_FILES["place_image"]["name"] != "" && !$this->upload->do_upload('place_image')){ 
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->upload->display_errors();
					echo  json_encode($response);
					exit;
				}else{
					$data=array();
					if(isset($_FILES["place_image"]["name"]) && $_FILES["place_image"]["name"] != ""){
						$file_info=$this->upload->data();
						$filename=$file_info['file_name'];
						$data['image_updated']=date('Y-m-d H:i:s');
					}else{
						$filename='';
					}
					
					$data['place_name']=$place_name;
					$data['region_id']=$region;
					$data['marker_id']=$marker;
					$data['latitude']=$latitude;
					$data['longitude']=$longitude;
					$data['place_description']=$description;
					$data['image']=$filename;
					$data['created_date']=date('Y-m-d H:i:s');
					if($this->places_model->AddNewPlace($data)==TRUE){
						$response=array();
						$response['result']=$this->lang->line('success');
					}else{
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('place_not_add');
					}
					echo  json_encode($response);
				}
			break;
			case 'edit':
				$edit_id=$this->input->post('edit_id');
				if(intval($edit_id)){
					$config['upload_path'] = 'assets/place_images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['encrypt_name']=true;
					$this->load->library('upload', $config);
					$place_name=$this->input->post('place_name',true);
					$region=$this->input->post('regions',true);
					$latitude=$this->input->post('latitude',true);
					$longitude=$this->input->post('longitude',true);
					$description=$this->input->post('description',true);
					$marker=$this->input->post('marker',true);
					$old_image=$this->input->post('old_image');
					if($place_name==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('place_name_error');
						echo  json_encode($response);exit;
					}else if($region==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('region_error');
						echo  json_encode($response);exit;
					}else if($latitude==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('latitude_error');
						echo  json_encode($response);exit;
					}else if($longitude==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('longitude_error');
						echo  json_encode($response);exit;
					}else if($description==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('description_error');
						echo  json_encode($response);exit;
					}else if($marker==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('marker_error');
						echo  json_encode($response);exit;
					}else if(isset($_FILES["place_image"]["name"]) && $_FILES["place_image"]["name"] != "" && !$this->upload->do_upload('place_image')){ 
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->upload->display_errors();
						echo  json_encode($response);
						exit;
					}else{
						$data=array();
						if(isset($_FILES["place_image"]["name"]) && $_FILES["place_image"]["name"] != ""){
							$file_info=$this->upload->data();
							$filename=$file_info['file_name'];
							$data['image_updated']=date('Y-m-d H:i:s');
							
							if($old_image!=''){
								$data1=array();
								$data1['image']=$old_image;
								$data1['deleted_date']=date('Y-m-d H:i:s');
								$this->places_model->DeletedPlaceImage($data1);
								
								if($old_image!="" && is_file(FCPATH.'assets/place_images/'.$old_image)){
									@unlink(FCPATH.'assets/place_images/'.$old_image);
								}
							}
						}else{
							$filename=$old_image;
						}
						
						$data['place_name']=$place_name;
						$data['region_id']=$region;
						$data['marker_id']=$marker;
						$data['latitude']=$latitude;
						$data['longitude']=$longitude;
						$data['place_description']=$description;
						$data['image']=$filename;
						$data['updated_date']=date('Y-m-d H:i:s');
						$this->places_model->UpdatePlace($data,$edit_id);
						$response=array();
						$response['result']=$this->lang->line('success');
						echo json_encode($response);exit;
					}
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('place_not_update');
					echo  json_encode($response);exit;
				}
				
			break;
		}
	}
	public function deleteRecord(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$place=$this->places_model->GetPlaceImage($edit_id);
			if($place->image!=''){
				$data1=array();
				$data1['image']=$place->image;
				$data1['deleted_date']=date('Y-m-d H:i:s');
				$this->places_model->DeletedPlaceImage($data1);
					
				if($place->image!="" && is_file(FCPATH.'assets/place_images/'.$place->image)){
					@unlink(FCPATH.'assets/place_images/'.$place->image);
				}
			}
			$this->db->where('id',$edit_id);
			$this->db->limit(1,0);
			$this->db->delete('places');
			$this->db->insert("deleted_places",array("place_id"=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
			
			$notices=$this->db->get_where('notice',array('place_id'=>$edit_id));
			if($notices->num_rows()>0){
				$notice=$notices->result();
				foreach ($notice as $no){
					$this->db->insert('deleted_notice',array('notice_id'=>$no->id,'created_date'=>date('Y-m-d H:i:s')));
				}
			}
			$this->db->where('place_id',$edit_id);
			$this->db->delete('notice');
			
			$rules=$this->db->get_where('rules',array('id'=>$edit_id));
			
			if($rules->num_rows()>0){
				$rule=$rules->result();
				foreach ($rule as $ru){
					$this->db->insert('deleted_rules',array('rules_id'=>$ru->id,'created_date'=>date('Y-m-d H:i:s')));
				}
			}
			$this->db->where('place_id',$edit_id);
			$this->db->delete('rules');
			
		}
	}
	
	public function deleteOldImage(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$place=$this->places_model->GetPlaceImage($edit_id);
			if($place->image!=''){
				$data1=array();
				$data1['image']=$place->image;
				$data1['deleted_date']=date('Y-m-d H:i:s');
				$this->places_model->DeletedPlaceImage($data1);
					
				if($place->image!="" && is_file(FCPATH.'assets/place_images/'.$place->image)){
					@unlink(FCPATH.'assets/place_images/'.$place->image);
				}
			}
			$data=array();
			$data['image']='';
			$data['updated_date']=date('Y-m-d H:i:s');
			$this->places_model->UpdatePlace($data,$edit_id);
		}
	}
}

?>