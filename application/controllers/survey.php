<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Survey extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		if(is_admin()==FALSE){
			redirect('notice');
		}
		$this->load->model('survey_model');
		$this->lang->load('admin','english');
	}
	
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->survey_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->survey_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$this->load->view('survey');
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	
	public function all(){
		$aColumns = array('main_region','age_group','main_type_vessel','main_reason_vessel','safety_equipment','id');
		$sIndexColumn = "id";
		$sTable = "mm_survey";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'main_region' ){
					$row[] = $this->survey_model->GetRegionName($aRow[$aColumns[$i]]);
				}
				if ( $aColumns[$i] == 'age_group' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if ( $aColumns[$i] == 'main_type_vessel' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if ( $aColumns[$i] == 'main_reason_vessel' )	{
					$row[] = $aRow[$aColumns[$i]];
				}
				if ( $aColumns[$i] == 'safety_equipment' )	{
					$row[] = $this->survey_model->GetSafetyEquipment($aRow[ $aColumns[$i] ]);
				}
				
			}
			if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="deletesurvey" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function exports(){
		
		$AllSurvey=$this->survey_model->GetAllSurvey();
		$filename=time().'_exports.csv';
		/*if(is_file($filename)){
			@unlink($filename);
		}*/
		$handler=fopen(FCPATH.$filename, "a+");
		$column_name=array('main region','age group','main type vessel','main reason vessel','Life jackets and Personal Flotation Devices (PFDs)','Communication equipment','Navigation','Anchor','Bailing system','Alternative power','Knife','First aid kit','Rope','Torch','Fire Extinguishers','Boat hook','Protective clothing','Throwing line','Radar reflector','device id');
		fputcsv($handler, $column_name);
		
		if(count($AllSurvey)>0){
			foreach($AllSurvey as $surveyData){
				$ColumnData=array();
				$ColumnData[]=$this->survey_model->GetRegionName($surveyData->main_region);
				$ColumnData[]=$surveyData->age_group;
				$ColumnData[]=$surveyData->main_type_vessel;
				$ColumnData[]=$surveyData->main_reason_vessel;
				$AllSafetyEquipment=$this->survey_model->GetAlllSeftyEquipment();
				foreach($AllSafetyEquipment as $sequip){
					$ids=explode(',', $surveyData->safety_equipment);
					if(in_array($sequip->id, $ids)){
						$ColumnData[]="Yes";
					}else{
						$ColumnData[]="No";
					}
				}
				$ColumnData[]=$surveyData->device_id;
				fputcsv($handler, $ColumnData);
			}
		}
		
		fclose($handler);
		/*header('Content-type: application/csv');
		header('Content-Disposition: inline; filename="exports.csv"');
		readfile($filename);
		exit;*/
		$this->load->helper('createzipfile');
		$createzipfile = new createzipfile_helper();
		$createzipfile->addFile(file_get_contents(FCPATH.$filename),$filename);
		$rand = rand(0, 9999);
		$zipName = md5(time().$rand).".zip";
		$fd=fopen($zipName, "wb");
		$out=fwrite($fd,$createzipfile->getZippedfile());
		fclose($fd);
		$createzipfile->forceDownload($zipName);
		@unlink($zipName);
		@unlink(FCPATH.$filename);
		exit;
	}
	public function deleteRecord(){
		if(is_admin()==TRUE){
			$edit_id = $this->input->post('edit_id');
			if(intval($edit_id)){
				$this->db->where('id',$edit_id);
				$this->db->delete('mm_survey');
			}
		}
	}
}