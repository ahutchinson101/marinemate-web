<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kmlfiles extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		if(is_admin()==FALSE){
			redirect('notice');
		}
		redirect('notice');
		$this->load->model('kmlfiles_model','kml');
		
			$this->lang->load('admin','english');
		
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->kml->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->kml->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$data=array();
		$data['regions']=$this->kml->GetAllRegions();
		$this->load->view('kmlfiles',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function GetRegions(){
		$regions=$this->kml->GetAllRegions();
		$response=array();
		if(count($regions)){
			$response['result']=$this->lang->line('success');
			$response['data']=$regions;
		}else{
			$response['result']=$this->lang->line('error');
			$response['message']='No Regions Availabel';
		}
		echo json_encode($response);exit;
	}
	public function all(){
		$aColumns = array('region_id','kml_files','id');
		$sIndexColumn = "id";
		$sTable = "region_kml_files";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if( $aColumns[$i] == 'region_id' ){
					$row[] = $this->kml->GetRegionName($aRow[ $aColumns[$i] ]);
				}
				if ( $aColumns[$i] == 'kml_files' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="deletekemlfiles" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function addNew(){
		$config['upload_path'] = 'assets/regions/kmls/';
		$config['allowed_types'] ='kml';
		//$config['file_name'] = $this->input->post('regions').'_'.rand(0,9999).'_kmlfiles';
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('kml_files')){
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->upload->display_errors();
		}else{
			$file_info=$this->upload->data();
			$filename=$file_info['file_name'];
			$data=array('region_id'=>$this->input->post('regions'),'kml_files'=>$filename,'created_date'=>date('Y-m-d H:i:s'));
			if($this->kml->AddNewKmlFiles($data)==TRUE){
				$response=array();
				$response['result']=$this->lang->line('success');
			}else{
				$response=array();
				$response['result']=$this->lang->line('error');
				$response['message']=$this->lang->line('kml_not_add');
			}
		}
		
		echo  json_encode($response);
	}
	public function deleteRecord(){
		if(is_admin()==TRUE){
			$edit_id = $this->input->post('edit_id');
			if(intval($edit_id)){
				$kmlsfiledata=$this->kml->getKmlFiles($edit_id);
				if(count($kmlsfiledata)>0){
					if($kmlsfiledata->kml_files!="" && is_file(FCPATH.'assets/regions/kmls/'.$kmlsfiledata->kml_files)){
						@unlink(FCPATH.'assets/regions/kmls/'.$kmlsfiledata->kml_files);
					}
					$this->db->where('id',$edit_id);
					$this->db->delete('region_kml_files');
					
					$this->db->insert('deleted_region_kml_files',array('kml_id'=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
				}
			}
		}
	}
}