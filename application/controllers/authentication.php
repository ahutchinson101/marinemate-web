<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Authentication extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('authentication_model','auth');
		$this->lang->load('admin','english');
	}
	public function index(){
		if($this->logged_in()){
			redirect('notice',true);
		}else{
			$this->login();
		}
	}
	public function login(){
		$this->form_validation->set_rules('username', $this->lang->line('username'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('password'), 'trim|required|xss_clean');
		if($this->form_validation->run() == FALSE){
			if((array_key_exists('login_attempts', $_COOKIE)) && ($_COOKIE['login_attempts'] >= 50)){
				echo $this->lang->line('max_login_attempts_error');
			}else{
				$data=array();
				$data['site_name'] = $this->lang->line('site_name');
				$this->load->view('login',$data);
			}
		}else{
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			if(!$this->_verify_details($username, $password)){
				$redirect = 'authentication';
				redirect($redirect);
			}
			$row=$this->auth->get_user($username);
			$data = array('user_id' => $row['id'],
					'email' => $row['email'],
					'role' => $row['role'],
					'region_id' => $row['region_id'],
					'logged_in' => TRUE,
					'name'=>$row['name'],
					'username'=>$row['username']
					);
			$this->session->set_userdata($data);
			redirect('notice');
		}
	}
	public function myAccount(){
		if($this->logged_in()){
			$data_header=array();
			$users=$this->auth->get_currentUser($this->session->userdata('user_id'));
			if(is_admin()==TRUE){
				$regions=$this->auth->GetRegion();
			}else{
				$regions=$this->auth->GetRegion($users->region_id);
			}
			$data['regions']=$regions;	
			$data['users']=$users;
			$this->load->view('myaccount',$data);
		}else{
			redirect('authentication',true);
		}
	}
	public function UpdateAccount(){
		if($this->input->post('myname')==""){
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('name_error');
			echo  json_encode($response);exit;
		}else if($this->input->post('myregions')==""){
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('region_error');
			echo  json_encode($response);exit;
		}else if($this->input->post('mypassword')!="" && $this->input->post('mypasswordagain')!="" && $this->input->post('mypassword')!=$this->input->post('mypasswordagain')){
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('password_not_match');
			echo  json_encode($response);exit;
		}else{
			$data=array('name'=>$this->input->post('myname'),'region_id'=>$this->input->post('myregions'),'email'=>$this->input->post('myemail'));
			if(($this->input->post('mypassword')==$this->input->post('mypasswordagain')) && ($this->input->post('mypassword')!="" && $this->input->post('mypasswordagain')!="")){
				$data['password']=md5($this->input->post('mypassword'));
			}
			$this->db->where('id',$this->session->userdata('user_id'));
			$this->db->update('users',$data);
			$response=array();
			$response['result']=$this->lang->line('success');
			echo  json_encode($response);
		}
	}
	function _verify_details($username, $password){
		$query=$this->auth->check_user($username,$password);
		if($query->num_rows != 1){
			if(isset($_COOKIE['login_attempts']))
				$attempts = $_COOKIE['login_attempts'] + 1;
			else
				$attempts = $_COOKIE['login_attempts'] =1;
			setcookie("login_attempts", $attempts, time()+900, '/');
			$this->session->set_flashdata('login-error', $this->lang->line('login_details_error'));
			return FALSE;
		}else{
			$user = $query->row();
			if($user->active!=1){
				$this->session->set_flashdata('login-error', $this->lang->line('login_inactive_error'));
				return FALSE;
			}
		}
		return TRUE;
	}
	public function logged_in(){
		if($this->session->userdata('logged_in') == TRUE){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function logout(){
		$datame=array();
		$datame['last_login'] = date('Y-m-d h:i:s');
		$datame['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$this->db->where('username',$this->session->userdata('username'));
		$this->db->update('users',$datame);
		$redirect='authentication';
		$this->session->sess_destroy();
		redirect($redirect);
	}
}