<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regions extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('regions_model');
		$this->lang->load('admin','english');
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->regions_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->regions_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$this->load->view('regions');
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function all(){
		$aColumns = array('region_name','border_coordinate_file','id');
		$sIndexColumn = "id";
		$sTable = "region_master";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'region_name' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				else if( $aColumns[$i] == 'border_coordinate_file' ){
					if($aRow[$aColumns[$i]]!="" && is_file(FCPATH.'assets/regions/csv/'.$aRow[$aColumns[$i]])){
						$row[] = $aRow[$aColumns[$i]];
					}else{
						$row[] = "";
					}
					
				}
			}
		if(is_admin()==TRUE){
				/*$row[] = '<a href="javascript:void(0);" id="editregions" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deleteregions" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';*/
				$row[] = '<a href="javascript:void(0);" id="editregions" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function deleteRecord(){
		if(is_admin()==TRUE){
			$edit_id = $this->input->post('edit_id');
			if(intval($edit_id)){
				$regions=$this->regions_model->getRegions($edit_id);
				if(count($regions)>0){
					if($regions->border_coordinate_file!="" && is_file(FCPATH.'assets/regions/csv/'.$regions->border_coordinate_file)){
						@unlink(FCPATH.'assets/regions/csv/'.$regions->border_coordinate_file);
					}
				}
				$this->db->where('id',$edit_id);
				$this->db->limit(1,0);
				$this->db->delete('region_master');
				$this->db->insert('deleted_region_master',array('region_id'=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
				
				$regionskmlfiles=$this->regions_model->GetRegionsKMLFiles($edit_id);
				if(count($regionskmlfiles)>0){
					foreach ($regionskmlfiles as $kml){
						if($kml->kml_files!="" && is_file(FCPATH.'assets/regions/kmls/'.$kml->kml_files)){
							@unlink(FCPATH,'assets/regions/kmls/'.$kml->kml_files);
						}
					}
				}
				$this->db->where('region_id',$edit_id);
				$this->db->delete('region_kml_files');
				
				$this->db->where('region_id',$edit_id);
				$this->db->delete('notice');
				
				$places=$this->db->get_where('places',array('region_id'=>$edit_id));
				if($places->num_rows()>0){
					$place=$places->result();
					foreach ($place as $pl){
						$this->db->where('place_id',$pl->id);
						$this->db->delete('rules');
						
						/*if($pl->image!=''){
							//$data1=array();
							//$data1['image']=$place->image;
							//$data1['deleted_date']=date('Y-m-d H:i:s');
							//$this->places_model->DeletedPlaceImage($data1);
							if($pl->image!="" && is_file(FCPATH.'assets/place_images/'.$pl->image)){
								@unlink(FCPATH.'assets/place_images/'.$pl->image);
							}
						}*/
					}
				}
				$this->db->where('region_id',$edit_id);
				$this->db->delete('places');
				
				$users=$this->db->get_where('users',array('region_id'=>$edit_id));
				if($users->num_rows()>0){
					$this->db->where('region_id',$edit_id);
					$this->db->where_not_in('role','admin');
					$this->db->update('users',array('active'=>0,'region_id'=>0));
				}
				
				$this->db->where('region_id',$edit_id);
				$this->db->delete('region_coordinates');
				
				$sections=$this->db->query("SELECT * FROM `sections` WHERE FIND_IN_SET('".$edit_id."',region_id)>0");
				if($sections->num_rows()>0){
					$sectionData=$sections->result();
					foreach ($sectionData as $section){
						$regions_ids=explode(",",$section->region_id);
						if(count($regions_ids)>1){
							$temp_array=array();
							for($i=0;$i<count($regions_ids);$i++){
								if($edit_id!=$regions_ids[$i]){
									$temp_array[]=$regions_ids[$i];
								}
							}
								$this->db->update("sections",array("region_id"=>implode(',', $temp_array),'updated_date'=>date('Y-m-d H:i:s')),array('id'=>$section->id));
						}else{
							if($section->kmlfiles!="" && is_file(FCPATH.'assets/regions/kmls/'.$section->kmlfiles)){
								@unlink(FCPATH.'assets/regions/kmls/'.$section->kmlfiles);
							}
							$this->db->where('id',$section->id);
							$this->db->delete("sections");
							$this->db->insert("deleted_sections",array('sections_id'=>$section->id,'created_date'=>date("Y-m-d H:i:s")));
						}
					}
				}
			}
		}
	}
	public function addNew(){
		$action=$this->input->post('action');
		$this->load->library('csvreader');
		switch ($action){
			case 'add':
				if($this->regions_model->UinqueRegion($this->input->post('region_name'))==TRUE){
					$config['upload_path'] = 'assets/regions/csv/';
					$config['allowed_types'] ='csv';
					$config['file_name'] = $this->input->post('region_name').'_coordinatefiles';
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('csvfiles')){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->upload->display_errors();
					}else{
						$file_info=$this->upload->data();
						$filename=$file_info['file_name'];
						$data=array('region_name'=>$this->input->post('region_name'),'border_coordinate_file'=>$filename,'created_date'=>date('Y-m-d H:i:s'));
						$last_id=$this->regions_model->AddNewRegions($data);
						
						if($last_id){
							$filepath=FCPATH.'assets/regions/csv/'.$filename;
							$CsvData = $this->csvreader->parse_file($filepath);
							foreach ($CsvData as $CSV){
								if($CSV['latitude']!="" && $CSV['longitude']!=""){
									$latittude=array();
									$latittude=explode(".", $CSV['latitude']);
									if(strlen($latittude[1])>6){
										$latfloatpoint=substr($latittude[1], 0,6);
									}else{
										$latfloatpoint=$latittude[1];
									}
									$latFinalPoint=$latittude[0].".".$latfloatpoint;
									
									$longitude=array();
									$longitude=explode(".", $CSV['longitude']);
									if(strlen($longitude[1])>6){
										$longfloatpoint=substr($longitude[1], 0,6);
									}else{
										$longfloatpoint=$longitude[1];
									}
									$longFinalPoint=$longitude[0].".".$longfloatpoint;
									
									$InsertData=array('region_id'=>$last_id,'latitude'=>$latFinalPoint,'longitude'=>$longFinalPoint);
									$this->db->insert('region_coordinates',$InsertData);
								}
							}
							$response=array();
							$response['result']=$this->lang->line('success');
						}else{
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('region_not_add');
						}
					}
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('unique_region_name');
				}
				echo  json_encode($response);
				
			break;
			case 'edit':
				$edit_id=$this->input->post('edit_id');
				if(intval($edit_id)){
					$config['upload_path'] = 'assets/regions/csv/';
					$config['allowed_types'] ='csv';
					$config['file_name'] = $this->input->post('region_name').'_coordinatefiles';
					$this->load->library('upload', $config);
					$data=array();
					if(!$this->upload->do_upload('csvfiles') && (isset($_FILES['csvfiles']['name']) && $_FILES['csvfiles']['name']!="")){
						$response=array();
						$response['result']='Error';
						$response['message']=$this->upload->display_errors();
					}else{
						if(isset($_FILES['csvfiles']['name']) && $_FILES['csvfiles']['name']!=""){
							$oldfile=$this->db->get_where('region_master',array('id'=>$edit_id));
							if($oldfile->num_rows()>0){
								$olddata=$oldfile->row();
								if(is_file(FCPATH.'assets/regions/csv/'.$olddata->border_coordinate_file) && $olddata->border_coordinate_file!=""){
									@unlink(FCPATH.'assets/regions/csv/'.$olddata->border_coordinate_file);
								}
							}
							$file_info=$this->upload->data();
							$filename=$file_info['file_name'];
							$data['border_coordinate_file']=$filename;
							$filepath=FCPATH.'assets/regions/csv/'.$filename;
							
							$this->db->where('region_id',$edit_id);
							$this->db->delete('region_coordinates');
							
							$CsvData = $this->csvreader->parse_file($filepath);
							foreach ($CsvData as $CSV){
								if($CSV['latitude']!="" && $CSV['longitude']!=""){
									$latittude=array();
									$latittude=explode(".", $CSV['latitude']);
									if(strlen($latittude[1])>6){
										$latfloatpoint=substr($latittude[1], 0,6);
									}else{
										$latfloatpoint=$latittude[1];
									}
									$latFinalPoint=$latittude[0].".".$latfloatpoint;
									
									$longitude=array();
									$longitude=explode(".", $CSV['longitude']);
									if(strlen($longitude[1])>6){
										$longfloatpoint=substr($longitude[1], 0,6);
									}else{
										$longfloatpoint=$longitude[1];
									}
									$longFinalPoint=$longitude[0].".".$longfloatpoint;
									$InsertData=array('region_id'=>$edit_id,'latitude'=>$latFinalPoint,'longitude'=>$longFinalPoint);
									$this->db->insert('region_coordinates',$InsertData);
								}
							}
						}
					}
					if($this->input->post('region_name')!=""){
						$data['region_name']=$this->input->post('region_name');
						$data['updated_date']=date('Y-m-d H:i:s');
						$this->regions_model->UpdateRegions($data,$edit_id);
						$response=array();
						$response['result']=$this->lang->line('success');
					}else{
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('region_name_error');
					}
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('region_not_update');
				}
				echo  json_encode($response);
				
			break;
		}
	} 
}
