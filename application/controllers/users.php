<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		if(is_admin()==FALSE){
			redirect('notice');
		}
		$this->load->model('users_model');
		$this->lang->load('admin','english');
	
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->users_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->users_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$data=array();
		$data['regions']=$this->users_model->GetAllRegions();
		$this->load->view('users',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function addNew(){
		$action=$this->input->post('action');
		switch ($action){
			case 'add':
				$username=$this->input->post('username');
				$name=$this->input->post('name');
				$email=$this->input->post('email');
				$password=$this->input->post('password');
				$regions=$this->input->post('regions');
				$userstatus=$this->input->post('userstatus');
				if($username==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('username_error');
					echo json_encode($response);exit;
				}else if($name==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('name_error');
					echo json_encode($response);exit;
				}else if($email==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('email_error');
					echo json_encode($response);exit;
				}else if($password==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('password_error');
					echo json_encode($response);exit;
				}else if($regions==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('region_error');
					echo json_encode($response);exit;
				}else if($this->users_model->UniqueUserName($username)==FALSE){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('unique_username_error');
					echo json_encode($response);exit;
				}else{
					$data=array();
					$response=array();
					$data['username']=$username;
					$data['name']=$name;
					$data['email']=$email;
					$data['password']=md5($password);
					$data['region_id']=$regions;
					$data['active']=$userstatus;
					if($this->users_model->addUsers($data)==TRUE){
						$response['result']=$this->lang->line('success');
					}else{
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('user_not_add');
					}
					echo  json_encode($response);
				}
			break;
			case 'edit':
				$edit_id=$this->input->post('edit_id');
				if(intval($edit_id)){
					$name=$this->input->post('name');
					$email=$this->input->post('email');
					$password=$this->input->post('password');
					$regions=$this->input->post('regions');
					$userstatus=$this->input->post('userstatus');
					if($name==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('name_error');
						echo json_encode($response);exit;
					}else if($email==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('email_error');
						echo json_encode($response);exit;
					}else if($regions==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('region_error');
						echo json_encode($response);exit;
					}else{
						$data=array();
						$response=array();
						$data['name']=$name;
						$data['email']=$email;
						$data['region_id']=$regions;
						$data['active']=$userstatus;
						if($password!=""){
							$data['password']=md5($password);
						}
						if($this->users_model->UpdateUsers($data,$edit_id)==TRUE){
							$response['result']=$this->lang->line('success');
						}else{
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('user_not_update');
						}
						echo  json_encode($response);
					}
				}
			break;
		}
	}
	public function all(){
		$aColumns = array('username','name','email','region_id','active','id');
		$sIndexColumn = "id";
		$sTable = "users";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		$this->db->where_not_in('id',$this->session->userdata('user_id'));
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->query("SELECT count(id) as cnt FROM ".$sTable." WHERE `id` !=".$this->session->userdata('user_id'))->row()->cnt;
		
		//$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'username' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if ( $aColumns[$i] == 'name' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if ( $aColumns[$i] == 'email' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
				if( $aColumns[$i] == 'region_id' ){
					if($aRow[ $aColumns[$i] ]!=0){
						$row[] = $this->users_model->GetRegionName($aRow[ $aColumns[$i] ]);
					}else{
						$row[]='All';
					}
				}
				if($aColumns[$i] == 'active'){
					$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '<img src="'.INCLUDE_URL.'assets/images/false.gif" border="0" alt="Inactive">' : '<img src="'.INCLUDE_URL.'assets/images/true.gif" border="0" align="center" alt="active">';
				}
			}
			 if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="editusers" data-ID="'.$aRow['id'].'" regions="'.$aRow['region_id'].'" usersstatus="'.$aRow['active'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deleteusers" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			 }
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function deleteRecord(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$this->db->where('id',$edit_id);
			$this->db->limit(1,0);
			$query = $this->db->delete('users');
		}
	}
}