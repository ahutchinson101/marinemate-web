<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rules extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		redirect('notice');
		if(is_admin()==FALSE){
			redirect('notice');
		}
		$this->load->model('rules_model');
		$this->lang->load('admin','english');
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->rules_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->rules_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$places=$this->rules_model->GetPlaces();
		$regions=$this->rules_model->GetALlRegions();
		$data['places']=$places;
		$data['regions']=$regions;
		$this->load->view('rules',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function GetPlaces($id=0,$place_id=0){
		$places=$this->rules_model->GetRegionPlaces($id,$place_id);
		if($places){
			$response=array();
			$response['result']=$this->lang->line('success');
			$response['data']=$places;
		}else{
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('place_not');
		}
		echo json_encode($response);exit;
	}
	public function GetFiltesRegions($id=0){
		$places=$this->rules_model->GetFiltersRegionPlaces($id);
		if($places){
			$response=array();
			$response['result']=$this->lang->line('success');
			$response['data']=$places;
		}else{
			$response=array();
			$response['result']=$this->lang->line('error');
			$response['message']=$this->lang->line('place_not');
		}
		echo json_encode($response);exit;
	}
	public function all(){
		$aColumns = array('created_date','place_id','rules_title','rules_description','rules_status','id');
		$sIndexColumn = "id";
		$sTable = "rules";
		$iDisplayStart = $this->input->post('iDisplayStart', true);
		$iDisplayLength = $this->input->post('iDisplayLength', true);
		$iSortCol_0 = $this->input->post('iSortCol_0', true);
		$iSortingCols = $this->input->post('iSortingCols', true);
		$sSearch = $this->input->post('sSearch', true);
		$sEcho = $this->input->post('sEcho', true);
		$filter_place_regions=$this->input->post('filter_place_regions',true);
		$filter_place=$this->input->post('filter_place',true);
		$filter_title=$this->input->post('filter_title',true);
		$filter_search=$this->input->post('filter_search',true);
		$form_date=$this->input->post('form_date',true);
		$to_date=$this->input->post('to_date',true);
		
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		if($form_date!=""){
			$this->db->where('DATE_FORMAT(created_date,"%Y-%m-%d")>=',date('Y-m-d',strtotime($form_date)));
		}
		if($to_date!=""){
			$this->db->where('DATE_FORMAT(created_date,"%Y-%m-%d")<=',date('Y-m-d',strtotime($to_date)));
		}
		
		if($filter_search!=""){
			$this->db->or_like('rules_title', $this->db->escape_like_str($filter_search));
			$this->db->or_like('DATE_FORMAT(created_date,"%D %M %Y")', $this->db->escape_like_str($filter_search));
		}
		
		if($filter_place_regions!=""){
			$place_id=array();
			$query=$this->db->get_where('places',array('region_id'=>$filter_place_regions));
			if($query->num_rows()>0){
				foreach ($query->result() as $places){
					$place_id[]=$places->id;
				}
			}
			if(count($place_id)>0){
				$this->db->where_in('place_id',$place_id);
			}
			
		}
		if($filter_place!=""){
			$this->db->where('place_id',$filter_place);
		}
		if($filter_title!=""){
			$this->db->like('rules_title',$this->db->escape_like_str($filter_title));
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'created_date' )	{
					$row[] = date('dS F Y',strtotime($aRow[ $aColumns[$i] ]));
				}
				else if( $aColumns[$i] == 'place_id' ){
					$row[] = $this->rules_model->GetPlaceName($aRow[ $aColumns[$i] ]);
				}else if($aColumns[$i] == 'rules_title'){
					$row[] = $aRow[ $aColumns[$i] ];
				}else if($aColumns[$i] == 'rules_description'){
					$row[] = $aRow[ $aColumns[$i] ];
				}else if($aColumns[$i] == 'rules_status'){
					$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '<img src="'.INCLUDE_URL.'assets/images/false.gif" border="0" alt="Inactive">' : '<img src="'.INCLUDE_URL.'assets/images/true.gif" border="0" align="center" alt="active">';
				}
			}
			if(is_admin()==TRUE){
				$querys=$this->db->get_where('places',array('id'=>$aRow['place_id']));
				if($querys->num_rows()>0){
					$region_id=$querys->row()->region_id;
				}else{
					$region_id="";
				}
				
				$row[] = '<a href="javascript:void(0);" id="editrules" data-ID="'.$aRow['id'].'" region-id="'.$region_id.'" place="'.$aRow['place_id'].'" status="'.$aRow['rules_status'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deleterules" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function addNew(){
		$action=strtolower($this->input->post('action'));
		switch ($action){
			case 'add':
				$place=$this->input->post('place',true);
				$title=$this->input->post('title',true);
				$description=$this->input->post('description',true);
				if($place==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('place_error');
					echo  json_encode($response);exit;
				}else if($title==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('title_error');
					echo  json_encode($response);exit;
				}else if($description==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('description_error');
					echo  json_encode($response);exit;
				}else{
					$data=array();
					$data['place_id']=$place;
					$data['rules_title']=$title;
					$data['rules_description']=$description;
					$data['rules_status']=$this->input->post('rules_status',true);
					$data['created_date']=date('Y-m-d H:i:s');
					if($this->rules_model->AddNewRules($data)==TRUE){
						$response=array();
						$response['result']=$this->lang->line('success');
					}else{
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('rules_not_add');
					}
					echo  json_encode($response);
				}
				break;
			case 'edit':
				$edit_id=$this->input->post('edit_id');
				if(intval($edit_id)){
					$place=$this->input->post('place',true);
					$title=$this->input->post('title',true);
					$description=$this->input->post('description',true);
					if($place==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('place_error');
						echo  json_encode($response);exit;
					}else if($title==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('title_error');
						echo  json_encode($response);exit;
					}else if($description==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('description_error');
						echo  json_encode($response);exit;
					}else{
						$data=array();
						$data['place_id']=$place;
						$data['rules_title']=$title;
						$data['rules_description']=$description;
						$data['rules_status']=$this->input->post('rules_status',true);
						$data['updated_date']=date('Y-m-d H:i:s');
						$this->rules_model->UpdateRules($data,$edit_id);
						$response=array();
						$response['result']=$this->lang->line('success');
						echo json_encode($response);exit;
					}
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('rules_not_update');
					echo  json_encode($response);exit;
				}
	
				break;
		}
	}
	public function deleteRecord(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$this->db->where('id',$edit_id);
			$this->db->limit(1,0);
			$this->db->delete('rules');
			
			$this->db->insert('deleted_rules',array('rules_id'=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
		}
	}
}
