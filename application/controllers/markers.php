<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Markers extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		if(is_admin()==FALSE){
			redirect('notice');
		}
		$this->load->model('markers_model');
		$this->lang->load('admin','english');
		
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->markers_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->markers_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$this->load->view('markers');
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function all(){
		$aColumns = array('marker_icon1','marker_icon2','marker_type','id');
		$sIndexColumn = "id";
		$sTable = "marker";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
			}
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == 'marker_icon1' )	{
					if($aRow[ $aColumns[$i] ]!="" && is_file(FCPATH.'assets/markers/'.$aRow[$aColumns[$i]])){
						$row[]='<img src="'.base_url().'assets/markers/'.$aRow[$aColumns[$i]].'">';
					}else{
						$row[]="";
					}
				}
				if ( $aColumns[$i] == 'marker_icon2' )	{
					if($aRow[ $aColumns[$i] ]!="" && is_file(FCPATH.'assets/markers/'.$aRow[$aColumns[$i]])){
						$row[]='<img src="'.base_url().'assets/markers/'.$aRow[$aColumns[$i]].'">';
					}else{
						$row[]="";
					}
				}
				else if( $aColumns[$i] == 'marker_type' ){
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="editmarkers" data-ID="'.$aRow['id'].'" image1="'.$aRow['marker_icon1'].'" image2="'.$aRow['marker_icon2'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deletemarkers" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function addNew(){
		$action=strtolower($this->input->post('action'));
		switch ($action){
			case 'add':
				$config['upload_path'] = 'assets/markers/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name']=true;
				$this->load->library('upload', $config);
				$data=array();
				if(!$this->upload->do_upload('marker_icon1')){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->upload->display_errors();
					echo json_encode($response);exit;
				}else{
					$file_info=$this->upload->data();
					$filename=$file_info['file_name'];
					$data['marker_icon1']=$filename;
				}
				
				if(!$this->upload->do_upload('marker_icon2')){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->upload->display_errors();
					echo json_encode($response);exit;
				}else{
					$file_info=$this->upload->data();
					$filename=$file_info['file_name'];
					$data['marker_icon2']=$filename;	
				}
				
				if($this->input->post('marker_type',true)==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line("marker_type_error");
					echo json_encode($response);exit;
				}else{
					$data['marker_type']=$this->input->post('marker_type',true);
				}
				
				if($data['marker_icon1']=="" || $data['marker_icon2']==""){
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line("marker_error");
					echo json_encode($response);exit;
				}else{
					$data['created_date']=date('Y-m-d H:i:s');
					if($this->markers_model->AddNewMarkers($data)==TRUE){
						$response=array();
						$response['result']=$this->lang->line('success');
						echo json_encode($response);exit;
					}else{
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('marker_not_add');
						echo json_encode($response);exit;
					}
				}
				
				break;
			case 'edit':
				$edit_id=$this->input->post('edit_id');
				if(intval($edit_id)){
					$config['upload_path'] = 'assets/markers/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['encrypt_name']=true;
					$this->load->library('upload', $config);
					$data=array();
					$marker=$this->db->get_where('marker',array('id'=>$edit_id));
					$image=array();
					if($marker->num_rows()>0){
						$image=$marker->row();
					}
					
					if(!$this->upload->do_upload('marker_icon1') && isset($_FILES['marker_icon1']['name']) && $_FILES['marker_icon1']['name']!=""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->upload->display_errors();
						echo json_encode($response);exit;
					}else{
						if(isset($_FILES['marker_icon1']['name']) && $_FILES['marker_icon1']['name']!=""){
							if(is_file(FCPATH.'assets/markers/'.$image->marker_icon1) && $image->marker_icon1!=""){
								@unlink(FCPATH.'assets/markers/'.$image->marker_icon1);
							}
						}
						$file_info=$this->upload->data();
						$filename=$file_info['file_name'];
						$data['marker_icon1']=($filename!="")?$filename:$image->marker_icon1;
					}
					
					if(!$this->upload->do_upload('marker_icon2') && isset($_FILES['marker_icon2']['name']) && $_FILES['marker_icon2']['name']!=""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->upload->display_errors();
						echo json_encode($response);exit;
					}else{
						if(isset($_FILES['marker_icon2']['name']) && $_FILES['marker_icon2']['name']!=""){
							if(is_file(FCPATH.'assets/markers/'.$image->marker_icon2) && $image->marker_icon2!=""){
								@unlink(FCPATH.'assets/markers/'.$image->marker_icon2);
							}
						}
						$file_info=$this->upload->data();
						$filename=$file_info['file_name'];
						$data['marker_icon2']=($filename!="")?$filename:$image->marker_icon2;
					}
					
					if($this->input->post('marker_type',true)==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line("marker_type_error");
						echo json_encode($response);exit;
					}else{
						$data['marker_type']=$this->input->post('marker_type',true);
					}
					if($data['marker_icon1']=="" || $data['marker_icon2']==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line("marker_error");
						echo json_encode($response);exit;
					}else{
						$data['updated_date']=date('Y-m-d H:i:s');
						if($this->markers_model->UpdateMarkers($data,$edit_id)==TRUE){
							$response=array();
							$response['result']=$this->lang->line('success');
						}else{
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$this->lang->line('marker_not_update');
						}
						echo json_encode($response);exit;
					}
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$this->lang->line('marker_not_update');
					echo json_encode($response);exit;
				}
				
				break;
		}
	}
	public function deleteRecord(){
		$edit_id = $this->input->post('edit_id');
		if(intval($edit_id)){
			$markers=$this->db->get_where('marker',array('id'=>$edit_id));
			if($markers->num_rows()>0){
				$data=$markers->row();
				if(is_file(FCPATH.'assets/markers/'.$data->marker_icon1) && $data->marker_icon1!=""){
					@unlink(FCPATH.'assets/markers/'.$data->marker_icon1);
				}
				if(is_file(FCPATH.'assets/markers/'.$data->marker_icon2) && $data->marker_icon2!=""){
					@unlink(FCPATH.'assets/markers/'.$data->marker_icon2);
				}
				$this->db->where('id',$edit_id);
				$this->db->limit(1,0);
				$this->db->delete('marker');
				
				$this->db->insert('deleted_marker',array('marker_id'=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
			}
		}
	}
}
