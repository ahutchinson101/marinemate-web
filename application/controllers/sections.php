<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sections extends CI_Controller{
	public function __construct(){
		parent::__construct();
		
		is_login();
		if(is_admin()==FALSE){
			redirect('notice');
		}
		$this->load->model('sections_model');
		$this->lang->load('admin','english');
	}
	public function index(){
		$data_header=array();
		$data_header['site_name'] = $this->lang->line('site_name');
		$data_header['display_menu']=check_user_role($this->session->userdata('role'));
		$data_header['username']=$this->sections_model->getUserName($this->session->userdata('user_id'));
		$data_header['userregion']=$this->sections_model->GetRegionName($this->session->userdata('region_id'));
		$this->load->view('header',$data_header);
		$data=array();
		$data['regions']=$this->sections_model->GetAllRegions();
		$this->load->view('sections',$data);
		$data_footer=array();
		$data_footer['role']=$this->session->userdata('role');
		$this->load->view('footer',$data_footer);
	}
	public function all(){
		$aColumns = array('region_id','row','col','kmlfiles','id');
		$sIndexColumn = "id";
		$sTable = "sections";
		$iDisplayStart = $this->input->get_post('iDisplayStart', true);
		$iDisplayLength = $this->input->get_post('iDisplayLength', true);
		$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		$iSortingCols = $this->input->get_post('iSortingCols', true);
		$sSearch = $this->input->get_post('sSearch', true);
		$sEcho = $this->input->get_post('sEcho', true);
		$filter_search=$this->input->post('filter_search',true);
		if(isset($iDisplayStart) && $iDisplayLength != '-1'){
			$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		}
		for($i=0; $i<intval($iSortingCols); $i++){
			$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
			$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
			$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
			if($bSortable == 'true'){
				$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
				//$this->db->order_by('row','ASC');
				//$this->db->order_by('col','ASC');
			}
		}
		if($filter_search!=""){
			$filter_search=explode(',',$filter_search);
			if(isset($filter_search[0]) && $filter_search[0]!=""){
				$this->db->where('row',$filter_search[0]);
			}
			if(isset($filter_search[1]) && $filter_search[1]!=""){
				$this->db->where('col',$filter_search[1]);
			}
			
			
		}
		$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
		$rResult = $this->db->get($sTable);
		$this->db->select('FOUND_ROWS() AS found_rows');
		$iFilteredTotal = $this->db->get()->row()->found_rows;
		$iTotal = $this->db->count_all($sTable);
		$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
		);
		$aRow = $rResult->result_array();
		foreach ( $aRow as $aRow)
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if( $aColumns[$i] == 'region_id' ){
					$regions=explode(',',$aRow[ $aColumns[$i]]);
					$regions_name="";
					$k=1;
					for($j=0;$j<count($regions);$j++){
						$regions_name.=$this->sections_model->GetRegionName($regions[$j]);
						if($k<count($regions)){
							$regions_name.=" , ";
						}
						$k++;
					}
					$row[] = $regions_name;
				}
				if ( $aColumns[$i] == 'row' ){
					$row[] = $aRow[$aColumns[$i]];
				}
				if ( $aColumns[$i] == 'col'){
					$row[] = $aRow[$aColumns[$i]];
				}
				if ( $aColumns[$i] == 'kmlfiles' )	{
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			if(is_admin()==TRUE){
				$row[] = '<a href="javascript:void(0);" id="editsections" regions="'.$aRow['region_id'].'" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/edit.png" border="0" alt="EDIT"></a>&nbsp;&nbsp;<a href="javascript:void(0);" id="deletesections" data-ID="'.$aRow['id'].'"><img src="'.INCLUDE_URL.'assets/images/delete.png" border="0" alt="DELETE"></a>';
			}
			$output['aaData'][] = $row;
		}
		echo json_encode( $output );exit;
	}
	public function addNew(){
		$action=$this->input->post('action',true);
		switch ($action){
			case 'add':
			$config['upload_path'] = 'assets/regions/kmls/';
			$config['allowed_types'] ='kml';
			$config['encrypt_name']=true;
			$this->load->library('upload', $config);
			if($this->input->post('col',true)==""){
				$response=array();
				$response['result']=$this->lang->line('error');
				$response['message']=$this->lang->line('col_error');
				echo  json_encode($response);
				exit;
			}else if($this->input->post('row',true)==""){
				$response=array();
				$response['result']=$this->lang->line('error');
				$response['message']=$this->lang->line('row_error');
				echo  json_encode($response);
				exit;
			}else if(($this->input->post('row',true)!="" && $this->input->post('col',true)!="") && ($this->sections_model->UniqueCellPoints($this->input->post('row',true),$this->input->post('col',true))==TRUE)){
				$response=array();
				$response['result']=$this->lang->line('error');
				$response['message']=$this->lang->line('row_col_unique_error');
				echo  json_encode($response);
				exit;
				
			}else if(!$this->upload->do_upload('kml_files')){
				$response=array();
				$response['result']=$this->lang->line('error');
				$response['message']=$this->upload->display_errors();
				echo  json_encode($response);
				exit;
			}else{
				$file_info=$this->upload->data();
				$filename=$file_info['file_name'];
				$data=array();
				
				$data['region_id']=implode(',',$this->input->post('regions',true));
				$data['col']=$this->input->post('col',true);
				$data['row']=$this->input->post('row',true);
				$data['kmlfiles']=$filename;
				$data['created_date']=date('Y-m-d H:i:s');
				$msg=$this->sections_model->AddSections($data);
				if($msg==TRUE){
					$response=array();
					$response['result']=$this->lang->line('success');
				}else{
					$response=array();
					$response['result']=$this->lang->line('error');
					$response['message']=$msg;
				}
			}
			echo  json_encode($response);
			break;
			case 'edit':
				if(intval($this->input->post('edit_id',true))){
					$config['upload_path'] = 'assets/regions/kmls/';
					$config['allowed_types'] ='kml';
					$config['encrypt_name']=true;
					$this->load->library('upload', $config);
					
					if($this->input->post('col',true)==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('col_error');
						echo  json_encode($response);
						exit;
					}else if($this->input->post('row',true)==""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('row_error');
						echo  json_encode($response);
						exit;
					}else if(($this->input->post('row',true)!="" && $this->input->post('col',true)!="") && ($this->sections_model->EditUniqueCellPoints($this->input->post('row',true),$this->input->post('col',true),$this->input->post('edit_id',true))==TRUE)){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->lang->line('row_col_unique_error');
						echo  json_encode($response);
						exit;
					
					}else if(!$this->upload->do_upload('kml_files') && isset($_FILES['kml_files']['name']) && $_FILES['kml_files']['name']!=""){
						$response=array();
						$response['result']=$this->lang->line('error');
						$response['message']=$this->upload->display_errors();
						echo  json_encode($response);
						exit;
					}else{
						$data=array();
						$data['region_id']=implode(',',$this->input->post('regions',true));
						$data['col']=$this->input->post('col',true);
						$data['row']=$this->input->post('row',true);
						if(isset($_FILES['kml_files']['name']) && $_FILES['kml_files']['name']!=""){
							$file_info=$this->upload->data();
							$filename=$file_info['file_name'];
							$data['kmlfiles']=$filename;
						}
						$data['updated_date']=date('Y-m-d H:i:s');
						$msg=$this->sections_model->UpdateSections($data,$this->input->post('edit_id',true));
						if($msg==TRUE){
							$response=array();
							$response['result']=$this->lang->line('success');
						}else{
							$response=array();
							$response['result']=$this->lang->line('error');
							$response['message']=$msg;
						}
					}
				}
				echo  json_encode($response);
			break;
		}
	}
	public function deleteRecord(){
		if(is_admin()==TRUE){
			$edit_id = $this->input->post('edit_id');
			if(intval($edit_id)){
				$sectionsData=$this->sections_model->getSectionsData($edit_id);
				if(count($sectionsData)>0){
					if($sectionsData->kmlfiles!="" && is_file(FCPATH.'assets/regions/kmls/'.$sectionsData->kmlfiles)){
						@unlink(FCPATH.'assets/regions/kmls/'.$sectionsData->kmlfiles);
					}
					$this->db->where('id',$edit_id);
					$this->db->delete('sections');
	
					$this->db->insert('deleted_sections',array('sections_id'=>$edit_id,'created_date'=>date('Y-m-d H:i:s')));
				}
			}
		}
	}
}