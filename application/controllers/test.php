<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Test extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('authentication_model','auth');
	}
	/*public function index(){
		$query=$this->db->query("select * from `sections` WHERE kmlfiles!=''");
		foreach($query->result() as $d){
			if(!is_file(FCPATH.'assets/regions/kmls/'.$d->kmlfiles)){
					echo $d->kmlfiles;
					echo '<br/>';
			}
		}
	}*/
	public function index(){
		if($this->logged_in()){
			redirect('notice',true);
		}else{
			$this->login();
		}
	}
	public function login(){
	$this->form_validation->set_rules('api_users', 'Username', 'trim|required|xss_clean|alpha_numeric');
		$this->form_validation->set_rules('api_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="error_inner"><strong>', '</strong></div>');
		if($this->form_validation->run() == FALSE){
			$data=array();
			$data['title']='Test';
			$this->load->view('test',$data);
		}
	}
	public function logged_in(){
		return FALSE;
	}
	
	//logout
	function logout(){
		$datame=array();
		$datame['last_login'] = date('Y-m-d h:i:s');
		$datame['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$this->db->where('username',$this->session->userdata('username'));
		$this->db->update('admin',$datame);
		$this->session->sess_destroy();
		redirect('login');
	}
}
