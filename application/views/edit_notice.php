	<form method="post" action="<?php echo BASE_URL?>notice/addNew" name="contentformupdate" enctype="multipart/form-data" id="contentformupdate" class="form" validate="validate">
		<input type="hidden" value="edit" name="action" id="action">
        <input type="hidden" name="region_mapping_id" value="<?php echo $notice_and_regions_mapping->id;?>" />
		<input type="hidden" value="<?php echo  $edit_id;?>" name="edit_id" id="edit_id">
		<div class="for-hd-login">
           	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/3-login-Icon.png" alt="Marine Mate" /></a></div>
            <div class="top-login-tex"><?php echo  $this->lang->line('notice_dialog_header');?></div>
        </div>
		<div class="for-log-repeat">
        	<div class="for-horozontal-repeat">
            	<div class="for-form-mn">
                    <span class="validateTips"><?php echo  $this->lang->line('all_field_required')?></span>
                      <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('region');?> :</div>
                            <div class="for-s-bg"><input type="text" name="regions" id="editregions" readonly="readonly" class="new-fid-search" value="<?php echo $this->notice_model->getMappingReginsName($notice->notice_and_regions_id);?>" />
                           <div class="myselectedregions" style="display:none">
                           	<?php if(count($notice_and_regions_mapping)>0){?>
                            	<?php
									$ids=explode(',',$notice_and_regions_mapping->regions_id);
									for($i=0;$i<count($ids);$i++){?>
                            			<input type="hidden" name="noticeregions[]" value="<?php echo ($ids[$i]==0)?'all':$ids[$i];?>">
                                    <?php }?>
                            <?php }?>
                           </div>
                           </div>
							<?php /*<div class="for-s-bg">
                                <select name="regions" class="for-select" style="border:none; background:transparent;" id="editregions">
                                	<?php if(is_admin() || $this->session->userdata('region_id')==0){?><option value=""><?php echo  $this->lang->line('select_region');?></option>
                                	<option value="all" <?php if(0==$notice->region_id){ echo 'selected="selected"';}?>><?php echo  $this->lang->line('all');?></option>
                                	<?php }?>
                                	<?php foreach ($regions as $region){?>
                                	<option value="<?php echo  $region->id;?>" <?php if($region->id==$notice->region_id){ echo 'selected="selected"';}?>><?php echo  $region->region_name;?></option>
                                	<?php }?>
                                </select>
                            </div>*/?>
                        </div>
                      <?php /*?><div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('place');?> :</div>
                            <div class="for-s-bg" id="place_combo">
                                <select name="places" class="for-select" style="border:none; background:transparent;" id="editplaces">
                              	<option value="all" <?php if(0==$notice->place_id){ echo 'selected="selected"';}?>><?php echo  $this->lang->line('all');?></option>
                                <?php  foreach ($places as $place){?>
                                	<option value="<?php echo  $place->id;?>" <?php if($place->id==$notice->place_id){ echo 'selected="selected"';}?>><?php echo  $place->place_name;?></option>
                                	<?php }?>
                                </select>
                            </div>
                        </div><?php */?>
                      
                        <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('notice_live');?> :</div>
                            <div class="for-s-bg">
                                <input type="text" name="notice_live" value="<?php echo  $notice->notice_live_date;?>" id="editnotice_live" class="new-fid-search">
                            </div>
                        </div>
                        <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('expiry_date');?> :</div>
                            <div class="for-s-bg">
                                <input type="text" name="expiry_date" value="<?php echo  $notice->expiry_date;?>" id="editexpiry_date" class="new-fid-search">
                            </div>
                        </div>
                        <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('title');?> :</div>
                            <div class="for-s-bg">
                                <input type="text" name="title" value="<?php echo  $notice->notice_title;?>" id="edittitle" class="new-fid-search">
                            </div>
                        </div>
                        <div class="for-lg-mn">
                        <div class="for-name"><?php echo  $this->lang->line('Addedbyorganisation');?> :</div>
                            <div class="for-s-bg">
                                <input type="text" name="addedbyorganisation" value="<?php echo  $notice->addedbyorganization;?>" id="addedbyorganisation" class="new-fid-search">
                            </div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo  $this->lang->line('description');?> :</div>
                            <div class="for-area-bg"><textarea name="description" class="new-fid-area" cols="" rows="" id="editdescription"><?php echo  $notice->notice_description;?></textarea></div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
		<script type="text/javascript">
		$(document).ready(function(){
			$( "#editnotice_live" ).datetimepicker({"dateFormat":"yy-mm-dd"});
	 		$( "#editexpiry_date" ).datetimepicker({"dateFormat":"yy-mm-dd"});
		});
		</script>