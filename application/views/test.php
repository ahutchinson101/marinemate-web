<!DOCTYPE html>
	<!--[if IE 7 ]>   <html lang="en" class="ie7 lte8"> <![endif]--> 
	<!--[if IE 8 ]>   <html lang="en" class="ie8 lte8"> <![endif]--> 
	<!--[if IE 9 ]>   <html lang="en" class="ie9"> <![endif]--> 
	<!--[if gt IE 9]> <html lang="en"> <![endif]-->
	<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<!--[if lte IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
	<!-- iPad Settings -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" /> 
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, width=device-width">
	<!-- iPad End -->
	<title><?php echo $title;?> Admin</title>
	<link rel="shortcut icon" href="favicon.ico">
<!-- iOS ICONS -->
<link rel="apple-touch-icon" href="touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone4.png" />
<link rel="apple-touch-startup-image" href="touch-startup-image.png">
<!-- iOS ICONS END -->
<!-- STYLESHEETS -->
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/reset.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/grids.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/style.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/jquery.uniform.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/forms.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/themes/lightblue/style.css" />
<style type = "text/css">
    #loading-container {position: absolute; top:50%; left:50%;}
    #loading-content {width:800px; text-align:center; margin-left: -400px; height:50px; margin-top:-25px; line-height: 50px;}
    #loading-content {font-family: "Helvetica", "Arial", sans-serif; font-size: 18px; color: black; text-shadow: 0px 1px 0px white; }
    #loading-graphic {margin-right: 0.2em; margin-bottom:-2px;}
    #loading {background-color: #eeeeee; height:100%; width:100%; overflow:hidden; position: absolute; left: 0; top: 0; z-index: 99999;}
</style>
<!-- STYLESHEETS END -->
<!--[if lt IE 9]>
<script src="<?php echo INCLUDE_URL;?>assets/js/html5.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/selectivizr.js"></script>
<![endif]-->
</head>
<body class="login" style="overflow: hidden;">
    <div id="loading"> 
        <script type = "text/javascript"> 
            document.write("<div id='loading-container'><p id='loading-content'>" +
                           "<img id='loading-graphic' width='16' height='16' src='<?php echo INCLUDE_URL;?>assets/images/ajax-loader-eeeeee.gif' /> " +
                           "Loading...</p></div>");
        </script> 
    </div> 
    <div class="login-box">
    	<section class="portlet login-box-top">
            <header class="for-header-bg">
                <h2 class="ac"><img src="<?php echo  INCLUDE_URL;?>assets/images/login-logo.png"></h2>
            </header>
			<?php if(validation_errors()):?>
				<div class="message error"> 
                	<h3>Error!</h3> 
                    <p><?php echo form_error('username'); ?></p> 
                    <p><?php echo form_error('password'); ?></p> 
                </div>
			<?php endif;?>
			 <?php if($this->session->flashdata('login-error')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('login-error'); ?></p>
                </div>
            <?php endif;?>
            <section>

                <div class="message info">Test Api</div>
				<form id="form" class="has-validation" method="post" action="api" style="margin-top: 30px">
                    <p style="margin-bottom: 30px">
                        <input type="text" id="api_users" class="full" value="<?php echo set_value('username'); ?>" name="api_users" required placeholder="Username" />
                    </p>
                    <p style="margin-bottom: 30px">
                        <input type="password" id="api_password" class="full" value="<?php echo set_value('password'); ?>" name="api_password" required placeholder="Password" />
                    </p>
                    <input type="text" id="dv" class="full" value="" name="dv"/>
                    <input type="hidden" id="last_requested_date" class="full" value="2014-11-21 06:24:39" name="last_requested_date"/>
                    <p class="clearfix">
						<button class="fr" type="submit">Login</button>
                   </p>
                </form>
               <!--  <footer class="ac">
                   <a href="#" class="button">Reset Password</a>
                </footer> -->
            </section>
    	</section>
	</div>
    <!-- MAIN JAVASCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.js"></script>
    <script>window.jQuery || document.write("<script src='<?php echo INCLUDE_URL;?>assets/js/jquery.min.js'>\x3C/script>")</script>
    <script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/jquery.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/jquery.uniform.min.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/PIE.js"></script>
    <script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/ie.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/global.js"></script>
    <!-- MAIN JAVASCRIPTS END -->
    <!-- LOADING SCRIPT -->
    <script>
    $(window).load(function(){
        $("#loading").fadeOut(function(){
            $(this).remove();
            $('body').removeAttr('style');
        });
    });
    $(document).ready(function(){
        $.tools.validator.fn("#username", function(input, value) {
            return value!='username' ? true : {     
                en: "Please complete this mandatory field"
            };
        });
       
       $.tools.validator.fn("#password", function(input, value) {
            return value!='Password' ? true : {     
                en: "Please complete this mandatory field"
            };
        });
        $("#form").validator({ 
            position: 'bottom left', 
            messageClass:'form-error',
            message: '<div><em/></div>' // em element is the arrow
        });
    });
    </script>
    <!-- LOADING SCRIPT -->
</body>
</html>