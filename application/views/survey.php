<div class="content-part">
	<div class="for-sepret-line">
		<h1><?php echo  $this->lang->line('survey_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="survey_table">
		<thead>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_main_region');?></th>
                <th width="10%"><?php echo $this->lang->line('th_age_group');?></th>
                <th width="20%"><?php echo $this->lang->line('th_main_type_vessel');?></th>
                <th width="20%"><?php echo $this->lang->line('th_main_reason_vessel');?></th>
                <th width="25%"><?php echo $this->lang->line('th_safety_equipment');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="5%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="6" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_main_region');?></th>
                <th width="10%"><?php echo $this->lang->line('th_age_group');?></th>
                <th width="20%"><?php echo $this->lang->line('th_main_type_vessel');?></th>
                <th width="20%"><?php echo $this->lang->line('th_main_reason_vessel');?></th>
                <th width="25%"><?php echo $this->lang->line('th_safety_equipment');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="5%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="<?php echo ADMIN_URL.'survey/exports';?>" class="button add_new" id="addnewsections"><span><span><?php echo $this->lang->line('exportbutton')?></span></span></a></li>
		</ul>
	</div>
	<div class="divClear"></div>
</div>