<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $site_name;?></title>
<link href="<?php echo INCLUDE_URL;?>assets/css/style.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/lib/datatables/css/cleanslate.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/lib/jquery-ui/css/ui-lightness/jquery-ui-1.8.17.custom.css" />
</head>

<body>
<div id="loader"></div>
<div class="main-part">
	<div class="header-part">
    	<div class="for-banner">
        	<div class="for-top-right">
        		<?php //$this->load->view('language');?>
            	<div class="for-welcome"><?php echo $this->lang->line('welcome');?> <?php echo $username;?><?php /*?> - <span><?php echo $userregion;?> <?php echo $this->lang->line('district_council');?></span><?php */?>
            		<div style=" display: block;font-size: 14px; margin-top: 10px; text-align: right;">
            			<a  style="color: #fff;" href="javascript:void(0);" id="myaccountview"><?php echo $this->lang->line('porfile');?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a  style="color: #fff;" href="<?php echo  ADMIN_URL;?>authentication/logout"><?php echo $this->lang->line('logout');?></a>
            		</div>
            	</div>
            </div>
        	<div class="logo"><a href="#"><img src="<?php echo INCLUDE_URL;?>assets/images/logo.png" alt="Marine Mate" /></a></div>
        </div>
    	<div class="menu-part">
    		<div class="second-sepret">
            	<div class="one-icon"><a href="<?php ADMIN_URL;?>notice"><img src="<?php if($this->uri->segment(1)=='notice'){echo INCLUDE_URL.'assets/images/AddNoticeIcon.png';}else{echo INCLUDE_URL.'assets/images/inactive-AddNoticeIcon.png';}?>" alt="<?php echo  $this->lang->line('notice');?>" /></a></div>
                <div class="for-link"><a href="<?php ADMIN_URL;?>notice"><?php echo  $this->lang->line('notice');?></a></div>
            </div>
        	<?php if($display_menu==TRUE){?>
          	<div class="second-sepret">
            	<div class="one-icon"><a href="<?php ADMIN_URL;?>users"><img src="<?php if($this->uri->segment(1)=='users'){echo INCLUDE_URL.'assets/images/AddUserIcon.png';}else{echo INCLUDE_URL.'assets/images/inactive-AddUserIcon.png';}?>" alt="<?php echo $this->lang->line('users');?>" /></a></div>
                <div class="for-link"><a href="<?php ADMIN_URL;?>users"><?php echo $this->lang->line('users');?></a></div>
            </div>
            <?php }?>
           
            <div class="second-sepret">
            	<div class="one-icon"><a href="<?php ADMIN_URL;?>places"><img src="<?php if($this->uri->segment(1)=='places'){echo INCLUDE_URL.'assets/images/AddPlaceIcon.png';}else{echo INCLUDE_URL.'assets/images/inactive-AddPlaceIcon.png';}?>" alt="<?php echo $this->lang->line('places');?>" /></a></div>
                <div class="for-link"><a href="<?php ADMIN_URL;?>places"><?php echo $this->lang->line('places');?></a></div>
            </div>
            
            <?php if($display_menu==TRUE){?>
          	<div class="second-sepret">
            	<div class="one-icon"><a href="<?php echo  ADMIN_URL;?>regions"><img src="<?php if($this->uri->segment(1)=='regions'){echo INCLUDE_URL.'assets/images/region.png';}else{echo INCLUDE_URL.'assets/images/inactive-region.png';}?>" alt="<?php echo $this->lang->line('regions');?>" /></a></div>
                <div class="for-link"><a href="<?php echo  ADMIN_URL;?>regions"><?php echo $this->lang->line('regions');?></a></div>
            </div>
            <?php }?>
            <?php /* if($display_menu==TRUE){?>
          	<div class="second-sepret">
            	<div class="one-icon"><a href="<?php echo  ADMIN_URL;?>rules"><img src="<?php if($this->uri->segment(1)=='rules'){echo INCLUDE_URL.'assets/images/active-rules.png';}else{echo INCLUDE_URL.'assets/images/inactive-rules.png';}?>" alt="<?php echo $this->lang->line('rules');?>" /></a></div>
                <div class="for-link"><a href="<?php echo  ADMIN_URL;?>rules"><?php echo $this->lang->line('rules');?></a></div>
            </div>
            <?php } */?>
            <?php if($display_menu==TRUE){?>
          	<div class="second-sepret">
            	<div class="one-icon"><a href="<?php echo  ADMIN_URL;?>sections"><img src="<?php if($this->uri->segment(1)=='sections'){echo INCLUDE_URL.'assets/images/sections_active.png';}else{echo INCLUDE_URL.'assets/images/sections_inactive.png';}?>" alt="<?php echo $this->lang->line('rules');?>" /></a></div>
                <div class="for-link"><a href="<?php echo  ADMIN_URL;?>sections"><?php echo $this->lang->line('sections');?></a></div>
            </div>
            <?php }?>
            <?php if($display_menu==TRUE){?>
          	   <div class="for-link-bullet"><a href="javascript:void(0);" id="showsubmenu"><img src="<?php echo INCLUDE_URL.'assets/images/bullets.png';?>" alt="" /></a></div>
            	<div class="submenu" style="display:none">
                	<div class="sub-link"><a href="<?php echo ADMIN_URL;?>survey">Survey</a></div>
                </div>
            <?php }?>
        </div>
    </div>