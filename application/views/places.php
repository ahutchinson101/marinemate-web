
<div class="content-part">
	<div class="for-sepret-line">
		<?php if($display_menu==TRUE){?>
		     <div style="float: right;padding-right: 15px; ">
		         <div class="one-icon" style="margin-top: 10px;"><a href="<?php echo  ADMIN_URL;?>markers"><img src="<?php echo INCLUDE_URL;?>assets/images/marker-small.png" alt="<?php echo $this->lang->line('menu_marker');?>" /></a></div>
		         <div class="for-link"><a href="<?php echo  ADMIN_URL;?>markers"><?php echo $this->lang->line('menu_marker');?></a></div>
		     </div>
		<?php }?>
		<div class="search-main">
			<a href="javascript:void(0);" id="filter_form_id" class="filter-button"><?php echo $this->lang->line('filter');?></a>
			<div class="input-main"><input type="text" name="filter_search" value="" id="filter_search" class="input-class"></div>
		</div>
		
		<h1 style="float: left;"><?php echo  $this->lang->line('place_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="places_table">
		<thead>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_place');?></th>
				<th width="20%"><?php echo $this->lang->line('th_region');?></th>
				<th width="15%"><?php echo $this->lang->line('th_marker');?></th>
				<th width="15%"><?php echo $this->lang->line('th_gps_coordinates');?></th>
				<th width="25%"><?php echo $this->lang->line('th_place_notice');?></th>
				<th width="25%"><?php echo $this->lang->line('image');?></th>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="6" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_place');?></th>
				<th width="20%"><?php echo $this->lang->line('th_region');?></th>
				<th width="15%"><?php echo $this->lang->line('th_marker');?></th>
				<th width="15%"><?php echo $this->lang->line('th_gps_coordinates');?></th>
				<th width="25%"><?php echo $this->lang->line('th_place_notice');?></th>
				<th width="25%"><?php echo $this->lang->line('image');?></th>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnewplace"><span><span><?php echo $this->lang->line('addbutton');?></span></span></a></li>
		</ul>
	</div>
	<div id="filter-dialog" style="display: none;">
		<form method="post" action="" name="filterform" enctype="multipart/form-data" id="filterform" class="form" validate="validate">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/2loginIcon.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('place_filter');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                    	<span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                    	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('region');?> :</div>
                            <div class="for-s-bg">
                                <select name="filter_place_regions" class="for-select" style="border:none; background:transparent;" id="filter_place_regions">
                                	<option value=""><?php echo $this->lang->line('select_region');?></option>
                                	<?php foreach ($regions as $region){?>
                                	 <?php if(is_admin()==FALSE){?>
                                    	<?php if($region->id==$this->session->userdata('region_id')){?>
                                        <option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option><?php }?>
									  <?php }else{?>
                                	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option><?php }?>
                                	<?php }?>
                                </select>
                            </div>
                        </div>
                       	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('place')?> :</div>
                            <div class="for-s-bg">
                                <input name="filter_place" class="new-fid-search" type="text" id="filter_place"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
	<div id="dialog-form" title="Add Place" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>places/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
			<input type="hidden" value="" name="action" id="action">
			<input type="hidden" value="" name="edit_id" id="edit_id">
			<input type="hidden" value="" name="old_image" id="old_image">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/2loginIcon.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('place_dialog_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                    <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('place_name')?> :</div>
                            <div class="for-s-bg"><input name="place_name" class="new-fid-search" type="text" id="place_name"/></div>
                        </div>
                        
                      <div class="for-lg-mn">
                      	<div class="for-name"><?php echo $this->lang->line('region')?> :</div>
                            <div class="for-s-bg">
                                <select name="regions" class="for-select" style="border:none; background:transparent;" id="regions">
                                	<option value=""><?php echo $this->lang->line('select_region');?></option>
                                	<?php foreach ($regions as $region){?>
                                    <?php if(is_admin()==FALSE){?>
                                    	<?php if($region->id==$this->session->userdata('region_id')){?>
                                        <option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option><?php }?>
									  <?php }else{?>
                                	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option><?php }?>
                                	<?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="for-lg-mn">
                      	<div class="for-name"><?php echo $this->lang->line('marker');?> :</div>
                            <div class="for-s-bg">
                                <select name="marker" class="for-select" style="border:none; background:transparent;" id="marker">
                                	<option value=""><?php echo $this->lang->line('select_marker');?></option>
                                	<?php foreach ($markers as $marker){?>
                                	<option value="<?php echo  $marker->id;?>"><?php echo  $marker->marker_type;?></option>
                                	<?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('coordinate')?> :</div>
                            <div class="one-s-bg"><input name="latitude" class="small-fid-search" type="text" id="latitude" /></div>
                            <div class="two-s-bg"><input name="longitude" class="small-fid-search" type="text" id="longitude"/></div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('place_description');?> :</div>
                            <div class="for-area-bg"><textarea name="description" class="new-fid-area" cols="" rows="" id="description"></textarea></div>
                        </div>
                        <div class="for-lg-mn place-image-main">
                            <div class="for-name"><?php echo $this->lang->line('old_image');?> :</div>
                            <div class="for-image">
                            	
                            </div>
                            <div class="for-name"></div>
                            <div class="for-delete-image">
                            	<a href="#" id="delete-image">Delete Image</a>
                            </div>
                        </div>
                        <div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('place_image');?> :</div>
                        	<div class="for-s-file"><input type="file" name="place_image" id="place_image"></div>
                      </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>