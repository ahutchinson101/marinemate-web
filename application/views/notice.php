
<div class="content-part">
  <div class="for-sepret-line">
    <div class="search-main"> <a href="javascript:void(0);" id="filter_form_id" class="filter-button"><?php echo $this->lang->line('filter');?></a>
      <div class="input-main">
        <input type="text" name="filter_search" value="" id="filter_search" class="input-class">
      </div>
    </div>
    <h1><?php echo  $this->lang->line('notice_header');?></h1>
  </div>
  <table cellpadding="0" cellspacing="0" border="0" class="display" id="notice_table">
    <thead>
      <tr>
        <th width="15%"><?php echo $this->lang->line('th_posted_date');?></th>
        <th width="15%"><?php echo $this->lang->line('th_region');?></th>
        <th width="15%"><?php echo $this->lang->line('th_area');?></th>
        <th width="25%"><?php echo $this->lang->line('th_title');?></th>
        <th width="15%"><?php echo $this->lang->line('th_added_by');?></th>
        <th width="10%"><?php echo $this->lang->line('th_expiry_date');?></th>
        <th width="5%" align="center"><?php echo $this->lang->line('th_action');?></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="7" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo $this->lang->line('load_data_in_server');?></td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <th width="15%"><?php echo $this->lang->line('th_posted_date');?></th>
        <th width="15%"><?php echo $this->lang->line('th_region');?></th>
        <th width="15%"><?php echo $this->lang->line('th_area');?></th>
        <th width="25%"><?php echo $this->lang->line('th_title');?></th>
        <th width="15%"><?php echo $this->lang->line('th_added_by');?></th>
        <th width="10%"><?php echo $this->lang->line('th_expiry_date');?></th>
        <th width="5%" align="center"><?php echo $this->lang->line('th_action');?></th>
      </tr>
    </tfoot>
  </table>
  <div class="table_menu">
    <ul class="left">
      <li><a href="javascript:void(0);" class="button add_new" id="addnewnotice"><span><span><?php echo  $this->lang->line('addbutton')?></span></span></a></li>
    </ul>
  </div>
  <div id="filter-dialog" style="display: none;">
    <form method="post" action="" name="filterform" enctype="multipart/form-data" id="filterform" class="form" validate="validate">
      <div class="for-hd-login">
        <div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/rules-big.png" alt="Marine Mate" /></a></div>
        <div class="top-login-tex"><?php echo $this->lang->line('notice_filters');?></div>
      </div>
      <div class="for-log-repeat">
        <div class="for-horozontal-repeat">
          <div class="for-form-mn"> <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
            <div class="for-lg-mn">
              <label><?php echo $this->lang->line('posted_date');?> :</label>
              <div class="posted-main">
                <div class="form-date-main">
                  <label><?php echo $this->lang->line('form');?> :</label>
                  <div class="form-date">
                    <input type="text" name="posted_form_date" id="posted_form_date">
                  </div>
                </div>
                <div class="form-date-main">
                  <label><?php echo $this->lang->line('to');?> :</label>
                  <div class="form-date">
                    <input type="text" name="posted_to_date" id="posted_to_date">
                  </div>
                </div>
              </div>
            </div>
            <div class="for-lg-mn">
              <label><?php echo $this->lang->line('expiry_date');?> :</label>
              <div class="posted-main">
                <div class="form-date-main">
                  <label><?php echo $this->lang->line('form');?> :</label>
                  <div class="form-date">
                    <input type="text" name="expiry_form_date" id="expiry_form_date">
                  </div>
                </div>
                <div class="form-date-main">
                  <label><?php echo $this->lang->line('to');?> :</label>
                  <div class="form-date">
                    <input type="text" name="expiry_to_date" id="expiry_to_date">
                  </div>
                </div>
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo $this->lang->line('region');?> :</div>
              <div class="for-s-bg">
                <select name="filter_place_regions" class="for-select" style="border:none; background:transparent;" id="filter_place_regions">
                  <option value=""><?php echo $this->lang->line('select_region')?></option>
                  <option value="all"><?php echo $this->lang->line('all');?></option>
                  <?php foreach ($regions as $region){?>
                  <option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <?php /*?><div class="for-lg-mn">
              <div class="for-name"><?php echo $this->lang->line('place')?> :</div>
              <div class="for-s-bg">
                <select name="filter_place" class="for-select" style="border:none; background:transparent;" id="filter_place">
                </select>
              </div>
            </div><?php */?>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo $this->lang->line('title');?> :</div>
              <div class="for-s-bg">
                <input name="filter_title" class="new-fid-search" type="text" id="filter_title"/>
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('Addedbyorganisation');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="filter_addedbyorganisation" value="" id="filter_addedbyorganisation" class="new-fid-search">
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div id="dialog-form" title="Add Notice" style="display: none;">
    <form method="post" action="<?php echo BASE_URL?>notice/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
      <input type="hidden" value="add" name="action" id="action">
      <div class="for-hd-login">
        <div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/3-login-Icon.png" alt="Marine Mate" /></a></div>
        <div class="top-login-tex"><?php echo  $this->lang->line('notice_dialog_header');?></div>
      </div>
      <div class="for-log-repeat">
        <div class="for-horozontal-repeat">
          <div class="for-form-mn"> <span class="validateTips"><?php echo  $this->lang->line('all_field_required')?></span>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('region');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="regions" id="regions" readonly="readonly" class="new-fid-search" />
                <div class="myselectedregions" style="display:none"></div>
              </div>
              <?php /*<select name="regions" class="for-select" style="border:none; background:transparent;" id="regions">
                                	<?php if(is_admin()==TRUE || $this->session->userdata('region_id')==0){?>
	                                	<option value=""><?php echo  $this->lang->line('select_region');?></option>
	                                	<option value="all"><?php echo  $this->lang->line('all');?></option>
	                                	<?php foreach ($regions as $region){?>
	                                	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
	                                	<?php }?>
                                	<?php }else{?>
                                		<option value="<?php echo  $this->session->userdata('region_id');?>"><?php echo  $userregion;?></option>
                                	<?php }?>
                                </select> */?>
            </div>
            <?php /*?><div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('place');?> :</div>
              <div class="for-s-bg" id="place_combo">
                <select name="places" class="for-select" style="border:none; background:transparent;" id="places">
                  <?php if(is_admin()==FALSE){?>
                  <option value=""><?php echo $this->lang->line('select_place');?></option>
                  <option value="all"><?php echo  $this->lang->line('all');?></option>
                  <?php foreach ($places as $place){?>
                  <option value="<?php echo  $place->id;?>"><?php echo  $place->place_name;?></option>
                  <?php }?>
                  <?php }?>
                </select>
              </div>
            </div><?php */?>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('notice_live');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="notice_live" value="" id="notice_live" class="new-fid-search">
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('expiry_date');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="expiry_date" value="" id="expiry_date" class="new-fid-search">
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('title');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="title" value="" id="title" class="new-fid-search">
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('Addedbyorganisation');?> :</div>
              <div class="for-s-bg">
                <input type="text" name="addedbyorganisation" value="" id="addedbyorganisation" class="new-fid-search">
              </div>
            </div>
            <div class="for-lg-mn">
              <div class="for-name"><?php echo  $this->lang->line('description');?> :</div>
              <div class="for-area-bg">
                <textarea name="description" class="new-fid-area" cols="" rows="" id="description"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div id="dialog-form-edit" title="Edit Notice" style="display: none;"></div>
	<div id="dialog-form-regions" title="Regions" style="display: none;">
		<form name="selectedregions" id="selectedregions">
			<div class="for-hd-login" style="height:37px;">
				<div class="top-login-tex" style="padding:9px 0 0 19px;"><?php echo  $this->lang->line('regions_dialog_header');?></div>
			</div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
					<div class="for-form-mn"> 
						<div class="for-lg-mn">
							<ul style="margin:0; padding:0; list-style:none;">
								<?php if(is_admin()==TRUE || $this->session->userdata('region_id')==0){?><li style="border-bottom:1px dotted;">
									<input type="checkbox" name="All" id="regionsids_all" value="all"/>
									<label><?php echo  $this->lang->line('all');?></label>
								</li><?php }?>
								<?php foreach ($regions as $region){?>
                                    <li style="border-bottom:1px dotted;">
										<input type="checkbox" name="<?php echo trim($region->region_name);?>" id="regionsids_<?php echo $region->id;?>" value="<?php echo $region->id;?>" />
										<label><?php echo trim($region->region_name);?></label>
									</li>
                                <?php }?>
                            </ul>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
