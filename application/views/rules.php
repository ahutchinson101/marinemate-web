
<div class="content-part">
	<div class="for-sepret-line">
		<div class="search-main">
			<a href="javascript:void(0);" id="filter_form_id" class="filter-button"><?php echo $this->lang->line('filter');?></a>
			<div class="input-main"><input type="text" name="filter_search" value="" id="filter_search" class="input-class"></div>
		</div>
		<h1><?php echo  $this->lang->line('rules_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="rules_table">
		<thead>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_posted_date');?></th>
				<th width="25%"><?php echo $this->lang->line('th_place');?></th>
				<th width="25%"><?php echo $this->lang->line('th_title');?></th>
				<th width="25%"><?php echo $this->lang->line('th_description');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_status');?></th>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="6" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_posted_date');?></th>
				<th width="25%"><?php echo $this->lang->line('th_place');?></th>
				<th width="25%"><?php echo $this->lang->line('th_title');?></th>
				<th width="25%"><?php echo $this->lang->line('th_description');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_status');?></th>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnewrules"><span><span><?php echo $this->lang->line('addbutton');?></span></span></a></li>
		</ul>
	</div>
	<div id="filter-dialog" style="display: none;">
		<form method="post" action="" name="filterform" enctype="multipart/form-data" id="filterform" class="form" validate="validate">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/rules-big.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('rules_filter');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                    <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                    	<div class="for-lg-mn">
                    		<label><?php echo $this->lang->line('posted_date');?> :</label>
                    		<div class="posted-main">
                    			<div class="form-date-main">
                    				<label><?php echo  $this->lang->line('form');?> :</label>
                    				<div class="form-date"><input type="text" name="form_date" id="form_date"></div>
                    			</div>
                    			<div class="form-date-main">
                    				<label><?php echo $this->lang->line('to');?> :</label>
                    				<div class="form-date"><input type="text" name="to_date" id="to_date"></div>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('region')?> :</div>
                            <div class="for-s-bg">
                                <select name="filter_place_regions" class="for-select" style="border:none; background:transparent;" id="filter_place_regions">
                                	<option value=""><?php echo $this->lang->line('select_region');?></option>
                                	<?php foreach ($regions as $region){?>
                                	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
                                	<?php }?>
                                </select>
                            </div>
                        </div>
                       	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('place');?> :</div>
                            <div class="for-s-bg">
                                <select name="filter_place" class="for-select" style="border:none; background:transparent;" id="filter_place">
                                	<option value=""><?php echo $this->lang->line('select_place');?></option>
                                	<?php /*foreach ($places as $place){?>
                                	<option value="<?php echo  $place->id;?>"><?php echo  $place->place_name;?></option>
                                	<?php } */?>
                                </select>
                            </div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('title');?> :</div>
                            <div class="for-s-bg"><input name="filter_title" class="new-fid-search" type="text" id="filter_title"/></div>
                        </div>
                     </div>
                </div>
            </div>
		</form>
	</div>
	<div id="dialog-form" title="Rules" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>rules/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
			<input type="hidden" value="" name="action" id="action">
			<input type="hidden" value="" name="edit_id" id="edit_id">
			<input type="hidden" value="" name="place_id" id="place_id">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/rules-big.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('rules_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                    <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                    	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('region')?> :</div>
                            <div class="for-s-bg">
                                <select name="place_regions" class="for-select" style="border:none; background:transparent;" id="place_regions">
                                	<option value=""><?php echo $this->lang->line('select_region');?></option>
                                	<?php foreach ($regions as $region){?>
                                	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
                                	<?php }?>
                                </select>
                            </div>
                        </div>
                       	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('place')?> :</div>
                            <div class="for-s-bg">
                                <select name="place" class="for-select" style="border:none; background:transparent;" id="place">
                                	<option value=""><?php echo $this->lang->line('select_place');?></option>
                                	<?php /*foreach ($places as $place){?>
                                	<option value="<?php echo  $place->id;?>"><?php echo  $place->place_name;?></option>
                                	<?php } */?>
                                </select>
                            </div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('title')?> :</div>
                            <div class="for-s-bg"><input name="title" class="new-fid-search" type="text" id="title"/></div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('description');?> :</div>
                            <div class="for-area-bg"><textarea name="description" class="new-fid-area" cols="" rows="" id="description"></textarea></div>
                        </div>
                        <div class="for-lg-mn">
                      	<div class="for-name"><?php echo $this->lang->line('status');?> :</div>
                            <div class="for-s-bg">
                                <select name="rules_status" class="for-select" style="border:none; background:transparent;" id="rules_status">
                                	<option value="0"><?php echo $this->lang->line('inactive');?></option>
                                	<option value="1"><?php echo $this->lang->line('active');?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>