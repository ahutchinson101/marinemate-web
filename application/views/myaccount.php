<form method="post" action="<?php echo BASE_URL?>authentication/UpdateAccount" name="updateaccountform" enctype="multipart/form-data" id="updateaccountform" class="form" validate="validate">
	<div class="for-hd-login">
		<div class="log-logo">
			<a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/user-big.png" alt="Marine Mate" /></a>
		</div>
		<div class="top-login-tex"><?php echo  $this->lang->line('profiles');?></div>
	</div>
	<div class="for-log-repeat">
		<div class="for-horozontal-repeat">
			<div class="for-form-mn">
				<span class="validateTips"><?php echo  $this->lang->line('all_field_required');?></span>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo  $this->lang->line('region');?> :</div>
					<div class="for-s-bg">
						<select name="myregions" class="for-select" style="border: none; background: transparent;" id="myregions">
							<?php if(is_admin()==TRUE){?>
								<option value=""><?php echo  $this->lang->line('select_region');?></option>
								<?php foreach ($regions as $region){?>
									<option value="<?php echo  $region->id;?>" <?php if($region->id==$users->region_id){ echo 'selected="selected"';} ?>><?php echo  $region->region_name;?></option>
								<?php }?>
							<?php }else{?>
								<option value="<?php echo  $regions->id;?>"><?php echo  $regions->region_name;?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo $this->lang->line('username')?> :</div>
					<div class="for-s-bg"><span style="padding: 5px 5px 0px 5px;"><?php echo  $users->username;?></span></div>
				</div>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo $this->lang->line('name');?> :</div>
					<div class="for-s-bg">
						<input type="text" name="myname" value="<?php echo  $users->name;?>" id="myname"class="new-fid-search">
					</div>
				</div>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo $this->lang->line('email');?> :</div>
					<div class="for-s-bg">
						<input type="email" name="myemail" value="<?php echo  $users->email;?>" id="myemail" class="new-fid-search">
					</div>
				</div>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo $this->lang->line('password');?> :</div>
					<div class="for-s-bg">
						<input type="password" name="mypassword" value="" id="mypassword" class="new-fid-search">
					</div>
				</div>
				<div class="for-lg-mn">
					<div class="for-name"><?php echo $this->lang->line('password_again');?> :</div>
					<div class="for-s-bg">
						<input type="password" name="mypasswordagain" value="" id="mypasswordagain" class="new-fid-search">
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
