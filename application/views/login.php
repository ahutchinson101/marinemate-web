<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $site_name;?></title>
<link href="<?php echo  INCLUDE_URL;?>assets/css/style.css" rel="stylesheet" type="text/css" />
<style type = "text/css">
    #loading-container {position: absolute; top:50%; left:50%;}
    #loading-content {width:800px; text-align:center; margin-left: -400px; height:50px; margin-top:-25px; line-height: 50px;}
    #loading-content {font-family: "Helvetica", "Arial", sans-serif; font-size: 18px; color: black; text-shadow: 0px 1px 0px white; }
    #loading-graphic {margin-right: 0.2em; margin-bottom:-2px;}
    #loading {background-color: #eeeeee; height:100%; width:100%; overflow:hidden; position: absolute; left: 0; top: 0; z-index: 99999;}
</style>
</head>
<body class="login" style="overflow: hidden;">

<div id="loading"> 
	<script type = "text/javascript"> 
    	document.write("<div id='loading-container'><p id='loading-content'>" + "<img id='loading-graphic' width='16' height='16' src='<?php echo INCLUDE_URL;?>assets/images/ajax-loader-eeeeee.gif' /> " + "<?php echo $this->lang->line('loading');?></p></div>");
    </script> 
</div> 

<div class="main-part">
    <div class="content-part">
    	<div class="login-main">
        	<div class="for-hd-login">
            	<div class="two-log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/login-logo.png" alt="Marine Mate" /></a></div>
        	</div>
            <div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
		               	<?php if(validation_errors()):?>
							<div class="message error"> 
		                		<h3>Error!</h3> 
		                    	<p><?php echo form_error('username'); ?></p> 
		                    	<p><?php echo form_error('password'); ?></p> 
		                	</div>
						<?php endif;?>
					 	<?php if($this->session->flashdata('login-error')):?>
		                	<div class="message error"> 
		                		<h3>Error!</h3> 
		                    	<p><?php echo $this->session->flashdata('login-error'); ?></p>
		                	</div>
		            	<?php endif;?>
            		<form id="form" class="has-validation" method="post" action="" style="margin-top: 30px">
                      <div class="for-lg-mn">
                        <div class="for-name"><?php echo $this->lang->line('username');?> :</div>
                            <div class="for-s-bg"><input name="username" class="new-fid-search" type="text" id="username"/></div>
                        </div>
                        
                      <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('password');?> :</div>
                            <div class="for-s-bg"><input name="password" class="new-fid-search" type="password"  id="password"/></div>
                        </div>
                      <div class="bottom-lg-mn">
                        <div class="for-name"></div>
                          <div class="for-enter"><input type="submit" class="btn-enter" value="<?php echo $this->lang->line('enter');?>" /></div>
                      </div>
                      </form>
                  </div>
                </div>
            </div>
            <div class="for-round"><img src="<?php echo  INCLUDE_URL;?>assets/images/login-bottom-bg.jpg" alt="" style="float:left;" /></div>
            <div class="divclear"></div>
        </div>
    </div>
	<div class="divClear"></div>
</div>
<script src="<?php echo INCLUDE_URL;?>assets/js/jquery.js"></script>
<script type="text/javascript">
$(window).load(function(){
    $("#loading").fadeOut(function(){
        $(this).remove();
        $('body').removeAttr('style');
    });
});
</script>
</body>
</html>
