<div class="content-part">
	<div class="for-sepret-line">
	<?php /*if($display_menu==TRUE){?>
	     <div style="float: right;padding-right: 15px; ">
	         <div class="one-icon"><a href="<?php echo  ADMIN_URL;?>kmlfiles"><img src="<?php echo INCLUDE_URL;?>assets/images/regions-UploadFileIcon.png" alt="<?php echo $this->lang->line('kml');?>" /></a></div>
	         <div class="for-link"><a href="<?php echo  ADMIN_URL;?>kmlfiles"><?php echo $this->lang->line('kml');?></a></div>
	     </div>
		<?php } */?>
		<h1 style="float: left;"><?php echo  $this->lang->line('regions_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="regions_table">
		<thead>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_region');?></th>
				<th width="25%"><?php echo $this->lang->line('th_coordinates_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="3" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_region');?></th>
				<th width="25%"><?php echo $this->lang->line('th_coordinates_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnewregions"><span><span><?php echo $this->lang->line('addbutton');?></span></span></a></li>
		</ul>
	</div>
	<div class="divClear"></div>
	<div id="dialog-form" title="<?php echo $this->lang->line('menu_regions');?>" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>regions/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
		<input type="hidden" value="" name="action" id="action">
		<input type="hidden" value="" name="edit_id" id="edit_id">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/login-region.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('regions_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                      <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('region_name');?> :</div>
                            <div class="for-s-bg"><input name="region_name" class="new-fid-search" type="text" id="region_name" /></div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('coordinate_file')?>:</div>
                            <div class="for-s-file"><input type="file" name="csvfiles" id="csvfiles" style="width: 170px;" size="13">
                            	<a href="javascript:void(0);" id="help_icon"><img src="<?php echo  INCLUDE_URL;?>assets/images/help-icon.png" alt="Marine Mate" /></a>
                            	<div id="files_help" style="display: none;" title="Help"><?php echo  $this->lang->line('coordinate_files_help');?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>