
<div class="content-part">
	<div class="for-sepret-line">
    	<div class="search-main">
      	<div class="input-main">
        <input type="text" name="filter_search" value="" id="filter_search" class="input-class" placeholder="Row,Col">
      </div>
    </div>
		<h1><?php echo  $this->lang->line('sections_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="sections_table">
		<thead>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_region');?></th>
				<th width="20%"><?php echo $this->lang->line('th_row');?></th>
                <th width="20%"><?php echo $this->lang->line('th_col');?></th>
                <th width="20%"><?php echo $this->lang->line('th_kml_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="20%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="5" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="20%"><?php echo $this->lang->line('th_region');?></th>
				<th width="20%"><?php echo $this->lang->line('th_row');?></th>
                <th width="20%"><?php echo $this->lang->line('th_col');?></th>
                <th width="20%"><?php echo $this->lang->line('th_kml_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="20%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnewsections"><span><span><?php echo $this->lang->line('addbutton')?></span></span></a></li>
		</ul>
	</div>
	<div class="divClear"></div>
	<div id="dialog-form" title="<?php echo $this->lang->line('menu_regions');?>" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>sections/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
		<input type="hidden" value="" name="action" id="action">
		<input type="hidden" value="" name="edit_id" id="edit_id">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/sections_big.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('sections_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                      <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                       <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('region');?> :</div>
                            <div class="multi-select">
                                <select name="regions[]" class="for-select" style="border:none; background:transparent;" id="regions" multiple="multiple">
                                   	<?php foreach ($regions as $region){?>
	                            	   	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
	                               	<?php }?>
                                </select>
                            </div>
                      </div>
                        <div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('row')?> :</div>
                            <div class="for-s-bg">
                                <input name="row" class="new-fid-search" type="text" id="row"/>
                            </div>
                        </div>
                     	<div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('col')?> :</div>
                            <div class="for-s-bg">
                                <input name="col" class="new-fid-search" type="text" id="col"/>
                            </div>
                        </div>
                      <div class="for-lg-mn">
                      		<div class="for-name"><?php echo $this->lang->line('kaml_files');?> :</div>
                        	<div class="for-s-file"><input type="file" name="kml_files" id="kml_files"></div>
                      </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>