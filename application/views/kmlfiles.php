
<div class="content-part">
	<div class="for-sepret-line">
		<h1><?php echo  $this->lang->line('kmlfiles_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="kmlfiles_table">
		<thead>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_region');?></th>
				<th width="25%"><?php echo $this->lang->line('th_kml_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="3" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_region');?></th>
				<th width="25%"><?php echo $this->lang->line('th_kml_files');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnewkmlfiles"><span><span><?php echo $this->lang->line('addbutton')?></span></span></a></li>
		</ul>
	</div>
	<div class="divClear"></div>
	<div id="dialog-form" title="<?php echo $this->lang->line('menu_regions');?>" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>kmlfiles/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
		<input type="hidden" value="" name="action" id="action">
		<input type="hidden" value="" name="edit_id" id="edit_id">
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/UploadFileIcon.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo $this->lang->line('kmlfiles_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                      <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                       <div class="for-lg-mn">
                      	<div class="for-name"><?php echo  $this->lang->line('region');?> :</div>
                            <div class="for-s-bg">
                                <select name="regions" class="for-select" style="border:none; background:transparent;" id="regions">
                                	<option value=""><?php echo  $this->lang->line('select_region');?></option>
	                               	<?php foreach ($regions as $region){?>
	                            	   	<option value="<?php echo  $region->id;?>"><?php echo  $region->region_name;?></option>
	                               	<?php }?>
                                </select>
                            </div>
                      </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('kaml_files');?> :</div>
                            <div class="for-s-file"><input type="file" name="kml_files" id="kml_files"></div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>