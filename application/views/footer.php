<div class="divClear"></div>
<div id="myaccount-form" title="My Account"></div>
 </div>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/lib/jquery-ui/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/lib/datatables/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/timepicker.js"></script> 
<?php if($display_menu==TRUE){?>
<script type="text/javascript">
$(document).ready(function(e) {
    $("#showsubmenu").live("click",function(){
		$(".submenu").toggle();
	});
});
</script>
<?php }?>
<?php if($this->uri->segment(1)=='survey'){?>
	<script type="text/javascript">
	var surveyTable;
        $(document).ready(function(){
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	surveyTable = $('#survey_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ]},{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ] },{ "sClass": "center", "aTargets": [ 3 ] },{ "sClass": "center", "aTargets": [ 4 ] }<?php if($display_menu==TRUE){?>,{ "sClass": "center", "aTargets": [5] , "bSortable": false }<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				 "fnServerData": function ( sSource, aoData, fnCallback ) {
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            });
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>survey/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            });
        	$(".ui-dialog-titlebar").remove();
			$( "#deletesurvey" ).live('click',function() {
        		var edit_id = $(this).attr('data-ID');
        		$.post('<?php echo BASE_URL;?>survey/deleteRecord',{edit_id:edit_id},function(){
        			$('#survey_table').dataTable().fnDraw();
            	});
    		});
    	});
       /*var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#survey_table').dataTable().fnDraw();}, 5 * 1000);
        };*/
   // $(document).live('keypress, click', function() { intvrefresh() });
   // intvrefresh();
	</script>
<?php }?>

<?php if($this->uri->segment(1)=='sections'){?>
	<script type="text/javascript">
		var giCount = 1;
 	var regions=$("#regions");
 	var tips = $( ".validateTips" );
	var sectionsTable;
        $(document).ready(function(){
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	sectionsTable = $('#sections_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ]},{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ] },{ "sClass": "center", "aTargets": [ 3 ] }<?php if($display_menu==TRUE){?>,{ "sClass": "center", "aTargets": [4] , "bSortable": false }<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				 "fnServerData": function ( sSource, aoData, fnCallback ) {
					if($.trim($("#filter_search").val())!=""){
						aoData.push( { "name":"filter_search","value": $("#filter_search").val() } );
					}
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            });
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>sections/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 250,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
        				var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var regions = $( "#regions" );
   				 	$( "#edit_id" ).val('');
   				 	$( "#action" ).val('');
					var col=$("#col");
					var row=$("#row");
					$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
   				 	regions.val("");
   					regions.removeClass( "ui-state-error" );
					col.val("");
   					col.removeClass( "ui-state-error" );
					row.val("");
   					row.removeClass( "ui-state-error" );
					$("#kml_files").val('');
   				}
    			
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deletesections" ).live('click',function() {
				if(confirm('Are you sure delete this record and his data?')){
        			var edit_id = $(this).attr('data-ID');
        			$.post('<?php echo BASE_URL;?>sections/deleteRecord',{edit_id:edit_id},function(){
        				$('#sections_table').dataTable().fnDraw();
            		});
				}
    		});	
    		
    		$( "#addnewsections" ).live('click',function() {
				$( "#action" ).val('add');
				$( "#dialog-form" ).dialog( "open" );
    		});
			$("#editsections").live("click",function(){
				var edit_id=$(this).attr('data-ID');
				var regions=$(this).attr('regions');
				$( "#dialog-form #action" ).val('edit');
    			var sData = sectionsTable.fnGetData($(this).parents('tr')[0]);
    			$( "#dialog-form #edit_id" ).val(edit_id);
				regions=regions.split(",");
				for(i=0;i<regions.length;i++){
					$("#dialog-form #regions option").filter('[value="'+regions[i]+'"]').prop('selected',true);
				}
				$("#dialog-form #row").val(sData[1]);
				$("#dialog-form #col").val(sData[2]);
				$( "#dialog-form" ).dialog( "open" );
			});
			$('#filter_search').live('keyup',function(){
    			$('#sections_table').dataTable().fnDraw();
        	});
    	});

        function updateTips( t ) {
			tips.text( t ).addClass( "ui-state-highlight" );
			setTimeout(function(){
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
		
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
        	var regions=$("#regions");
			var col=$("#col");
			var row=$("#row");
			
        	if(regions.val()==""){
        		regions.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('region_error');?>" );
				return false;
        	}else{
        		regions.removeClass( "ui-state-error" );
        	}
			if($.trim(col.val()).length<=0){
				col.addClass("ui-state-error");
				updateTips("<?php echo $this->lang->line('col_error');?>");
				return false;
			}else{
				col.removeClass("ui-state-error");
			}
			if($.trim(row.val()).length<=0){
				row.addClass("ui-state-error");
				updateTips("<?php echo $this->lang->line('row_error');?>");
				return false;
			}else{
				row.removeClass("ui-state-error");
			}
			jQuery("#loader").show();
			
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
        	if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				var regions = $( "#regions" );
				var col=$("#col");
				var row=$("#row");
				
				regions.val( "" );
				regions.removeClass( "ui-state-error" );
				col.val( "" );
				col.removeClass( "ui-state-error" );
				row.val( "" );
				row.removeClass( "ui-state-error" );
				
				$("#kml_files").val('');
			 	$( "#dialog-form" ).dialog('close');
				$('#sections_table').dataTable().fnDraw();
			}
        } 

/* = refresh */
      /*  var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#kmlfiles_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
	</script>
<?php }?>
<?php if($this->uri->segment(1)=='markers'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var tips = $( ".validateTips" );
	var markersTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	markersTable = $('#markers_table').dataTable( {
        		"aoColumnDefs": [{"sClass":"center","aTargets":[0],"bSortable":false},{"sClass":"center","aTargets":[1]},{"sClass":"center", "aTargets":[2]}<?php if($display_menu==TRUE){?>,{"sClass":"center","aTargets":[3],"bSortable":false}<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				 "fnServerData": function ( sSource, aoData, fnCallback ) {
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            });
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>markers/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height:'auto',
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var marker_type=$("#marker_type");
    	      	  	var marker_icon1=$("#marker_icon1");
    	      	  	var marker_icon2=$("#marker_icon2");
    	      	  	$( "#edit_id" ).val('');
 				 	$( "#action" ).val('');
					$("#is_upload").val('');
					$("#image_preview1").html("");
					$("#image_preview2").html("");
 				 	marker_type.val( "" );
 				 	marker_type.removeClass( "ui-state-error" );
 				 	marker_icon1.val( "" );
 				 	marker_icon1.removeClass( "ui-state-error" );
					marker_icon2.val( "" );
 				 	marker_icon2.removeClass( "ui-state-error" );
    			}
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deletemarkers" ).live('click',function() {
				if(confirm('Are you sure delete this record and his data?')){
        			var edit_id = $(this).attr('data-ID');
        			$.post('<?php echo BASE_URL;?>markers/deleteRecord',{edit_id:edit_id},function(){
        				$('#markers_table').dataTable().fnDraw();
            		});
				}
    		});	
    		
    		$( "#editmarkers" ).live('click',function() {
    				var edit_id = $(this).attr('data-ID');
					var image1=$(this).attr('image1');
					var image2=$(this).attr('image2');
					if(image1!=""){
						$("#is_upload1").val("no");
					}else{
						$("#is_upload1").val('yes');
					}
					if(image2!=""){
						$("#is_upload2").val("no");
					}else{
						$("#is_upload2").val('yes');
					}
					
    				$( "#dialog-form #action" ).val('edit');
    				var sData = markersTable.fnGetData($(this).parents('tr')[0]);
    				$( "#dialog-form #edit_id" ).val(edit_id);
					$("#dialog-form #marker_type").val(sData[2]);
					$("#dialog-form #image_preview1").html(sData[0]);
					$("#dialog-form #image_preview2").html(sData[1]);
    				$( "#dialog-form" ).dialog( "open" );
    		});
			
    		$( "#addnemarkers" ).live('click',function() {
    			$( "#dialog-form #edit_id" ).val('');
				$("#is_upload1").val('yes');
				$("#is_upload2").val('yes');
    			$( "#dialog-form #action" ).val('add');
				$( "#dialog-form" ).dialog( "open" );
    		});

    	} );

        function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var marker_type=$("#marker_type");
    	    var marker_icon1=$("#marker_icon1");
			 var marker_icon2=$("#marker_icon2");
      	  	if(marker_type.val()==""){
          	  	marker_type.addClass("ui-state-error");
                updateTips("<?php echo $this->lang->line('marker_type_error');?>");
                return false;
    	  	}else{
        	  	marker_type.removeClass("ui-state-error");
    	  	}
			if($("#is_upload1").val()=='yes'){
				if(marker_icon1.val()==""){
					marker_icon1.addClass("ui-state-error");
                	updateTips("<?php echo $this->lang->line('marker_icon_error');?>");
                	return false;
				}else{
					marker_icon1.removeClass("ui-state-error");
				}
			}
			if($("#is_upload2").val()=='yes'){
				if(marker_icon2.val()==""){
					marker_icon2.addClass("ui-state-error");
                	updateTips("<?php echo $this->lang->line('marker_icon_error');?>");
                	return false;
				}else{
					marker_icon2.removeClass("ui-state-error");
				}
			}
			jQuery("#loader").show();
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				$("#marker_type").val('');
      	  		$("#marker_icon1").val('');
				$("#marker_icon2").val('');
				$("#is_upload1").val('');
				$("#is_upload2").val('');
				$("#image_preview1").html("");
				$("#image_preview2").html("");
      	  		$( "#dialog-form" ).dialog('close');
				$('#markers_table').dataTable().fnDraw();
			}
        } 
/* = refresh */
      /*  var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#markers_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
    </script> 
<?php }?>

<?php if($this->uri->segment(1)=='rules'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var tips = $( ".validateTips" );
	var rulesTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	rulesTable = $('#rules_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ] },{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ]},{ "sClass": "center", "aTargets": [ 3 ],"bSortable": false}<?php if($display_menu==TRUE){?>,{ "sClass": "center", "aTargets": [4] , "bSortable": false },{ "sClass": "center", "aTargets": [5] , "bSortable": false }<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				
				 "fnServerData": function ( sSource, aoData, fnCallback ) {
		            /* Add some data to send to the source, and send as 'POST' */
					if($.trim($("#form_date").val())!=""){
						aoData.push({"name":"form_date","value":$("#form_date").val()});
					}
					if($.trim($("#to_date").val())!=""){
						aoData.push({"name":"to_date","value":$("#to_date").val()});
					}
		           	if($.trim($("#filter_place_regions").val())!=""){
						aoData.push( { "name":"filter_place_regions","value": $("#filter_place_regions").val() } );
					}
					if($.trim($("#filter_place").val())!=""){
						aoData.push( { "name":"filter_place","value": $("#filter_place").val() } );
					}
					if($.trim($("#filter_title").val())!=""){
						aoData.push( { "name":"filter_title","value": $("#filter_title").val() } );
					}
					if($.trim($("#filter_search").val())!=""){
						aoData.push( { "name":"filter_search","value": $("#filter_search").val() } );
					}
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            } );
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>rules/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 550,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var place=$("#place");
    	      	  	var title=$("#title");
    	      	  	var rules_status=$("#rules_status");
    	      	  	var description=$("#description");
					var place_regions=$("#place_regions");
    	      	  	$( "#edit_id" ).val('');
 				 	$( "#action" ).val('');
					$("#place_id").val('');
 				 	$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
					place_regions.val( "" );
 				 	place_regions.removeClass( "ui-state-error" );
 				 	place.html( "" );
 				 	place.removeClass( "ui-state-error" );
 				 	title.val( "" );
 				 	title.removeClass( "ui-state-error" );
 				 	rules_status.val( "" );
 				 	rules_status.removeClass( "ui-state-error" );
 				 	description.val( "" );
 				 	description.removeClass( "ui-state-error" );
    			}
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deleterules" ).live('click',function() {
				if(confirm('Are you sure delete this record and his data?')){
        			var edit_id = $(this).attr('data-ID');
        			$.post('<?php echo BASE_URL;?>rules/deleteRecord',{edit_id:edit_id},function(){
        				$('#rules_table').dataTable().fnDraw();
            		});
				}
    		});	
    		$('#filter_search').live('keyup',function(){
    			$('#rules_table').dataTable().fnDraw();
        	});
    		$( "#editrules" ).live('click',function() {
    				var edit_id = $(this).attr('data-ID');
					var place_id=$(this).attr('place');
					var region_id=$(this).attr('region-id');
					var rules_status=$(this).attr('status');
					
					var ajaxUrl="<?php ADMIN_URL;?>rules/GetPlaces/"+region_id+"/"+place_id;
					$.ajax({
						dataType:'json',
						Type:'post',
						url:ajaxUrl,
						beforeSend: function(){
							jQuery("#loader").show();
						},
						success:function(response){
							jQuery("#loader").hide();
							if(response.result=='<?php echo  $this->lang->line('error');?>'){
								var content='<option value="all">'+response.message+'</option>';
								$("#place").html(content);
							}else{
								var content="";
								for(i=0;i<response.data.length;i++){
									var selected="";
									if(response.data[i].id==place_id){
										selected='selected="selected"';
									}
									option='<option value="'+response.data[i].id+'" '+selected+'>'+response.data[i].place_name+'</option>';
									content=content+option;
								}
								$("#place").html('');
								$("#place").html(content);
							}
						}
					});
    				$( "#dialog-form #action" ).val('edit');
    				var sData = rulesTable.fnGetData($(this).parents('tr')[0]);
    				$( "#dialog-form #edit_id" ).val(edit_id);
					$("#dialog-form #title").val(sData[2]);
					$("#dialog-form #description").val(sData[3]);
					$("#dialog-form #rules_status").val(rules_status);
					$("#dialog-form #place_id").val(place_id);
					$("#dialog-form #place_regions").val(region_id);
					$("#dialog-form #place").val(place_id);
    				$( "#dialog-form" ).dialog( "open" );
    		});
			
    		$( "#addnewrules" ).live('click',function() {
    			$( "#dialog-form #edit_id" ).val('');
    			$( "#dialog-form #action" ).val('add');
				$( "#dialog-form" ).dialog( "open" );
    		});
			$("#place_regions").live("change",function(){
        		var region_id=$(this).val();
				var place_id_id=$("#place_id").val();
        		var ajaxUrl="<?php ADMIN_URL;?>rules/GetPlaces/"+region_id+"/"+place_id_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					beforeSend: function(){
						jQuery("#loader").show();
					},
					success:function(response){
						jQuery("#loader").hide();
						if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option>';
							$("#place").html(content);
						}else{
							var content="";
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#place").html('');
							$("#place").html(content);
						}
					}
				});
    		});
			$("#filter_place_regions").live("change",function(){
        		var region_id=$(this).val();
				var ajaxUrl="<?php ADMIN_URL;?>rules/GetFiltesRegions/"+region_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					beforeSend: function(){
						jQuery("#loader").show();
					},
					success:function(response){
						jQuery("#loader").hide();
						if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option>';
							$("#filter_place").html(content);
						}else{
							var content="";
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#filter_place").html('');
							$("#filter_place").html(content);
						}
					}
				});
    		});
    	} );
		$("#filter_form_id").live("click",function(){
			$("#form_date").datepicker({"dateFormat":"yy-mm-dd"});
			$("#to_date").datepicker({"dateFormat":"yy-mm-dd"});
			$( "#filter-dialog" ).dialog( "open" );
		});
		$("#filter-dialog").dialog({
			autoOpen: false,
            height: 'auto',
            width: 359,
            position:'center',
            resizable: false,
            hide: "explode",
            modal: true,
			buttons: {
    			"Filters": function() {
					$('#rules_table').dataTable().fnDraw();
						 $( this ).dialog( "close" );
    				},
					Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
            close: function() {
                $("#filter_place_regions").val("");
				$("#filter_place").html("");
				$("#filter_title").val("");
				$("#form_date").val("");
				$("#to_date").val("");
			 }
		 });
        function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var place=$("#place");
      	  	var title=$("#title");
      	  	var rules_status=$("#rules_status");
      	  	var description=$("#description");
			var place_regions=$("#place_regions");
			
			if(place_regions.val().length<=0){
      	  		place_regions.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('region_error');?>" );
				return false;
      	  	}else{
      	  		place_regions.removeClass( "ui-state-error" );
      	  	}
			
      	  	if(place.val().length<=0){
      	  		place.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('place_error');?>" );
				return false;
      	  	}else{
      	  		place.removeClass( "ui-state-error" );
      	  	}
      	  	if(title.val()==""){
          	  	title.addClass("ui-state-error");
                updateTips("<?php echo $this->lang->line('title_error');?>");
                return false;
    	  	}else{
        	  	title.removeClass("ui-state-error");
    	  	}
	      	if(description.val().length<=0){
	      		description.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('description_error');?>" );
				return false;
	  	  	}else{
	  	  		description.removeClass("ui-state-error");
	  	  	}
			if(rules_status.val()==""){
	      		rules_status.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('status_error');?>" );
				return false;
	  	  	}else{
	  	  		rules_status.removeClass("ui-state-error");
	  	  	}
			jQuery("#loader").show();
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				$("#place").html('');
      	  		$("#title").val('');
      	  		$("#rules_status").val('');
      	  		$("#description").val('');
				$("#place_regions").val('');
				$("#place_id").val('');
			 	$( "#dialog-form" ).dialog('close');
				$('#rules_table').dataTable().fnDraw();
	           
			}
            
        } 

/* = refresh */
      /*  var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#rules_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
    </script> 
<?php }?>
<?php if($this->uri->segment(1)=='notice'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var tips = $( ".validateTips" );
	var noticeTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	$( "#notice_live" ).datetimepicker({"dateFormat":"yy-mm-dd"});
        	$( "#expiry_date" ).datetimepicker({"dateFormat":"yy-mm-dd"});
			 
        	noticeTable = $('#notice_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ] },{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ] },{ "sClass": "center", "aTargets": [ 3 ], "bSortable": false  },{ "sClass": "center", "aTargets": [ 4 ], "bSortable": true },{ "sClass": "center", "aTargets": [ 5 ] , "bSortable": true },{ "sClass": "center", "aTargets": [ 6 ] , "bSortable": false }],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				 "fnServerData": function ( sSource, aoData, fnCallback ) {
		            /* Add some data to send to the source, and send as 'POST' */
					if($.trim($("#posted_form_date").val())!=""){
						aoData.push({"name":"posted_form_date","value":$("#posted_form_date").val()});
					}
					if($.trim($("#posted_to_date").val())!=""){
						aoData.push({"name":"posted_to_date","value":$("#posted_to_date").val()});
					}
					if($.trim($("#expiry_form_date").val())!=""){
						aoData.push({"name":"expiry_form_date","value":$("#expiry_form_date").val()});
					}
					if($.trim($("#expiry_to_date").val())!=""){
						aoData.push({"name":"expiry_to_date","value":$("#expiry_to_date").val()});
					}
		           	if($.trim($("#filter_place_regions").val())!=""){
						aoData.push( { "name":"filter_place_regions","value": $("#filter_place_regions").val() } );
					}
					/*if($.trim($("#filter_place").val())!=""){
						aoData.push( { "name":"filter_place","value": $("#filter_place").val() } );
					}*/
					if($.trim($("#filter_title").val())!=""){
						aoData.push( { "name":"filter_title","value": $("#filter_title").val() } );
					}
					if($.trim($("#filter_search").val())!=""){
						aoData.push( { "name":"filter_search","value": $("#filter_search").val() } );
					}
					if($.trim($("#filter_addedbyorganisation").val())!=""){
						aoData.push( { "name":"filter_addedbyorganisation","value": $("#filter_addedbyorganisation").val() } );
					}
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            } );
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>notice/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 550,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
					var regions=$("#regions");
					//var places=$("#places");
					var title=$("#title");
					var notice_live=$("#notice_live");
					var expiry_date=$("#expiry_date");
					var description=$("#description");
					$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
 				 	regions.val( "" );
 				 	regions.removeClass( "ui-state-error" );
 				 	//places.html( "" );
 				 	//places.removeClass( "ui-state-error" );
 				 	title.val( "" );
 				 	title.removeClass( "ui-state-error" );
 				 	notice_live.val( "" );
 				 	notice_live.removeClass( "ui-state-error" );
					expiry_date.val( "" );
 				 	expiry_date.removeClass( "ui-state-error" );
 				 	description.val( "" );
 				 	description.removeClass( "ui-state-error" );
    			}
    		});
			$(".ui-dialog-titlebar").remove();
			$( "#dialog-form-edit" ).dialog({
    			autoOpen: false,
    			height: 400,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validateEdit,  // pre-submit callback 
    		    		        success:       showResponseUpdate  // post-submit callback 
    		    		    };
		    		    
    					$('#contentformupdate').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
					var regions=$("#regions");
					//var places=$("#places");
					var title=$("#title");
					var notice_live=$("#notice_live");
					var expiry_date=$("#expiry_date");
					var description=$("#description");
					$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
 				 	regions.val( "" );
 				 	regions.removeClass( "ui-state-error" );
 				 	//places.html( "" );
 				 	//places.removeClass( "ui-state-error" );
 				 	title.val( "" );
 				 	title.removeClass( "ui-state-error" );
 				 	notice_live.val( "" );
 				 	notice_live.removeClass( "ui-state-error" );
					expiry_date.val( "" );
 				 	expiry_date.removeClass( "ui-state-error" );
 				 	description.val( "" );
 				 	description.removeClass( "ui-state-error" );
    			}
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deletenotice" ).live('click',function() {
        		if(confirm('Are you sure delete this record and his data?')){
					var edit_id = $(this).attr('data-ID');
					$.post('<?php echo BASE_URL;?>notice/deleteRecord',{edit_id:edit_id},function(){
						$('#notice_table').dataTable().fnDraw();
					});
				}
    		});	
    		
    		$( "#editnotice" ).live('click',function() {
    				var edit_id = $(this).attr('data-ID');
					$( "#dialog-form-edit" ).load('<?php echo BASE_URL;?>notice/editForm/'+edit_id).dialog('open');
    				//var sData = noticeTable.fnGetData($(this).parents('tr')[0]);
    				//$( "#dialog-form" ).dialog( "open" );
			});
			
			$('#filter_search').live('keyup',function(){
    			$('#notice_table').dataTable().fnDraw();
        	});
    		$( "#addnewnotice" ).live('click',function() {
    			$( "#dialog-form" ).dialog( "open" );
    		});
    		/*$("#regions").live("change",function(){
        		var region_id=$(this).val();
        		ajaxUrl="<?php ADMIN_URL;?>notice/GetPlaces/"+region_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					success:function(response){
						if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option><option value="all"><?php echo  $this->lang->line('all');?></option>';
								
							$("#places").html(content);
						}else{
							var content="";
							content='<option value="all"><?php echo  $this->lang->line('all');?></option>';
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#places").html('');
							$("#places").html(content);
						}
					}
				});
    		});
			
			$("#editregions").live("change",function(){
        		var region_id=$(this).val();
        		ajaxUrl="<?php ADMIN_URL;?>notice/GetPlaces/"+region_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					success:function(response){
						if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option><option value="all"><?php echo  $this->lang->line('all');?></option>';
							$("#editplaces").html(content);
						}else{
							var content="";
							content='<option value="all"><?php echo  $this->lang->line('all');?></option>';
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#editplaces").html('');
							$("#editplaces").html(content);
						}
					}
				});
    		});*/
			$("#filter_place_regions").live("change",function(){
        		var region_id=$(this).val();
        		ajaxUrl="<?php ADMIN_URL;?>notice/GetPlaces/"+region_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					success:function(response){
						/*if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option><option value="all"><?php echo  $this->lang->line('all');?></option>';
							$("#filter_place").html(content);
						}else{
							var content="";
							content='<option value="all"><?php echo  $this->lang->line('all');?></option>';
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#filter_place").html('');
							$("#filter_place").html(content);
						}*/
					}
				});
    		});

    	} );

		$("#filter_form_id").live("click",function(){
			$("#posted_form_date").datepicker({"dateFormat":"yy-mm-dd"});
			$("#posted_to_date").datepicker({"dateFormat":"yy-mm-dd"});
			$("#expiry_form_date").datepicker({"dateFormat":"yy-mm-dd"});
			$("#expiry_to_date").datepicker({"dateFormat":"yy-mm-dd"});
			$( "#filter-dialog" ).dialog( "open" );
		});
		$("#filter-dialog").dialog({
			autoOpen: false,
            height: 'auto',
            width: 359,
            position:'center',
            resizable: false,
            hide: "explode",
            modal: true,
			buttons: {
    			"Filters": function() {
					$('#notice_table').dataTable().fnDraw();
						 $( this ).dialog( "close" );
    				},
					Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
            close: function() {
				$("#posted_form_date").val('');
				$("#posted_to_date").val('');
				$("#expiry_form_date").val('');
				$("#expiry_to_date").val('');
				$("#filter_place_regions").val('');
				//$("#filter_place").html('');
				$("#filter_title").val('');
                $("#filter_place_regions").val("");
				$("#filter_place").val("");
				$("#filter_addedbyorganisation").val('');
			}
		 });
        function updateTips( t ) {
				var tips = $( ".validateTips" );
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
		function validateEdit(formData, jqForm, options){
			var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var regions=$("#editregions");
			//var places=$("#editplaces");
			var title=$("#edittitle");
			var notice_live=$("#editnotice_live");
			var expiry_date=$("#editexpiry_date");
			var description=$("#editdescription");
			
			if(regions.val()==""){
				regions.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('region_error');?>');
				return false;
			}else{
				regions.removeClass('ui-state-error');
			}
			
			/*if(places.val()==""){
				places.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('place_error');?>');
				return false;
			}else{
				places.removeClass('ui-state-error');
			}*/
			
			if(title.val()==""){
				title.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('title_error');?>');
				return false;
			}else{
				title.removeClass('ui-state-error');
			}
			
			if(notice_live.val()==""){
				notice_live.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('notice_live_error');?>');
				return false;
			}else{
				notice_live.removeClass('ui-state-error');
			}
			if(description.val()==""){
				description.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('description_error');?>');
				return false;
			}else{
				description.removeClass('ui-state-error');
			}
			jQuery("#loader").show();
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var regions=$("#regions");
			//var places=$("#places");
			var title=$("#title");
			var notice_live=$("#notice_live");
			var expiry_date=$("#expiry_date");
			var description=$("#description");
			
			if(regions.val()==""){
				regions.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('region_error');?>');
				return false;
			}else{
				regions.removeClass('ui-state-error');
			}
			
			/*if(places.val()==""){
				places.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('place_error');?>');
				return false;
			}else{
				places.removeClass('ui-state-error');
			}*/
			
			if(title.val()==""){
				title.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('title_error');?>');
				return false;
			}else{
				title.removeClass('ui-state-error');
			}
			
			if(notice_live.val()==""){
				notice_live.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('notice_live_error');?>');
				return false;
			}else{
				notice_live.removeClass('ui-state-error');
			}
			
			if(description.val()==""){
				description.addClass('ui-state-error');
				updateTips('<?php echo $this->lang->line('description');?>');
				return false;
			}else{
				description.removeClass('ui-state-error');
			}
			jQuery("#loader").show();
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				$("#regions").val('');
				//$("#places").html('');
				$("#title").val('');
				$("#notice_live").val('');
				$("#expiry_date").val('');
				$("#description").val('');
				$( "#dialog-form" ).dialog('close');
				$('#notice_table').dataTable().fnDraw();
	           
			}
            
        } 
		function showResponseUpdate(responseText, statusText, xhr, $form){
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				$("#editregions").val('');
				$("#editplaces").html('');
				$("#edittitle").val('');
				$("#editnotice_live").val('');
				$("#editexpiry_date").val('');
				$("#editdescription").val('');
				$( "#dialog-form-edit" ).dialog('close');
				$('#notice_table').dataTable().fnDraw();
	           
			}
		}
		
		$("#regions").live("focus",function(){
			$( "#dialog-form-regions" ).dialog( "open" );
		});
		$("#editregions").live("focus",function(){
			if(jQuery("#contentformupdate").length>0){
				jQuery("#contentformupdate input[name='noticeregions[]']").each(function(index, element){
				  	jQuery("#selectedregions #regionsids_"+jQuery(this).val()).attr('checked','checked');
                });
			}
			$( "#dialog-form-regions" ).dialog( "open" );
		});
		$( "#dialog-form-regions" ).dialog({
    			autoOpen: false,
    			height:'auto',
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var data =$("#selectedregions").serializeArray()
						var RegionsName='';
						var inputfield="";
						jQuery(".myselectedregions").html('');
						if(jQuery("#editregions").length>0){
							jQuery("#editregions").val('');
						}else{
							jQuery("#regions").val('');
						}
						if(data.length>1){
							for(i=0;i<data.length;i++){
								 if(i==data.length-1){
									RegionsName+=data[i].name; 
								 }else{
									RegionsName+=data[i].name+",";
								 }
								 inputfield+='<input type="hidden" name="noticeregions[]" value="'+data[i].value+'">';
							}
							jQuery(".myselectedregions").html(inputfield);
							if(jQuery("#editregions").length>0){
								jQuery("#editregions").val(RegionsName);
							}else{
								jQuery("#regions").val(RegionsName);
							}
							
							//$("#places").html('');
							//$("#places").html('<option value="all"><?php echo  $this->lang->line('all');?></option>');
						}else{
							if(data.length>0){
							//setRegionsPlaces(data[0].value);
							
							jQuery(".myselectedregions").html('');
							jQuery(".myselectedregions").html('<input type="hidden" name="noticeregions[]" value="'+data[0].value+'">');
							if(jQuery("#editregions").length>0){
								jQuery("#editregions").val(data[0].name);
							}else{
								jQuery("#regions").val(data[0].name);
							}
							}
						}
						
						 $( this ).dialog( "close" );
					},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function(){
					if(jQuery.trim(jQuery("#regions").val()).length<=0){
						$("#selectedregions input[type='checkbox']").each(function(index, element) {
                        	jQuery(this).removeAttr('checked');
                    	});
					}
    			}
    		});

/* = refresh */
       /* var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#notice_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
	function setRegionsPlaces(id){
	   		var region_id=id;
        		ajaxUrl="<?php ADMIN_URL;?>notice/GetPlaces/"+region_id;
        		$.ajax({
            		dataType:'json',
            		Type:'post',
            		url:ajaxUrl,
					success:function(response){
						if(response.result=='<?php echo  $this->lang->line('error');?>'){
							var content='<option value="">'+response.message+'</option><option value="all"><?php echo  $this->lang->line('all');?></option>';
								
							$("#places").html(content);
						}else{
							var content="";
							content='<option value="all"><?php echo  $this->lang->line('all');?></option>';
							for(i=0;i<response.data.length;i++){
								option='<option value="'+response.data[i].id+'">'+response.data[i].place_name+'</option>';
								content=content+option;
							}
							$("#places").html('');
							$("#places").html(content);
						}
					}
				});
    		
	}
    </script> 
<?php }?>
<?php if($this->uri->segment(1)=='places'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var tips = $( ".validateTips" );
	var placesTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	placesTable = $('#places_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ] },{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ] },{ "sClass": "center", "aTargets": [ 3 ]},{ "sClass": "center", "aTargets": [ 4 ],"bSortable": false},{ "sClass": "center", "aTargets": [5] , "bSortable": false },{ "sClass": "center", "aTargets": [6] , "bSortable": false }],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				"fnServerData": function ( sSource, aoData, fnCallback ) {
		           	if($.trim($("#filter_place_regions").val())!=""){
						aoData.push( { "name":"filter_place_regions","value": $("#filter_place_regions").val() } );
					}
					if($.trim($("#filter_place").val())!=""){
						aoData.push( { "name":"filter_place","value": $("#filter_place").val() } );
					}
					if($.trim($("#filter_search").val())!=""){
						aoData.push( { "name":"filter_search","value": $("#filter_search").val() } );
					}
				   $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            } );
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>places/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 550,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var place_name=$("#place_name");
    	      	  	var regions=$("#regions");
    	      	  	var latitude=$("#latitude");
    	      	  	var longitude=$("#longitude");
    	      	  	var description=$("#description");
					var marker=$("#marker");
    	      	  	$( "#edit_id" ).val('');
 				 	$( "#action" ).val('');
 				 	$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
 				 	place_name.val( "" );
 				 	place_name.removeClass( "ui-state-error" );
 				 	regions.val( "" );
 				 	regions.removeClass( "ui-state-error" );
 				 	latitude.val( "" );
 				 	latitude.removeClass( "ui-state-error" );
 				 	longitude.val( "" );
 				 	longitude.removeClass( "ui-state-error" );
 				 	description.val( "" );
 				 	description.removeClass( "ui-state-error" );
					marker.val( "" );
 				 	marker.removeClass( "ui-state-error" );
    			}
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deleteplaces" ).live('click',function() {
    			if(confirm('Are you sure delete this record and his data?')){
        			var edit_id = $(this).attr('data-ID');
        			$.post('<?php echo BASE_URL;?>places/deleteRecord',{edit_id:edit_id},function(){
            			$('#places_table').dataTable().fnDraw();
            		});
				}
    		});	
    		$('#filter_search').live('keyup',function(){
    			$('#places_table').dataTable().fnDraw();
        	});
    		$( "#editplaces" ).live('click',function() {
        			var edit_id = $(this).attr('data-ID');
    				var region_id = $(this).attr('regions');
    				var latitude = $(this).attr('latitude');
    				var longitude = $(this).attr('longitude');
					var marker=$(this).attr('marker');
					$( "#dialog-form #action" ).val('edit');
					var sData = placesTable.fnGetData($(this).parents('tr')[0]);
    				$( "#dialog-form #edit_id" ).val(edit_id);
    				$( "#dialog-form #place_name" ).val(sData[0]);
    				$( "#dialog-form #regions" ).val(region_id);
					$("#dialog-form #marker").val(marker);
    				$( "#dialog-form #latitude" ).val(latitude);
    				$( "#dialog-form #longitude" ).val(longitude);
    				$( "#dialog-form #description" ).val(sData[4]);
    				$('#dialog-form .for-image').html(sData[5]);
    				$('#dialog-form .place_image').attr('width','140px');
    				var img=$('#dialog-form .place_image').attr('alt');
    				
    				$( "#dialog-form #place_image" ).val('');
    				$( "#dialog-form #old_image").val(img);
    				if($("#dialog-form #old_image").val()==''){
    					$( "#dialog-form .place-image-main" ).css('display','none');
        			}else{
    					$( "#dialog-form .place-image-main" ).css('display','block');
        			}
        			
    				$( "#dialog-form" ).dialog( "open" );
    				
    		});
			
    		$( "#addnewplace" ).live('click',function() {
    			$( "#dialog-form .place-image-main" ).css('display','none');
    			$( "#dialog-form #old_image" ).val('');
    			$( "#dialog-form #edit_id" ).val('');
    			$( "#dialog-form #action" ).val('add');
    			$( "#dialog-form #place_image" ).val('');
				$( "#dialog-form" ).dialog( "open" );
    		});

    		$( "#delete-image" ).live('click',function() {
    			if(confirm('Are you sure delete this image?')){
    				var edit_id=$( "#dialog-form #edit_id" ).val();
        			$.post('<?php echo BASE_URL;?>places/deleteOldImage',{edit_id:edit_id},function(){
        				$( "#dialog-form" ).dialog( "close" );
            			$('#places_table').dataTable().fnDraw();
            		});
				}
				return false;
    		});	
    		
    	} );
		$("#filter_form_id").live("click",function(){
			$( "#filter-dialog" ).dialog( "open" );
		});
		$("#filter-dialog").dialog({
			autoOpen: false,
            height: 'auto',
            width: 359,
            position:'center',
            resizable: false,
            hide: "explode",
            modal: true,
			buttons: {
    			"Filters": function() {
					$('#places_table').dataTable().fnDraw();
						 $( this ).dialog( "close" );
    				},
					Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
            close: function() {
                $("#filter_place_regions").val("");
				$("#filter_place").val("");
			}
		 });
        function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var place_name=$("#place_name");
      	  	var regions=$("#regions");
      	  	var latitude=$("#latitude");
      	  	var longitude=$("#longitude");
      	  	var description=$("#description");
			var marker=$("#marker");
			//var latReg=/\?[-+]\?[0-9]+[\.]{0,1}[0-9]*/;
			var latReg=/^\-?\d+(\.{0,1}\d+)$/;
			//var longReg=/\?[0-9]+[\.]{0,1}[0-9]*/;
			var longReg=/^\-?\d+(\.{0,1}\d+)$/;	
			
      	  	if(place_name.val().length<=0){
      	  		place_name.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('place_name_error');?>" );
				return false;
      	  	}else{
      	  		place_name.removeClass( "ui-state-error" );
      	  	}
      	  	if(regions.val()==""){
          	  	regions.addClass("ui-state-error");
                updateTips("<?php echo $this->lang->line('region_error');?>");
                return false;
    	  	}else{
        	  	regions.removeClass("ui-state-error");
    	  	}
			if(marker.val()==""){
          	  	marker.addClass("ui-state-error");
                updateTips("<?php echo $this->lang->line('marker_error');?>");
                return false;
    	  	}else{
        	  	marker.removeClass("ui-state-error");
    	  	}
	      	if(latitude.val().length<=0){
	      		latitude.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('latitude_error');?>" );
				return false;
	  	  	}else if(latitude.val().length>0 && !latReg.test(latitude.val())){
				latitude.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('lat_error');?>" );
				return false;	
			}else{
	  	  		latitude.removeClass("ui-state-error");
	  	  	}
	      	if(longitude.val().length<=0){
	      		longitude.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('longitude_error');?>" );
				return false;
	  	  	}else if(longitude.val().length>0 && !longReg.test(longitude.val())){
				longitude.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('long_error');?>" );
				return false;	
			}else{
	  	  		longitude.removeClass("ui-state-error");
	  	  	}
	      	if(description.val().length<=0){
	      		description.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('description_error');?>" );
				return false;
	  	  	}else{
	  	  		description.removeClass("ui-state-error");
	  	  	}
	      	jQuery("#loader").show(); 
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				var place_name=$("#place_name").val('');
	      	  	var regions=$("#regions").val('');
	      	  	var latitude=$("#latitude").val('');
	      	  	var longitude=$("#longitude").val('');
	      	  	var description=$("#description").val('');
				$("#marker").val("");
			 	$( "#dialog-form" ).dialog('close');
				$('#places_table').dataTable().fnDraw();
	           
			}
            
        } 

/* = refresh */
       /* var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#places_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
    </script> 
<?php }?>
<?php if($this->uri->segment(1)=='users'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var tips = $( ".validateTips" );
	var usersTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	usersTable = $('#users_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ] },{ "sClass": "center", "aTargets": [ 1 ] },{ "sClass": "center", "aTargets": [ 2 ]},{ "sClass": "center", "aTargets": [ 3 ],"bSortable": false}<?php if($display_menu==TRUE){?>,{ "sClass": "center", "aTargets": [4] , "bSortable": false },{ "sClass": "center", "aTargets": [5] , "bSortable": false }<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				"fnServerData": function ( sSource, aoData, fnCallback ) {
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            });
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>users/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 550,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
    					var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var username=$("#username");
    	      	  	var regions=$("#regions");
    	      	  	var name=$("#name");
    	      	  	var email=$("#email");
    	      	  	var password=$("#password");
					var userstatus=$("#userstatus");
    	      	  	$( "#edit_id" ).val('');
 				 	$( "#action" ).val('');
 				 	$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
 				 	username.val( "" );
 				 	username.removeClass( "ui-state-error" );
 				 	regions.val( "" );
 				 	regions.removeClass( "ui-state-error" );
 				 	name.val( "" );
 				 	name.removeClass( "ui-state-error" );
 				 	email.val( "" );
 				 	email.removeClass( "ui-state-error" );
 				 	password.val( "" );
 				 	password.removeClass( "ui-state-error" );
					userstatus.val(0);
 				 	userstatus.removeClass( "ui-state-error" );
    			}
    		});
        	$(".ui-dialog-titlebar").remove();
    		$( "#deleteusers" ).live('click',function() {
				if(confirm('Are you sure delete this record and his data?')){
					var edit_id = $(this).attr('data-ID');
					$.post('<?php echo BASE_URL;?>users/deleteRecord',{edit_id:edit_id},function(){
						$('#users_table').dataTable().fnDraw();
					});
				}
    		});	
    		
    		$( "#editusers" ).live('click',function() {
    			var edit_id = $(this).attr('data-ID');
    			var region_id = $(this).attr('regions');
				var usersStatus=$(this).attr('usersstatus');
    			$( "#dialog-form #action" ).val('edit');
    			var sData = usersTable.fnGetData($(this).parents('tr')[0]);
    			$( "#dialog-form #edit_id" ).val(edit_id);
    			$( "#dialog-form #regions" ).val(region_id);
				$("#dialog-form #username").val(sData[0]).attr('readonly','readonly');
				$("#dialog-form #name").val(sData[1]);
				$("#dialog-form #email").val(sData[2]);	
				$("#dialog-form #userstatus").val(usersStatus);			
    			$( "#dialog-form" ).dialog( "open" );
    		});
			
    		$( "#addnewusers" ).live('click',function() {
				$("#dialog-form #username").removeAttr('readonly');
    			$( "#dialog-form #edit_id" ).val('');
    			$( "#dialog-form #action" ).val('add');
				$( "#dialog-form" ).dialog( "open" );
    		});

    	} );

        function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
      	  	var username=$("#username");
    	    var regions=$("#regions");
    	    var name=$("#name");
    	    var email=$("#email");
    	    var password=$("#password");
			if(regions.val()==""){
          	  	regions.addClass("ui-state-error");
                updateTips("<?php echo $this->lang->line('region_error');?>");
                return false;
    	  	}else{
        	  	regions.removeClass("ui-state-error");
    	  	}
      	  	if(username.val().length<=0){
      	  		username.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('username_error');?>" );
				return false;
      	  	}else{
      	  		username.removeClass( "ui-state-error" );
      	  	}
      	  
	      	if(name.val().length<=0){
	      		name.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('name_error');?>" );
				return false;
	  	  	}else{
	  	  		name.removeClass("ui-state-error");
	  	  	}
	      	if(email.val().length<=0){
	      		email.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('email_error');?>" );
				return false;
	  	  	}else{
	  	  		email.removeClass("ui-state-error");
	  	  	}
			if(email.val()!=""){
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if( !emailReg.test(email.val() ) ) {
					email.addClass( "ui-state-error" );
					updateTipsText( "<?php echo $this->lang->line('valide_email');?>" );
					return false;
				} else {
					email.removeClass( "ui-state-error" );
					return true;
				}
			}
	      	if(password.val().length<=0){
	      		password.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('password_error');?>" );
				return false;
	  	  	}else{
	  	  		password.removeClass("ui-state-error");
	  	  	}
			jQuery("#loader").show();
	      	     
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				$("#username").val('');
    	    	$("#regions").val('');
    	   		$("#name").val('');
    	    	$("#email").val('');
    	    	$("#password").val('');
				$("#userstatus").val(0);
			 	$( "#dialog-form" ).dialog('close');
				$('#users_table').dataTable().fnDraw();
			}
        } 

/* = refresh */
      /*  var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#users_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
    </script> 
<?php }?>

<?php if($this->uri->segment(1)=='regions'){?>
    <script type="text/javascript">
 	var giCount = 1;
 	var region_name=$("#region_name");
 	var tips = $( ".validateTips" );
	var regionsTable;
        $(document).ready(function() {
        	var fixHelper = function(e, ui) {
        		ui.children().each(function() {
        			$(this).width($(this).width());
        		});
        		return ui;
        	};
        	
        	regionsTable = $('#regions_table').dataTable( {
        		"aoColumnDefs": [ { "sClass": "center", "aTargets": [ 0 ] },{ "sClass": "center", "aTargets": [ 1 ] }<?php if($display_menu==TRUE){?>,{ "sClass": "center", "aTargets": [2] , "bSortable": false }<?php }?>],
        		"sPaginationType": "full_numbers",
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "bSort": true,
                "aaSorting": [[ 0, "desc" ]],
				"fnServerData": function ( sSource, aoData, fnCallback ) {
		            $.ajax( {
		                "dataType": 'json',
		                "type": "POST",
		                "url": sSource,
		                "data": aoData,
		                "success": fnCallback,
						"error":loginRedirect
		            });
		        },
                "bLengthChange":false,
                "sAjaxSource": "<?php echo BASE_URL;?>regions/all/",
                "fnRowCallback":function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                	 $(nRow).attr("id","id_"+aData[0]);
                     return nRow;
                }
            } );
        	$( "#dialog-form" ).dialog({
    			autoOpen: false,
    			height: 250,
    			width: 359,
    			modal: true,
    			resizable:false,
    			buttons: {
    				"Submit": function() {
        				var options = { 
    							dataType:'json',
    							Type:'POST',
    		    		      	beforeSubmit:  validate,  // pre-submit callback 
    		    		        success:       showResponse  // post-submit callback 
    		    		    };
		    		    
    					$('#contentform').ajaxSubmit(options);
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			},
    			close: function() {
    				var region_name = $( "#region_name" );
   				 	$( "#edit_id" ).val('');
   				 	$( "#action" ).val('');
   				 	$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
   				 	region_name.val( "" );
   					region_name.removeClass( "ui-state-error" );
   					$("#csvfiles").val('');
   					
    			}
    			
    		});
        	$(".ui-dialog-titlebar").remove();
        	/*$("#dialog-form").data('dialog').uiDialog.draggable('option', {
        	    cancel: '.ui-dialog-titlebar-close',
        	    handle: '.ui-dialog-titlebar, .ui-dialog-content'
        	})*/
    		$( "#deleteregions" ).live('click',function() {
				if(confirm('Are you sure delete this record and his data?')){
        			var edit_id = $(this).attr('data-ID');
        			$.post('<?php echo BASE_URL;?>regions/deleteRecord',{edit_id:edit_id},function(){
        				$('#regions_table').dataTable().fnDraw();
            		});
				}
    		});	
    		
    		$( "#editregions" ).live('click',function() {
    				var edit_id = $(this).attr('data-ID');
    				$("#dialog-form #action").val('edit');
    				var sData = regionsTable.fnGetData($(this).parents('tr')[0]);
    				$("#dialog-form #edit_id").val(edit_id);
    				$("#dialog-form #region_name").val(sData[0]);
    				$( "#dialog-form" ).dialog( "open" );
    		});
			
    		$( "#addnewregions" ).live('click',function() {
    			$( "#dialog-form #edit_id" ).val('');
    			$("#dialog-form #action").val('add');
				$( "#dialog-form" ).dialog( "open" );
    		});
			$("#help_icon").live("click",function(){
				 $( "#files_help" ).dialog({
					height: 140,
					resizable:false,
					modal: true,
					draggable:false
				});
			});

    	} );

        function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
        function validate(formData, jqForm, options) { 
        	var re2 = /^\d{3}-\d{3}-\d{4}$/;
        	var region_name=$("#region_name");
        	if(region_name.val()==""){
        		region_name.addClass( "ui-state-error" );
				updateTips( "<?php echo $this->lang->line('region_name_error');?>" );
				return false;
        	}else{
        		region_name.removeClass( "ui-state-error" );
        	}
			jQuery("#loader").show();
        }
        function checkRegexp( o, regexp ) {
			if ( !( regexp.test( o.val() ) ) ) {
				return false;
			} else {
				return true;
			}
		}
		
        function showResponse(responseText, statusText, xhr, $form)  { 
			jQuery("#loader").hide();
        	$("#lodingImg").hide();
			if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
				$(".validateTips").html(responseText.message);
			}else{
				$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
				var region_name = $( "#region_name" );
				$( "#edit_id" ).val('');
				$( "#action" ).val('');
				region_name.val( "" );
				region_name.removeClass( "ui-state-error" );
				$("#csvfiles").val('');
			 	$( "#dialog-form" ).dialog('close');
				$('#regions_table').dataTable().fnDraw();
	           
			}
            
        } 

/* = refresh */
      /*  var refresh,       
        intvrefresh = function() {
            clearInterval(refresh);
            refresh = setTimeout(function() {$('#regions_table').dataTable().fnDraw();}, 5 * 1000);
        };

    $(document).live('keypress, click', function() { intvrefresh() });
    intvrefresh();*/
    </script> 
<?php }?>
<script type="text/javascript">
var giCount = 1;
var tips = $( ".validateTips" );
$(document).ready(function() {
	$( "#myaccount-form" ).dialog({
		autoOpen: false,
		height: 200,
    	width: 359,
    	modal: true,
    	resizable:false,
		buttons: {
			"Submit": function() {
				var options = { 
						dataType:'json',
						Type:'POST',
	    		      	beforeSubmit:  validateTip,  // pre-submit callback 
	    		        success:       showResponseText  // post-submit callback 
	    		    };
    		    
				$('#updateaccountform').ajaxSubmit(options);
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			var myname = $( "#myname" );
			var rmyegions = $( "#myregions" );
			var mypassword = $( "#mypassword" );
			var mypasswordagain = $( "#mypasswordagain" );
			var myemail = $( "#myemail" );
		 	$(".validateTips").html('<?php echo $this->lang->line('all_field_required');?>');
		 	myname.val( "" );
		 	myname.removeClass( "ui-state-error" );
		 	rmyegions.val( "" );
		 	rmyegions.removeClass( "ui-state-error" );
		 	mypassword.val( "" );
		 	mypassword.removeClass( "ui-state-error" );
		 	mypasswordagain.val( "" );
		 	mypasswordagain.removeClass( "ui-state-error" );
		 	myemail.val( "" );
		 	myemail.removeClass( "ui-state-error" );
				
		}
	});
	$(".ui-dialog-titlebar").remove();
	$( "#myaccountview" ).live('click',function() {
		$( "#myaccount-form" ).load('<?php echo BASE_URL;?>authentication/myaccount').dialog('open'); 
	});
	
		
});
function updateTipsText( t ) {
	var tips = $( ".validateTips" );
	tips.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		tips.removeClass( "ui-state-highlight", 1500 );
	}, 500 );
}
function validateTip(formData, jqForm, options) { 
	var re2 = /^\d{3}-\d{3}-\d{4}$/;
	var myname = $( "#myname" );
	var rmyegions = $( "#myregions" );
	var mypassword = $( "#mypassword" );
	var mypasswordagain = $( "#mypasswordagain" );
	var myemail = $( "#myemail" );
	if(myname.val()==""){
		myname.addClass( "ui-state-error" );
		updateTipsText( "<?php echo $this->lang->line('name_error');?>" );
		return false;
	}else{
		myname.removeClass( "ui-state-error" );
	}
	if(rmyegions.val()==""){
		rmyegions.addClass( "ui-state-error" );
		updateTipsText( "<?php echo  $this->lang->line('region_error');?>" );
		return false;
	}else{
 		rmyegions.removeClass( "ui-state-error" );
	}
	if(myemail.val()==""){
		myemail.addClass( "ui-state-error" );
		updateTipsText( "<?php echo $this->lang->line('email_error');?>" );
		return false;
	}else{
 		myemail.removeClass( "ui-state-error" );
	}
	
	if(myemail.val()!=""){
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    if( !emailReg.test(myemail.val() ) ) {
	    	myemail.addClass( "ui-state-error" );
	    	updateTipsText( "<?php echo $this->lang->line('valide_email');?>" );
			return false;
	    } else {
	    	myemail.removeClass( "ui-state-error" );
	    	return true;
	    }
	}
	if(mypassword.val()!=""){
		if(mypasswordagain.val()==""){
			mypasswordagain.addClass( "ui-state-error" );
			updateTipsText( "<?php echo $this->lang->line('password_again_error');?>" );
			return false;
		}else if(mypassword.val()!=mypasswordagain.val()){
			mypasswordagain.addClass( "ui-state-error" );
			updateTipsText( "<?php echo $this->lang->line('password_not_match');?>" );
			return false;
		}
	}else{
		mypasswordagain.removeClass( "ui-state-error" );
	}
}
function checkRegexp( o, regexp ) {
	if ( !( regexp.test( o.val() ) ) ) {
		return false;
	} else {
		return true;
	}
}

function showResponseText(responseText, statusText, xhr, $form)  { 
	if(responseText.result=='<?php echo  $this->lang->line('error');?>'){
		$(".validateTips").html(responseText.message);
	}else{
		$( "#myaccount-form" ).dialog('close');
	}
} 
jQuery(document).ready(function(e) {
    jQuery("#language_name").live("change",function(){
		jQuery("#language_form").submit();
	});
});
function loginRedirect(error){
	if(error.responseText=="Session Expired"){
		alert("Your Session is expired please re login");
		document.location="<?php echo BASE_URL;?>";
	}
}
</script>
</body>
</html>