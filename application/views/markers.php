<div class="content-part">
	<div class="for-sepret-line">
		<h1><?php echo  $this->lang->line('marker_header');?></h1>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="markers_table">
		<thead>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_marker_icon1');?></th>
                <th width="25%"><?php echo $this->lang->line('th_marker_icon2');?></th>
				<th width="25%"><?php echo $this->lang->line('th_marker_type');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="4" class="dataTables_empty" style="border-left: 1px solid #C4CDD7;border-right: 1px solid #C4CDD7;"><?php echo  $this->lang->line('load_data_inserver');?></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th width="25%"><?php echo $this->lang->line('th_marker_icon1');?></th>
                <th width="25%"><?php echo $this->lang->line('th_marker_icon2');?></th>
				<th width="25%"><?php echo $this->lang->line('th_marker_type');?></th>
				<?php if($display_menu==TRUE){?>
				<th width="25%" align="center"><?php echo $this->lang->line('th_action');?></th>
				<?php }?>
			</tr>
		</tfoot>
	</table>
	<div class="table_menu">
		<ul class="left">
			<li><a href="javascript:void(0);" class="button add_new" id="addnemarkers"><span><span><?php echo $this->lang->line('addbutton');?></span></span></a></li>
		</ul>
	</div>
	<div id="dialog-form" title="Add Place" style="display: none;">
		<form method="post" action="<?php echo BASE_URL?>markers/addNew" name="contentform" enctype="multipart/form-data" id="contentform" class="form" validate="validate">
			<input type="hidden" value="" name="action" id="action">
			<input type="hidden" value="" name="edit_id" id="edit_id">
			<input type="hidden" value="" name="is_upload1" id="is_upload1">
            <input type="hidden" value="" name="is_upload2" id="is_upload2">
            
			<div class="for-hd-login">
            	<div class="log-logo"><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/marker-big.png" alt="Marine Mate" /></a></div>
              <div class="top-login-tex"><?php echo  $this->lang->line('marker_header');?></div>
            </div>
			<div class="for-log-repeat">
                <div class="for-horozontal-repeat">
                    <div class="for-form-mn">
                    <span class="validateTips"><?php echo $this->lang->line('all_field_required');?></span>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('marker_type');?> :</div>
                            <div class="for-s-bg"><input name="marker_type" class="new-fid-search" type="text" id="marker_type"/></div>
                        </div>
                       <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('marker_icon1');?> :</div>
                            <div class="for-s-file">
                            	<div id="image_preview1"></div>
                            	<input type="file" name="marker_icon1" id="marker_icon1">
                            </div>
                        </div>
                        <div class="for-lg-mn">
                            <div class="for-name"><?php echo $this->lang->line('marker_icon2');?> :</div>
                            <div class="for-s-file">
                            	<div id="image_preview2"></div>
                            	<input type="file" name="marker_icon2" id="marker_icon2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>