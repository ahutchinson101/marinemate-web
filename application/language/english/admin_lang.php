<?php
// login detail 
$lang ['username']="Username";
$lang ['password']="Password";
$lang ['enter']="Enter";
$lang ['max_login_attempts_error']="Your maximum login attempt has been finished. Please try after some time.";
$lang ['site_name']="Marine Mate";
$lang ['login_details_error']="Wrong Username and/or Password.";
$lang ['login_inactive_error']="Your Account is status is inactive.You can\'t login!";
$lang ['welcome']="Welcome";
$lang ['district_council']='District Council';
$lang ['loading']="Loading...";

//menu
$lang ['porfile']="Profile";
$lang ['logout']="Logout";
$lang ['notice']="Notice";
$lang ['users']="Users";
$lang ['places']="Places";
$lang ['regions']="Regions";
$lang ['rules']="Rules";
$lang ['kml']="KML Files";
$lang ['sections']="Sections";

//Form Fields
$lang ['all_field_required']="All form fields are required.";
$lang['region']="Regions";
$lang ['select_region']="Select Region";
$lang ['name']="Name";
$lang ['email']="Email";
$lang ['password_again']="Password (Again)";
$lang ['posted_date']="Posted Date";
$lang ['form']="Form";
$lang ['to']="To";
$lang ['expiry_date']="Expiry Date";
$lang ['all']="All";
$lang['place']="Places";
$lang['title']="Title";
$lang['select_place']="Select Places";
$lang['notice_live']="Notice Live";
$lang['description']="Description";
$lang['status']="Status";
$lang['inactive']="Inactive";
$lang['active']="Active";
$lang ['place_name']="Place Name";
$lang['marker']="Markers";
$lang['select_marker']="Select Marker";
$lang['coordinate']="GPS Coordinates";
$lang['marker_type']="Marker Type";
$lang['marker_icon1']="Marker Icon1";
$lang['marker_icon2']="Marker Icon2";
$lang['region_name']="Region Name";
$lang['coordinate_file']="Coordinate File";
$lang ['coordinate_files_help']="Please upload unique comma saprated file with anti clock counter wise point";
$lang['kaml_files']="KML Files";
$lang['place_image']="Place Image";
$lang['old_image']='Old Image';
$lang['image']='Image';
$lang['col']="Col";
$lang['row']="Row";
$lang['Addedbyorganisation']="Added by which organisation?";
$lang['place_description']="Notes";

// Form Header
$lang ['profiles']="Profiles";

//Form Field Error
$lang ['name_error']="Please Enter Name..!";
$lang ['region_error']="Please Select Region..!";
$lang ['password_not_match']="Password Again Not Match..!";
$lang ['password_again_error']="Please Enter Password (again)..!";
$lang ['valide_email']="Please Enter Valide Email..!";
$lang ['email_error']="Please Enter Email...!";
$lang ['place_error']="Please Select Place...!";
$lang ['title_error']="Please Enter Title..!";
$lang['notice_live_error']="Please Enter Notice Live Date...!";
$lang['expiry_date_error']="Please Enter Expiry Date...!";
$lang['description_error']="Please Enter Description..!";
$lang['username_error']="Please Enter Username...!";
$lang['password_error']="Please Enter Password...!";
$lang['unique_username_error']="This username already exists...!";
$lang['place_name_error']="Please enter place name";
$lang['latitude_error']="Please enter latitude";
$lang['longitude_error']="Please enter longitude";
$lang['marker_error']="Please Select marker";
$lang['marker_type_error']="Please enter Marker Type";
$lang['marker_icon_error']="Please Select Marker Icon";
$lang['region_name_error']="Please Enter Region Name...!";
$lang['unique_region_name']="This Region Already Exists...!";
$lang['status_error']="Please Select Status...!";
$lang['col_error']="Please enter Col...!";
$lang['row_error']="Please enter Row...!";
$lang['row_col_unique_error']="This Row and Col already exists...!";
$lang['lat_error']="Latitude is not correctly typed";
$lang['long_error']="Longitude is not correctly typed";
//Button
$lang['addbutton']="Add New";
$lang['filter']="Filter";
$lang['exportbutton']="Export Survey";

//Response Check
$lang ['error']="Fail";
$lang ['success']="Success";
$lang['load_data_in_server']="Loading data from server";
$lang['place_not']="Place Not Available";
$lang['notice_not_add']="Notice Not Added...!";
$lang['notice_not_update']="Notice Not Updated...!";
$lang['user_not_add']="User Not Added...!";
$lang['user_not_update']="User Not Updated...!";
$lang ['place_not_add']="Place Not Added...!";
$lang['place_not_update']="Place Not Updated...!";
$lang['marker_not_add']="Marker Not Added...!";
$lang['marker_not_update']="Marker Not Updated...!";
$lang['region_not_add']="Region Not Addedd...!";
$lang['region_not_update']="Region Not Updated...!";
$lang['kml_not_add']="KML Files Not Added...!";
$lang['rules_not_add']="Rules Not Added...!";
$lang['rules_not_update']="Rules Not Updated...!";

//General Descriptions
$lang ['notice_header']="Recent notice in your area";
$lang['notice_filters']="Notice Filters";
$lang['users_header']="Users Management";
$lang['place_header']="Place Management";
$lang['place_filter']="Place Filters";
$lang['place_dialog_header']="Please add a place";
$lang['marker_header']="Marker Management";
$lang['regions_header']="Regions Management";
$lang['kmlfiles_header']="KML Files Management";
$lang['rules_header']="Rules Management";
$lang['rules_filter']="Rules Filters";
$lang['sections_header']="Sections Management";
$lang['survey_header']="Survey Management";
$lang['notice_dialog_header']='Add Notice';
$lang['regions_dialog_header']='Regions';

//column header
$lang ['th_posted_date']="Posted Date";
$lang ['th_region']="Regions";
$lang['th_area']="Area";
$lang['th_title']="Title";
$lang['th_expiry_date']="Expiry Date";
$lang['th_action']="Action";
$lang['th_username']="Username";
$lang['th_name']="Name";
$lang['th_email']="Email";
$lang['th_status']="Status";
$lang['th_place']="Places";
$lang['th_marker']="Marker Types";
$lang['th_gps_coordinates']="GPS Coordinates";
$lang['th_description']="Description";
$lang['th_marker_icon1']="Marker Icon1";
$lang['th_marker_icon2']="Marker Icon2";
$lang['th_marker_type']="Marker Type";
$lang['th_coordinates_files']="Coordinates Files";
$lang['th_kml_files']="KML Files";
$lang['th_col']="Col";
$lang['th_row']="Row";
$lang['th_device_id']="Device ID";
$lang['th_main_region']="Main Region";
$lang['th_age_group']="Age Group";
$lang['th_main_type_vessel']="Main Type Vessel";
$lang['th_main_reason_vessel']="Main Reason Vessel";
$lang['th_safety_equipment']="Safety Equipment";
$lang['th_place_notice']="Notes";
$lang['th_added_by']='Added By';
?>