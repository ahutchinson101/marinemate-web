<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class CSVReader{
    var $fields;
    var $separator = ',';
    function parse_text($p_Text){
        $lines = explode("\n", $p_Text);
        return $this->parse_lines($lines);
    }
    function parse_file($p_Filepath){
        $lines = file($p_Filepath);
        return $this->parse_lines($lines);
    }
    function parse_lines($p_CSVLines){    
        $content = FALSE;
        foreach( $p_CSVLines as $line_num => $line ){
            if( $line != '' ){
                $elements = explode($this->separator, $line);
                if(!is_array($content)){
                    $this->fields =$elements;
                    $content = array();
                }else{
                    $item = array();
                    foreach($this->fields as $id => $field ){
                        if(isset($elements[$id])){
                            $item[trim($field)] =trim(str_replace('\n', "", $elements[$id]));
                        }
                    }
                    $content[] = $item;
                }
            }
        }
        return $content;
    }
}