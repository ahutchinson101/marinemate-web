<?php
class Sections_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}
	}
	public function GetAllRegions(){
		$query=$this->db->get("region_master");
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function UniqueCellPoints($row,$col){
		$query=$this->db->get_where('sections',array('row'=>$row,'col'=>$col));
		if($query->num_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function AddSections($data){
		$query=$this->db->insert("sections",$data);
		if($query){
			return TRUE;
		}else{
			return $this->db->_error_message();
		}
	}
	public function UpdateSections($data,$id){
		$query=$this->db->update("sections",$data,array('id'=>$id));
		if($query){
			return TRUE;
		}else{
			return $this->db->_error_message();
		}
	}
	public function EditUniqueCellPoints($row,$col,$id){
		$query=$this->db->get_where('sections',array('id'=>$id));
		if($query->num_rows()>0){
			if($query->row()->row==$row && $query->row()->col==$col){
				return FALSE;
			}else{
				return $this->UniqueCellPoints($row, $col);
			}
		}
	}
	public function getSectionsData($id){
		$query=$this->db->get_where('sections',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row();
		}
	}
}