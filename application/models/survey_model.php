<?php
class Survey_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return $id;
		}
	}
	
	public function GetAllSurvey(){
		$query=$this->db->get("mm_survey");
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetSafetyEquipment($id){
		$ids=explode(',', $id);
		$this->db->where_in('id',$ids);
		$query=$this->db->get("survey_safety_equipment");
		if($query->num_rows()>0){
			
			$string="";
			foreach ($query->result() as $data){
				$string.='<img src="'.INCLUDE_URL.'assets/equipment/icon/'.$data->icon.'" alt="'.$data->equipment_name.'" title="'.$data->equipment_name.'"> &nbsp;';	
			}
			return $string;
		}
	}
	
	public function GetAlllSeftyEquipment(){
		$query=$this->db->get("survey_safety_equipment");
		if($query->num_rows()>0){
			return $query->result();
		}
	}
}