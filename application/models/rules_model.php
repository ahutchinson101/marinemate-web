<?php
class Rules_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function GetALlRegions(){
		$query=$this->db->get('region_master');
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetRegionPlaces($id,$place_id){
		if($place_id!=0){
			$query=$this->db->query("SELECT * FROM `places` where (not exists (select id from rules where places.id=rules.place_id) OR exists (select id from rules where places.id=".$place_id.")) AND region_id=".$id);
		}else{
			$query=$this->db->query("SELECT * FROM `places` where not exists (select id from rules where places.id=rules.place_id) AND region_id=".$id);
		}
		//$query=$this->db->get_where('places',array('region_id'=>$id));
		
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetFiltersRegionPlaces($id){
		$query=$this->db->get_where('places',array('region_id'=>$id));
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetPlaces(){
		//$query=$this->db->get('places');
		$query=$this->db->query("SELECT * FROM `places` where not exists (select id from rules where places.id=rules.place_id)");
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetPlaceName($id){
		$query=$this->db->get_where('places',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->place_name;
		}else{
			return "";
		}
	}
	public function AddNewRules($data){
		$query=$this->db->insert('rules',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function UpdateRules($data,$id){
		$this->db->where('id',$id);
		$this->db->update('rules',$data);
	}
}