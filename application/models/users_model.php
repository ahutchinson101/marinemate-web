<?php
class Users_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function GetAllRegions(){
		$query=$this->db->query("SELECT * FROM `region_master` ORDER BY `region_name` ASC");
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return "";
		}
	}
	public function UniqueUserName($username){
		$query=$this->db->get_where('users',array('LOWER(username)'=>strtolower($username)));
		if($query->num_rows()>0){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	public function addUsers($data){
		$query=$this->db->insert('users',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function UpdateUsers($data,$id){
		$this->db->where('id',$id);
		$this->db->update('users',$data);
		if($this->db->affected_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}