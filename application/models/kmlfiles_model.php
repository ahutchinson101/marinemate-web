<?php
class Kmlfiles_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function GetAllRegions(){
		$query=$this->db->query("SELECT * FROM `region_master` where not exists (select id from region_kml_files where region_master.id=region_kml_files.region_id)  ORDER BY `region_name` ASC");
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return "";
		}
	}
	public function getKmlFiles($id){
		$query=$this->db->get_where('region_kml_files',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row();
		}else{
			return '';
		}
	}
	function AddNewKmlFiles($data){
		$query=$this->db->insert('region_kml_files',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}