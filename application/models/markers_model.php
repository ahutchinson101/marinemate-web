<?php
class Markers_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function AddNewMarkers($data){
		$query=$this->db->insert('marker',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function UpdateMarkers($data,$id){
		$this->db->where('id',$id);
		$this->db->update('marker',$data);
		if($this->db->affected_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}