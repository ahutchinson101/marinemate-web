<?php
class Places_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function GetRegions(){
		$query=$this->db->query("SELECT * FROM `region_master` ORDER BY `region_name` ASC");
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetMarkers(){
		$query=$this->db->get('marker');
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetMarkersName($id){
		$query=$this->db->get_where('marker',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->marker_type;
		}else{
			return "";
		}
	}
	public function AddNewPlace($data){
		$query=$this->db->insert('places',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function UpdatePlace($data,$id){
		$this->db->where('id',$id);
		$this->db->update('places',$data);
	}
	
	public function GetPlaceImage($id){
		$query=$this->db->query("SELECT image FROM `places` WHERE `id`=$id");
		return $query->row();
	}
	
	public function DeletedPlaceImage($data1){
		$query=$this->db->insert('deleted_place_image',$data1);		
	}
}