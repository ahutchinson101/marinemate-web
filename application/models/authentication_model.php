<?php
class Authentication_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function get_user($username){
		$userdata = $this->db->query("SELECT * FROM `users` WHERE `username` = ? ",array($username));
		if($userdata->num_rows()>0)
			return $userdata->row_array();
	}
	public function check_user($username,$password){
		return $this->db->query("SELECT * FROM `users` WHERE `username` = ? AND password = ? Limit 1",array($username,md5($password)));
	}
	
	public function get_currentUser($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0)
			return $query->row();
	}
	public function GetRegion($id=""){
		if($id!=""){
			$query=$this->db->get_where("region_master",array('id'=>$id));
			if($query->num_rows()>0){
				return $query->row();
			}else{
				return "";
			}
		}else{
			$query=$this->db->query("SELECT * FROM `region_master` ORDER BY `region_name` ASC");
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return "";
			}
		}
	}
	
}