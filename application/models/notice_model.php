<?php
class Notice_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}
	}
	public function GetAllRegions(){
		if(is_admin()==TRUE){
			$query=$this->db->query("SELECT * FROM `region_master` ORDER BY `region_name` ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}else{
			if($this->session->userdata('region_id')==0){
				$query=$this->db->query("SELECT * FROM `region_master` ORDER BY `region_name` ASC");
				if($query->num_rows()>0){
					return $query->result();
				}	
			}else{
				$query=$this->db->get_where('region_master',array('id'=>$this->session->userdata('region_id')));
				if($query->num_rows()>0){
					return $query->result();
				}
			}
		}
	}
	public function GetRegionPlaces($id){
		$query=$this->db->get_where('places',array('region_id'=>$id));
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetOneNotice($id){
		$query=$this->db->get_where('notice',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row();
		}
	}
	
	public function GetPlaceName($id){
		$query=$this->db->get_where('places',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->place_name;
		}
	}
	public function AddNotice($data){
		$query=$this->db->insert('notice',$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function UpdateNotice($data,$id){
		$this->db->where('id',$id);
		$this->db->update('notice',$data);
		if($this->db->affected_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function getMappingReginsName($id){
		$query=$this->db->get_where('notice_and_region_mapping',array('id'=>$id));
		if($query->num_rows()>0){
			$data=$query->row();
			return $this->ReginameByID($data->regions_id);
		}
	}
	function ReginameByID($id){
		$ids=explode(',',$id);
		if(in_array(0,$ids)){
			$all='All';
		}else{
			$all="";
		}
		$regionsname="";
		$data=$this->db->where_in('id',$ids)->get('region_master')->result();
		foreach($data as $d){
			$regionsname.=$d->region_name.',';
		}
		$regionsname=substr($regionsname,0,-1);
		if($all!="" && $regionsname!=""){
			$all=",All";
		}
		return $regionsname.$all;
	}
}