<?php
class Regions_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function getUserName($id){
		$query=$this->db->get_where('users',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->name;
		}else{
			return "";
		}
	}
	public function GetRegionName($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row()->region_name;
		}else{
			return "";
		}
	}
	public function UinqueRegion($region_name){
		$query=$this->db->get_where('region_master',array('LOWER(region_name)'=>strtolower($region_name)));
		if($query->num_rows()>0){
			return FALSE;
		}else{
			return TRUE;
		}
	}
	function AddNewRegions($data){
		$query=$this->db->insert('region_master',$data);
		if($query){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	function UpdateRegions($data,$id){
		$this->db->where('id',$id);
		$this->db->update('region_master',$data);
	}
	public function getRegions($id){
		$query=$this->db->get_where('region_master',array('id'=>$id));
		if($query->num_rows()>0){
			return $query->row();
		}
	}
	public function GetRegionsKMLFiles($regionid){
		$query=$this->db->get_where('region_kml_files',array('region_id'=>$regionid));
		if($query->num_rows()>0){
			return $query->row();
		}
	}
}