<?php
class Api_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function GetApi($username,$password){
		if($username!="" && $password!=""){
			$query=$this->db->query("SELECT * FROM `api_users` WHERE `username`='".$username."' AND `password`='".md5($password)."'");
			if($query->num_rows()>0){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	public function GetRegionsMaster($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM region_master WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY region_name ASC");
		}else{
			$query=$this->db->query("SELECT * FROM region_master ORDER BY region_name ASC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetNotice($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT id,user_id,region_id,place_id,notice_title,notice_description,notice_live_date,expiry_date,posted_date,notice_status,updated_date,addedbyorganization FROM notice WHERE (`posted_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY posted_date DESC");
		}else{
			$query=$this->db->query("SELECT id,user_id,region_id,place_id,notice_title,notice_description,notice_live_date,expiry_date,posted_date,notice_status,updated_date,addedbyorganization FROM notice ORDER BY posted_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetPlaces($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT `id`,`region_id`,`marker_id`,`place_name`,`latitude`,`longitude`,`place_description`,`created_date`,`updated_date` FROM places WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT `id`,`region_id`,`marker_id`,`place_name`,`latitude`,`longitude`,`place_description`,`created_date`,`updated_date` FROM places ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetPlacesdv1($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT `id`,`region_id`,`marker_id`,`place_name`,`latitude`,`longitude`,`place_description`,`image`,`created_date`,`updated_date`,`image_updated` FROM places WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT `id`,`region_id`,`marker_id`,`place_name`,`latitude`,`longitude`,`place_description`,`image`,`created_date`,`updated_date`,`image_updated` FROM places ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetPlaceImages($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT image FROM places WHERE (`image_updated`>'".$lastdate."')");
		}else{
			$query=$this->db->query("SELECT image FROM places");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetRegionsKmlFiles($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM region_kml_files WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT * FROM region_kml_files ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetRules($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM rules WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT * FROM rules ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetMarker($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM marker WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT * FROM marker ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function GetSections($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM sections WHERE (`created_date`>'".$lastdate."' OR `updated_date`>'".$lastdate."') ORDER BY created_date DESC");
		}else{
			$query=$this->db->query("SELECT * FROM sections ORDER BY created_date DESC");
		}
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	
	public function GetDeletedRegionsMaster($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_region_master WHERE `created_date`>'".$lastdate."' ORDER BY region_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedRegionKmlFiles($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_region_kml_files WHERE `created_date`>'".$lastdate."' ORDER BY kml_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedPlaces($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_places WHERE `created_date`>'".$lastdate."' ORDER BY place_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedPlaceImages($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT image FROM deleted_place_image WHERE `deleted_date`>'".$lastdate."'");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedNotice($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_notice WHERE `created_date`>'".$lastdate."' ORDER BY notice_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedRules($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_rules WHERE `created_date`>'".$lastdate."' ORDER BY rules_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedMarker($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_marker WHERE `created_date`>'".$lastdate."' ORDER BY marker_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetDeletedSections($lastdate){
		if($lastdate!=""){
			$query=$this->db->query("SELECT * FROM deleted_sections WHERE `created_date`>'".$lastdate."' ORDER BY sections_id ASC");
			if($query->num_rows()>0){
				return $query->result();
			}
		}
	}
	public function GetRegionCoordinates($RegionId){
		$this->db->where_in('region_id',$RegionId);
		$query=$this->db->get('region_coordinates');
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	public function AddMMSurvey($data){
		$query=$this->db->insert("mm_survey",$data);
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}