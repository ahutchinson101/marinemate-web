<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class CSVReader{
	var $fields;
   	function parse_file($p_Filepath){
        $content=false;
        $file= fopen($p_Filepath, 'r');
        $this->fields=fgetcsv($file);
        while(($row=fgetcsv($file))!=false){            
            if($row[0]!=null){ 
                if(!$content){
                    $content = array();
                }
               	$items = array();
                foreach($this->fields as $id=>$field){
                	if(isset($row[$id])){
                    	$items[$field] = $row[$id];    
                    }
                }
                $content[] = $items;
            }
        }
        fclose($file);
        return $content;
    }
}