<?php
function is_login(){
	$CI = &get_instance();
	if($CI->session->userdata('logged_in') == TRUE){
		return TRUE;
	}else{
		if(isset($_REQUEST['sEcho'])){
			echo "Session Expired";exit;	
		}else{
			redirect('authentication');
		}
	}
}
function is_admin(){
	$CI = &get_instance();
	if($CI->session->userdata('logged_in') == TRUE && $CI->session->userdata('role')=='admin'){
		return TRUE;
	}else{
		return FALSE;
	}
}
function check_user_role($role){
	$CI = &get_instance();
	
	if($CI->session->userdata('role') == $role && $CI->session->userdata('role')=='admin'){
		return TRUE;
	}else{
		return FALSE;
	}
}
?>